# fvfilter - Forensic Video Filters #

fvfilter is a collection of filters for forensic video editing and processing.
The low level C API can be interfaced from plugins within other programs
or frameworks. The programming interface bears some resemblance to
the API of FFmpeg/libav libraries.

A GStreamer plugin is included and built by default.

Included filters:

* fvsharpen: Fast unsharp mask sharpening and gaussian blur

* fvconvolve2d: Generic 2d image convolution using a freely specified kernel

* fvtsmooth: Temporal median or average filter to smooth out noise in
  static video (such as low light surveillance camera footage)

fvfilter is distributed under the terms of the GNU Library General Public
License (LGPL) version 2, or (at your option) any later version.
See file LICENSE for more details.

## Prerequisites for building and running fvfilter ##

For building, you need

* a standard POSIX shell environment

* autoconf 2.53 or later

* automake 1.10 or later

* libtool 2.2.6 or later

* glibc 2.x

* gcc (4.5.x or later recommendend)

* yasm 1.0 or later (optional but required for SIMD code: SSE/SSE2/...)

On Debian/Ubuntu, these dependencies can be covered by
installing packages build-essential, autoconf and automake:

    sudo apt-get install build-essential autoconf automake yasm

Additionally the GStreamer plugin requires

* GStreamer 0.10 with development packages

* GLib 2.x, as required by GStreamer

On Debian/Ubuntu, install libgstreamer0.10-dev and
libgstreamer-plugins-base0.10-dev:

    sudo apt-get install libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev


## Build and install ##

Build procedure follows standard "./configure; make" paradigm.

### Generate automake and autoconf files ###

    ./autogen.sh

If the command runs through the sequence successfully, there is
usually no need to run it again on the same source code tree --
unless configure.ac gets changed.

### Configure the project ###

    ./configure

Autogen.sh runs the configure script once, so you may skip this
step if autogen was just used.

Optionally it is possible to specify an installation prefix and
system-specific compiler optimizations at this stage:

    ./configure --prefix="$HOME" CFLAGS="-O2 -march=native -mtune=native -fomit-frame-pointer"

### Build ###

    make

### Install ###

    make install


## Usage ##

Here is an example gst-launch command to create a playbin-based
filter graph that sharpens a video while playing it back through an Xv window:

    gst-launch playbin2 uri="file:///home/user/path/to/video.mp4" video-sink="navseek ! fvsharpen sigma=2.0 amount=0.5 ! xvimagesink"

Use gst-inspect to list filter properties:

    gst-inspect fvsharpen
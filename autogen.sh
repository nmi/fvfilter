#!/bin/sh
# you can either set the environment variables AUTOCONF, AUTOHEADER, AUTOMAKE,
# ACLOCAL, AUTOPOINT and/or LIBTOOLIZE to the right versions, or leave them
# unset and get the defaults

autoreconf --force -i -v || {
 echo 'autoreconf failed';
 exit 1;
}

./configure --enable-debug --prefix "$HOME" CFLAGS="-march=native -mtune=native -O2 -fomit-frame-pointer" || {
 echo 'configure failed';
 exit 1;
}

echo
echo "Now type 'make' to build this module or 'make install' to build and install"
echo

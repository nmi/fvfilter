
AC_DEFUN([ax_check_single_cflag], [
    CFLAGS="-Werror $1"
    AC_MSG_CHECKING([if $CC supports $1])
    AC_COMPILE_IFELSE([AC_LANG_SOURCE([int main(void) { return 0; }])], [
        valid_CFLAGS="$valid_CFLAGS $1"
        AC_MSG_RESULT([yes])
    ], [
        AC_MSG_RESULT([no])
    ])
])

# AX_CHECK_CFLAGS(VARIABLE, FLAGS)
#
# Checks whether the compiler accepts FLAGS, one item at a time.
# If a flag is accepted, it is added to a variable named VARIABLE.
#
AC_DEFUN([AX_CHECK_CFLAGS], [
    saved_CFLAGS="$CFLAGS"
    eval "valid_CFLAGS=\"\$$1\""
    m4_map_args_w([$2], [ax_check_single_cflag(], [)])
    CFLAGS="$saved_CFLAGS"
    eval "$1=\"$valid_CFLAGS\""
])


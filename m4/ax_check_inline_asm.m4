# AX_CHECK_INLINE_ASM(action-found, action-notfound)
#
# Checks whether the compiler supports GCC-style inline assembly.
# Defines HAVE_INLINE_ASM to 0 or 1 according to the result.
#
AC_DEFUN([AX_CHECK_INLINE_ASM], [
    AC_MSG_CHECKING([if $CC supports GCC-style inline assembly])
    AC_COMPILE_IFELSE([
        AC_LANG_PROGRAM([],
                        [__asm__ volatile ("" ::);])
    ], [
        AC_MSG_RESULT([yes])
        AC_DEFINE_UNQUOTED([HAVE_INLINE_ASM], 1,
           [Define to 1 if you have GCC-style inline assembly.])
        dnl action-found
        $1
    ], [
        AC_MSG_RESULT([no])
        AC_DEFINE_UNQUOTED([HAVE_INLINE_ASM], 0,
           [Define to 1 if you have GCC-style inline assembly.])
        dnl action-notfound
        $2
    ])
])


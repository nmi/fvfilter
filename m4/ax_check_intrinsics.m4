# AX_CHECK_MMX_INTRINSICS(action-found, action-notfound)
#
# Checks whether the compiler supports MMX intrinsics.
# Defines HAVE_MMX_INTRINSICS to 0 or 1 according to the result.
#
AC_DEFUN([AX_CHECK_MMX_INTRINSICS], [
    AC_MSG_CHECKING([if MMX intrinsics are enabled])
    AC_COMPILE_IFELSE([
        AC_LANG_PROGRAM([#include <mmintrin.h>],
                        [_mm_empty();])
    ], [
        AC_MSG_RESULT([yes])
        AC_DEFINE_UNQUOTED([HAVE_MMX_INTRINSICS], 1,
                           [Define to 1 if you have MMX intrinsics])
        dnl action-found
        $1
    ], [
        AC_MSG_RESULT([no])
        AC_DEFINE_UNQUOTED([HAVE_MMX_INTRINSICS], 0,
                           [Define to 1 if you have MMX intrinsics])
        dnl action-notfound
        $2
    ])
])


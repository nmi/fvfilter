# AX_GET_X86_CPU_ARCH(output_var, cpu_name)
#
# Returns the machine architecture of a given CPU.
# The main purpose is to parse various IA-32 CPU names.
#
# List of architectures and the recognized CPU names they cover:
#
#  - x86_32
#     - i386
#     - i486
#     - i586
#     - i686
#     - k6
#     - athlon*
#     - pentium*
#
#   x86_64
#     - x86_64
#     - amd64
#
# If the CPU is not in the above list, output_var is set empty.
#
# @param cpu_name CPU name, such as i686 or x86-64 from $host_cpu
# @param output_var name of a variable where the architecture is written
#
AC_DEFUN([AX_GET_X86_CPU_ARCH], [
    AS_CASE([$2],
        [i386|i486|i586|i686|k6|athlon*|pentium*], [x86arch="x86_32"],
        [x86_64|amd64],                            [x86arch="x86_64"],
    [
        x86arch=""
    ])
    $1="$x86arch"
])

# AX_CHECK_X86_CPU_ARCH(cpu_name)
#
# Checks the machine architecture of a given CPU. Sets and defines
# variables ARCH_X86, ARCH_X86_32 and ARCH_X86_64 to 0 or 1 according
# to the result.
#
# @param cpu_name CPU name, such as i686 or x86-64 from $host_cpu.
#                 see AX_GET_X86_CPU_ARCH() for a list of supported names.
#
AC_DEFUN([AX_CHECK_X86_CPU_ARCH], [
    ARCH_X86=0
    ARCH_X86_32=0
    ARCH_X86_64=0

    AC_MSG_CHECKING([for host architecture type])

    AX_GET_X86_CPU_ARCH([ARCH], [$1])
    AS_CASE([$ARCH],
        [x86_32], [
            ARCH_X86=1
            ARCH_X86_32=1
            AC_MSG_RESULT([32-bit x86 ($1)])
        ],
        [x86_64], [
            ARCH_X86=1
            ARCH_X86_64=1
            AC_MSG_RESULT([64-bit x86 ($1)])
        ],
    [
        AC_MSG_RESULT([generic ($1)])
    ])

    AC_DEFINE_UNQUOTED([ARCH_X86],    $ARCH_X86,
                       [Define to 1 if target architecture is IA-32 or x86-64])

    AC_DEFINE_UNQUOTED([ARCH_X86_32], $ARCH_X86_32,
                       [Define to 1 if target architecture is IA-32])

    AC_DEFINE_UNQUOTED([ARCH_X86_64], $ARCH_X86_64,
                       [Define to 1 if target architecture is x86-64])
])


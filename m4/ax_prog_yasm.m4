# AX_PROG_YASM(action-found, action-notfound, action-unsupported)
#
# Checks for Yasm and its output object format. If Yasm is found and
# can be used to generate binary objects for the host system, this
# macro sets and exports environment variables AS and ASFLAGS
# (after running action-found).
#
# @param action-found       executed if Yasm is found
# @param action-notfound    executed if Yasm is not found
# @param action-unsupported executed if the host OS or CPU is not one of
#                           the supported varieties and therefore yasm
#                           output object format cannot be set
#
AC_DEFUN([AX_PROG_YASM], [
    host_supported=1
    AX_GET_X86_CPU_ARCH([x86_arch], [$host_cpu])
    AS_IF([test "x$x86_arch" = "x"], [
        host_supported=0
    ], [
        AC_MSG_CHECKING([for object format])
        AS_CASE([$host_os],
            [*bsd*|linux*|beos|irix*|solaris*], [
                AS_IF([test "x$x86_arch" = "xx86_64"], [
                    AC_MSG_RESULT([elf64])
                    OBJFORMAT="elf64"
                ], [
                    AC_MSG_RESULT([elf])
                    OBJFORMAT="elf"
                ])],
            [[[cC]][[yY]][[gG]][[wW]][[iI]][[nN]]*|mingw32*|mks*], [
                AS_IF([test "x$x86_arch" = "xx86_64"], [
                    AC_MSG_RESULT([win64])
                    OBJFORMAT="win64"
                ], [
                    AC_MSG_RESULT([win32])
                    OBJFORMAT="win32"
                ])],
            [*darwin*], [
                AC_MSG_RESULT([macho])
                OBJFORMAT="macho"
            ],
        [
            host_supported=0
        ])
        AS_IF([test "x$host_supported" = "x1"], [
            AC_CHECK_PROG([yasm_found], [yasm], [yes], [no], [], [yes])
            AS_IF([test "$yasm_found" = "yes"], [
                AS="yasm"
                arch_x86_32=0
                arch_x86_64=0
                AS_IF([test "x$x86_arch" = "xx86_32"], [arch_x86_32=1])
                AS_IF([test "x$x86_arch" = "xx86_64"], [arch_x86_64=1])
                ASFLAGS="$ASFLAGS -f $OBJFORMAT -DARCH_X86_32=$arch_x86_32 -DARCH_X86_64=$arch_x86_64" 
                AS_IF([test "x$DEBUG" = "x1"], [
                    ASFLAGS="$ASFLAGS -g dwarf2"
                ])

                dnl action-found
                $1

                AC_SUBST(AS)
                AC_SUBST(ASFLAGS)
            ])
        ], [
            dnl action-notfound
            $2
        ])
    ])
    AS_IF([test "x$host_supported" = "x0"], [
        dnl action-unsupported
        $3
    ])
])


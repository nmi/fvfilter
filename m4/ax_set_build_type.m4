# AX_SET_BUILD_TYPE()
#
# Adds a switch for turning debugging on and off.
#
AC_DEFUN([AX_SET_BUILD_TYPE], [
    AC_MSG_CHECKING([whether to generate a debug build])
    AC_ARG_ENABLE([debug], AS_HELP_STRING([--enable-debug],
        [generate a debug build for development [default=no]]))
    AS_IF([test "x$enable_debug" = "xyes"], [
        AC_MSG_RESULT([yes])
        AC_DEFINE([DEBUG],[1],[debug build])
        CFLAGS="$CFLAGS -g"
    ], [
        AC_MSG_RESULT([no])
        AC_DEFINE([NDEBUG],[1],[release build])
    ])
])


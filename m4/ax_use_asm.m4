
# ax_match_wildcard_pattern(pattern, string, found)
#
# Matches a string to a wildcard pattern and sets variable "found" to 1 if
# they match.
#
AC_DEFUN([ax_match_wildcard_pattern], [
    AS_CASE([$2],
        [$1], [$3=1]
    )
])
 
# AX_USE_ASM(switch-type, default, supported-cpus, cpu,
#            action-supported, action-unsupported, action-disabled)
#
# Adds a switch for assembly optimizations.
# This macro sets and defines HAVE_ASM to 1 if assembly optimizations are
# enabled, or to 0 otherwise. Automake conditional USE_ASM is also set.
#
# @param switch-type        switch type: [enable] or [disable]
# @param default            default value: [yes] or [no]
# @param supported-cpus     whitespace-separated list of supported CPU name
#                           patterns
# @param cpu                CPU name to check against supported_cpus
# @param action-supported   executed if assembly optimizations are
#                           enabled and supported
# @param action-unsupported executed if assembly optimizations are not
#                           supported
# @param action-disabled    executed if assembly optimizations are
#                           disabled
#
AC_DEFUN([AX_USE_ASM], [
    dnl Set default value according to the switch type
    AS_IF([test "x$1" = "xenable"], [
        AS_IF([test "x$2" = "xno"], [default="no"], [default=""])
    ], [
        AS_IF([test "x$2" = "xno"], [default=""], [default="no"])
    ])
    AC_MSG_CHECKING([whether to enable assembly optimizations])
    AC_ARG_ENABLE([asm], AS_HELP_STRING([--$1-asm],
        [$1 assembly optimizations [default=$2]]), [],
        [enable_asm="$default"])

    AS_IF([test "x$enable_asm" = "xno"], [
        HAVE_ASM=0
        AC_MSG_RESULT([no])
        dnl action-disabled
        $7
    ], [ 
        cpu_found=0
        m4_map_args_w([$3],
                [ax_match_wildcard_pattern("], [", [$4], [cpu_found])])

        AS_IF([test "x$cpu_found" = "x0"], [
            HAVE_ASM=0
            AC_MSG_RESULT([no])
            dnl action-unsupported
            $6
            AS_IF([test "x$enable_asm" = "xyes"], [
                AC_MSG_ERROR([assembly optimizations requested but not available])
            ])
        ], [
            HAVE_ASM=1
            AC_MSG_RESULT([yes])
            dnl action-supported
            $5
        ])
    ])

    AC_DEFINE_UNQUOTED([HAVE_ASM], $HAVE_ASM,
        [Define to 1 if you would like to include assembly optimizations])

    AM_CONDITIONAL([USE_ASM], [test "x$HAVE_ASM" = "x1"])

])


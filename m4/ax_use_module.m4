
# AX_ENABLE_MODULE(name, upper-case-name, switch-type, default, modules,
#                  action-found, action-notfound, action-disabled)
#
# Adds a switch for a program module (or group of modules) that can be
# found with pkg-config. If the switch is enabled, the macro checks if
# the specified modules are available.
#
# This macro sets and defines variable HAVE_$upper-case-name to 1 if
# the group of modules is enabled and available, 0 otherwise.
# Variables $name_CFLAGS and $name_LIBS are exported for use in Makefile.am.
# Automake conditional variable USE_$upper-case-name is also set.
# If the module is enabled by the user but not available, an error
# is thrown.
#
# @param 1_name            name of the module
# @param 2_upper-case-name upper-case name of the module
# @param 3_switch-type     switch type: [enable] or [disable]
# @param 4_default         default value: [yes] or [no]
# @param 5_modules         pkg-config module specifications
# @param 6_action-found    executed if module is enabled and found
# @param 7_action-notfound executed if module is enabled but not found
# @param 8_action-disabled executed if module is disabled
#
AC_DEFUN([AX_ENABLE_MODULE], [
    dnl Set default value and printout values according to switch type
    AS_IF([test "x$3" = "xenable"], [
        AS_IF([test "x$4" = "xno"],
            [default="no"], [default=""])
    ], [
        AS_IF([test "x$4" = "xno"],
            [default=""], [default="no"])
    ])

    dnl Name of the variable that holds switch value
    resultvarname=enable_$1

    AC_MSG_CHECKING([whether to enable support for $1])

    AC_ARG_ENABLE([$1], AS_HELP_STRING([--$3-$1],
        [$3 support for $1 [default=$4]]), [],
        [eval "$resultvarname=$default"])

    eval "result=\$$resultvarname"
    AS_IF([test "x$result" = "xno"], [
        HAVE_$2=0
        AC_MSG_RESULT([no])
        dnl action-disabled
        $8
    ], [
        AC_MSG_RESULT([yes])
        PKG_CHECK_MODULES($1, [$5], [
            HAVE_$2=1
            dnl action-found
            $6
            dnl export $upper-case-name_CFLAGS and $upper-case-name_LIBS
            $2_CFLAGS="$$1_CFLAGS"
            $2_LIBS="$$1_LIBS"
            AC_SUBST($2_CFLAGS)
            AC_SUBST($2_LIBS)
        ], [
            HAVE_$2=0
            dnl action-notfound
            $7
            AS_IF([test "x$result" = "xyes"], [
                AC_MSG_ERROR([Module \"$1\" requested but not available])
            ])
        ])
    ])

    AC_DEFINE_UNQUOTED([HAVE_$2], [$HAVE_$2],
        [Define to 1 if you would like to enable $1])

    AM_CONDITIONAL([USE_$2], [test "x$HAVE_$2" = "x1"])

])

# AX_WITH_MODULE(name, upper-case-name, switch-type, default, modules,
#                action-found, action-notfound, action-disabled)
#
# Adds a switch for a program module (or group of modules) that can be
# found with pkg-config. If the switch is enabled, the macro checks if
# the specified modules are available. This is the same macro as
# AX_ENABLE_MODULE except that the created switch is
# --with-name/--without-name.
#
# This macro sets and defines variable HAVE_$upper-case-name to 1 if
# the group of modules is enabled and available, 0 otherwise.
# Variables $name_CFLAGS and $name_LIBS are exported for use in Makefile.am.
# Automake conditional variable USE_$upper-case-name is also set.
# If the module is enabled by the user but not available, an error
# is thrown.
#
# @param 1_name            name of the module
# @param 2_upper-case-name upper-case name of the module
# @param 3_switch-type     switch type: [with] or [without]
# @param 4_default         default value: [yes] or [no]
# @param 5_modules         pkg-config module specifications
# @param 6_action-found    executed if module is enabled and found
# @param 7_action-notfound executed if module is enabled but not found
# @param 8_action-disabled executed if module is disabled
#
AC_DEFUN([AX_WITH_MODULE], [
    dnl Set default value according to switch type
    AS_IF([test "x$3" = "xwith"], [
        AS_IF([test "x$4" = "xno"],
            [default="no"], [default=""])
    ], [
        AS_IF([test "x$4" = "xno"],
            [default=""], [default="no"])
    ])

    dnl Name of the variable that holds switch value
    resultvarname=with_$1

    AC_MSG_CHECKING([whether to build with support for $1])
    AC_ARG_WITH([$1], AS_HELP_STRING([--$3-$1],
        [build $3 support for $1 [default=$4]]), [],
        [eval "$resultvarname=$default"])

    eval "result=\$$resultvarname"
    AS_IF([test "x$result" = "xno"], [
        HAVE_$2=0
        AC_MSG_RESULT([no])
        dnl action-disabled
        $8
    ], [
        AC_MSG_RESULT([yes])
        PKG_CHECK_MODULES($1, [$5], [
            HAVE_$2=1
            dnl action-found
            $6
            dnl export $upper-case-name_CFLAGS and $upper-case-name_LIBS
            $2_CFLAGS="$$1_CFLAGS"
            $2_LIBS="$$1_LIBS"
            AC_SUBST($2_CFLAGS)
            AC_SUBST($2_LIBS)
        ], [
            HAVE_$2=0
            dnl action-notfound
            $7
            AS_IF([test "x$result" = "xyes"], [
                AC_MSG_ERROR([Module \"$1\" requested but not available])
            ])
        ])
    ])

    AC_DEFINE_UNQUOTED([HAVE_$2], [$HAVE_$2],
        [Define to 1 if you would like to enable $1])

    AM_CONDITIONAL([USE_$2], [test "x$HAVE_$2" = "x1"])

])


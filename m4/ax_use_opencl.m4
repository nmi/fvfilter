
# AX_USE_OPENCL(switch-type, default,
#               action-available, action-unavailable, action-disabled)
#
# Adds a with/without switch for OpenCL support.
# This macro sets and defines HAVE_OPENCL to 1 if OpenCL support is
# enabled and available, or to 0 otherwise. Automake conditional
# USE_OPENCL is also defined. OpenCL libraries are added to the LIBS list.
#
# @param 1_switch-type        switch type: [with] or [without]
# @param 2_default            default value: [yes] or [no]
# @param 3_action-available   executed if OpenCL support is
#                             available and enabled
# @param 4_action-unavailable executed if OpenCL support is not available
# @param 5_action-disabled    executed if OpenCL support is disabled
#
AC_DEFUN([AX_USE_OPENCL], [
    dnl Set default value according to the switch type
    AS_IF([test "x$1" = "xwith"], [
        AS_IF([test "x$2" = "xno"], [default="no"], [default=""])
    ], [
        AS_IF([test "x$2" = "xno"], [default=""], [default="no"])
    ])
    AC_ARG_WITH([opencl], AS_HELP_STRING([--$1-opencl],
        [build $1 support for OpenCL [default=$2]]), [],
        [with_opencl="$default"])

    HAVE_OPENCL=0
    AS_IF([test "x$with_opencl" = "xno"], [
        AC_MSG_RESULT([no])
        dnl action-disabled
        $5
    ], [ 
        message=""
        dnl Find the library
        libs_found=0
        AC_SEARCH_LIBS([clCreateContext], [OpenCL], [
            libs_found=1
            AC_MSG_CHECKING([for OpenCL libraries])
            AC_MSG_RESULT([yes])
        ], [
            AC_MSG_CHECKING([for OpenCL libraries])
            AC_MSG_RESULT([no])
            message="libraries are missing.
        Install OpenCL-enabled display drivers for your GPU or
        get an appropriate OpenCL SDK."
            AS_IF([test "x$with_opencl" = "xyes"], [
                AC_MSG_ERROR([
        OpenCL support requested but $message])
            ], [
                AC_MSG_WARN([
        OpenCL support disabled because $message])
            ])
        ])

        dnl Check for headers
        headers_found=0
        AC_CHECK_HEADERS([CL/opencl.h OpenCL/opencl.h], [
            headers_found=1
            break;
        ])
        AC_MSG_CHECKING([for OpenCL headers])
        AS_IF([test "x$headers_found" = "x0"], [
            AC_MSG_RESULT([no])
            message="headers are missing.
        Install a headers package or an OpenCL SDK. For example on
        Debian/Ubuntu, OpenCL headers are in package opencl-headers"
            AS_IF([test "x$with_opencl" = "xyes"], [
                AC_MSG_ERROR([
        OpenCL support requested but $message])
            ], [
                AC_MSG_WARN([
        OpenCL support disabled because $message])
            ])
        ], [
            AC_MSG_RESULT([yes])
        ])

        AS_IF([test "x$headers_found$libs_found" = "x11"], [
            HAVE_OPENCL=1
            dnl action-available
            $3
        ], [
            HAVE_OPENCL=0
            dnl action-unavailable
            $4
        ])
    ])

    AC_DEFINE_UNQUOTED([HAVE_OPENCL], $HAVE_OPENCL,
        [Define to 1 if you would like to enable OpenCL support])

    AM_CONDITIONAL([USE_OPENCL], [test "x$HAVE_OPENCL" = "x1"])

])


#!/bin/sh
#
# libtool assumes that the compiler can handle the -fPIC flag, which
# yasm does not do
command=""
while [ $# -gt 0 ]; do
    case "$1" in
        -fPIC)
            # Ignore -fPIC
            ;;
        *)
            command="$command $1"
            ;;
    esac
    shift
done
echo $command
exec $command

;
; @file      convert.asm
; @brief     x86 SIMD code for numeric conversions.
; @author    Niko Mikkilä <nm@phnet.fi>
; @copyright GNU Lesser General Public License version 2.1 or
;            (at your option) any later version
;

%include "x86inc.asm"

%define USE_SSSE3 1

SECTION_RODATA

shuffle_bytes_to_pd: db 0x00,0xff,0xff,0xff,0x01,0xff,0xff,0xff, \
                        0x02,0xff,0xff,0xff,0x03,0xff,0xff,0xff

; uint32_t bytes_to_float_mask[4] = {((127+23  ) << 23) + 0xff00,
;                                    ((127+23-8) << 23) + 0x00ff,
;                                    ((127+23  ) << 23) + 0xff00,
;                                    ((127+23-8) << 23) + 0x00ff};
low16_ps_mask:    ddq 0x0000ffff0000ffff0000ffff0000ffff
u8_to_float_mask: ddq 0x470000ff4b00ff00470000ff4b00ff00

SECTION .text

; Benchmarks
; ----------
;
; Convert 1008 uint8 values to float, cycles counted with PMCTest
; 
;                                  Merom
; fv_u8_to_float_sse:               1407
; fv_u8_to_float_unrolled_sse:       985
; fv_u8_to_float_sse2:              1151
; fv_u8_to_float_mmx_sse2:          1248
; fv_u8_to_float_unrolled_mmx_sse2:  966
; fv_u8_to_float_ssse3:              922
; fv_u8_to_float_unrolled_ssse3:     705


INIT_XMM sse 

; void fv_u8_to_float_sse(float *dst, const uint8_t *src, size_t n)
;
cglobal u8_to_float, 3, 3, 3, dst, src, n
    movaps     m0, [u8_to_float_mask]
    movaps     m1, [low16_ps_mask]
.loop:
    movss      m3, [srcq]                 ; m3 <- 0000 0000 0000 dcba
    movss      m2, [srcq+2]               ; m2 <- 0000 0000 0000 fedc
    shufps     m3, m2, 0                  ; m3 <- fedc fedc dcba dcba
    andps      m3, m1                     ; m3 <- 00dc 00dc 00ba 00ba
    orps       m3, m0                     ; m3 <-  d.F Fc.0  b.F Fa.0
    subps      m3, m0                     ; m3 <-  d.0  c.0  b.0  a.0

    add      srcq, 4
    movaps [dstq], m3
    add      dstq, 16
    sub        nq, 4
    jg .loop
    REP_RET

%macro u8_to_float_t1 0
    lea        r0, [UserData]
    lea        r1, [UserData+1024]
    mov        r2, 1024

    movaps     m0, [u8_to_float_mask]
    movaps     m1, [low16_ps_mask]
%%loop:
    movss      m3, [r0]                   ; m3 <- 0000 0000 0000 dcba
    movss      m2, [r0+2]                 ; m2 <- 0000 0000 0000 fedc
    shufps     m3, m2, 0                  ; m3 <- fedc fedc dcba dcba
    andps      m3, m1                     ; m3 <- 00dc 00dc 00ba 00ba
    orps       m3, m0                     ; m3 <-  d.F Fc.0  b.F Fa.0
    subps      m3, m0                     ; m3 <-  d.0  c.0  b.0  a.0

    add      r0, 4
    movaps [r1], m3
    add      r1, 16
    sub        r2, 4
    jg %%loop
%endmacro

%macro u8_to_float_t2 0
    lea        r0, [UserData]
    lea        r1, [UserData+1024]
    mov        r2, 1024
    push       r2
    push       r1
    push       r0
    call       u8_to_float_unrolled_sse
    add        esp, 12
%endmacro

; void fv_u8_to_float_unrolled_sse(float *dst, const uint8_t *src, size_t n)
;
cglobal u8_to_float_unrolled, 3, 3, 7, dst, src, n
    movaps     m0, [u8_to_float_mask]
    movaps     m1, [low16_ps_mask]
.loop:
    movss      m3, [srcq]
    movss      m2, [srcq+2]
    movss      m4, [srcq+4]
    shufps     m3, m2, 0
    movss      m5, [srcq+8]
    movss      m2, [srcq+6]
    andps      m3, m1
    shufps     m4, m2, 0
    movss      m6, [srcq+12]
    movss      m2, [srcq+10]
    andps      m4, m1
    shufps     m5, m2, 0
    movss      m2, [srcq+14]
    andps      m5, m1
    shufps     m6, m2, 0
    orps       m3, m0
    andps      m6, m1
    orps       m4, m0
    orps       m5, m0
    orps       m6, m0
    subps      m3, m0
    subps      m4, m0
    subps      m5, m0
    subps      m6, m0
    add      srcq, 16
    movaps  [dstq   ], m3
    movaps  [dstq+16], m4
    movaps  [dstq+32], m5
    movaps  [dstq+48], m6
    add      dstq, 64
    sub        nq, 16
    jg .loop
    REP_RET


INIT_XMM sse2


; void fv_u8_to_float_sse2(float *dst, const uint8_t *src, size_t n)
;
; 10% slower than fv_u8_to_float_unrolled_sse()
;
cglobal u8_to_float, 3, 3, 5, dst, src, n
    xorps      m0, m0
    movaps     m4, [u8_to_float_mask]

.loop:
    movdqu     m2, [srcq]         ; m2 <- ponm lkji hgfe dcba
    add      srcq, 16
    movdqa     m1, m2             ; m1 <- m2

    punpcklwd  m2, m0             ; m2 <- 00hg 00fe 00dc 00ba
    movdqa     m3, m2             ; m3 <- m2
    shufps     m2, m2, 01010000b  ; m2 <- 00dc 00dc 00ba 00ba
    shufps     m3, m3, 11111010b  ; m3 <- 00hg 00hg 00fe 00fe
    orps       m2, m4             ; m2 <-  h.F Fg.0  f.F Fe.0 (F=0xFF)
    orps       m3, m4             ; m3 <-  d.F Fc.0  b.F Fa.0
    subps      m2, m4             ; m2 <-  h.0  g.0  f.0  e.0
    subps      m3, m4             ; m3 <-  d.0  c.0  b.0  a.0
    movaps  [dstq   ], m2
    movaps  [dstq+16], m3

    punpckhwd  m1, m0             ; m1 <- 00po 00nm 00lk 00ji
    movdqa     m3, m1             ; m3 <- m1
    shufps     m1, m1, 01010000b  ; m1 <- 00lk 00lk 00ji 00ji
    shufps     m3, m3, 11111010b  ; m3 <- 00po 00po 00nm 00nm
    orps       m1, m4             ; m1 <-  p.F Fo.0  n.F Fm.0
    orps       m3, m4             ; m3 <-  l.F Fk.0  j.F Fi.0
    subps      m1, m4             ; m1 <-  p.0  o.0  n.0  m.0
    subps      m3, m4             ; m3 <-  l.0  k.0  j.0  i.0
    movaps  [dstq+32], m1
    movaps  [dstq+48], m3

    add      dstq, 64
    sub        nq, 16
    jg .loop
    REP_RET

; void fv_u8_to_float_mmx_sse2(float *dst, const uint8_t *src, size_t n)
;
; 8% slower than fv_u8_to_float_unrolled_sse() on Core 2.
;
cglobal u8_to_float_mmx, 3, 3, 2, dst, src, n
    movaps     m0, [u8_to_float_mask]
.loop:
    movd      mm1, [srcq]            ; mm0 <- 0000 dcba
    pshufw    mm1, mm1, 11011000b    ; mm0 <- 00dc 00ba
    movq2dq    m1, mm1
    shufps     m1, m1, 01010000b     ;  m1 <- 00dc 00dc 00ba 00ba
    orps       m1, m0                ;  m1 <-  d.F Fc.0  b.F Fa.0
    subps      m1, m0                ;  m1 <-  d.0  c.0  b.0  a.0
    add      srcq, 4
    movaps  [dstq], m1
    add      dstq, 16
    sub        nq, 4
    jg .loop
    REP_RET

; void fv_u8_to_float_unrolled_mmx_sse2(float *dst, const uint8_t *src, size_t n)
;
; 5% faster than fv_u8_to_float_unrolled_sse() on Core 2.
;
cglobal u8_to_float_unrolled_mmx, 3, 3, 5, dst, src, n
    movaps     m0, [u8_to_float_mask]
.loop:
    movd      mm1, [srcq]
    movd      mm2, [srcq+4]
    movd      mm3, [srcq+8]
    movd      mm4, [srcq+12]
    pshufw    mm1, mm1, 11011000b
    pshufw    mm2, mm2, 11011000b
    pshufw    mm3, mm3, 11011000b
    pshufw    mm4, mm4, 11011000b
    movq2dq    m1, mm1
    movq2dq    m2, mm2
    movq2dq    m3, mm3
    movq2dq    m4, mm4
    shufps     m1, m1, 01010000b
    shufps     m2, m2, 01010000b
    shufps     m3, m3, 01010000b
    shufps     m4, m4, 01010000b
    orps       m1, m0
    orps       m2, m0
    orps       m3, m0
    orps       m4, m0
    subps      m1, m0
    subps      m2, m0
    subps      m3, m0
    subps      m4, m0
    movaps  [dstq   ], m1
    movaps  [dstq+16], m2
    movaps  [dstq+32], m3
    movaps  [dstq+48], m4
    add      srcq, 16
    add      dstq, 64
    sub        nq, 16
    jg .loop
    REP_RET


; void fv_u8_to_u16_sse2(uint16_t *dst, const uint8_t *src, size_t n)
;
cglobal u8_to_u16, 3, 3, 3, dst, src, n
    xorps      m1, m1
    xorps      m2, m2
.loop:
    ; Use unaligned load since src might be non-mod16 or aligned to 8 bytes
    movdqu     m0, [srcq]
    punpcklbw  m1, m0
    punpckhbw  m2, m0
    movdqa  [dstq   ], m1
    movdqa  [dstq+16], m2
    add      srcq, 16
    add      dstq, 32
    sub        nq, 16
    jg .loop
    REP_RET

    
INIT_XMM ssse3


; void fv_u8_to_float_ssse3(float *dst, const uint8_t *src, size_t n)
;
cglobal u8_to_float, 3, 3, 2, dst, src, n
    mova       m0, [shuffle_bytes_to_pd]
.loop:
    movss      m1, [srcq]
    pshufb     m1, m0
    cvtdq2ps   m1, m1
    add      srcq, 4
    movaps [dstq], m1
    add      dstq, 16
    sub        nq, 4
    jg .loop
    REP_RET


; void fv_u8_to_float_unrolled_ssse3(float *dst, const uint8_t *src, size_t n)
;
cglobal u8_to_float_unrolled, 3, 3, 5, dst, src, n
    mova       m0, [shuffle_bytes_to_pd]
.loop:
    movss      m1, [srcq]
    movss      m2, [srcq+4]
    movss      m3, [srcq+8]
    movss      m4, [srcq+12]
    pshufb     m1, m0
    pshufb     m2, m0
    pshufb     m3, m0
    pshufb     m4, m0
    cvtdq2ps   m1, m1
    cvtdq2ps   m2, m2
    cvtdq2ps   m3, m3
    cvtdq2ps   m4, m4
    add      srcq, 16
    movaps  [dstq   ], m1
    movaps  [dstq+16], m2
    movaps  [dstq+32], m3
    movaps  [dstq+48], m4
    add      dstq, 64
    sub        nq, 16
    jg .loop
    REP_RET



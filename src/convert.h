/**
 * @file      convert.h
 * @brief     SIMD-optimized conversion functions
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef CONVERT_H
#define CONVERT_H

#include <stdio.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Copies and converts an array of bytes to floats.
 *
 * @param dst destination buffer
 * @param src source buffer
 * @param n   number of bytes to convert
 */
void fv_u8_to_float_c(float *dst, const uint8_t *src, size_t n);

/**
 * Copies and converts an array of bytes to floats.
 * Uses SSE instructions (Pentium III and later).
 *
 * Note! This implementation overreads src by 2 bytes.
 *
 * @param dst destination buffer
 * @param src source buffer
 * @param n   number of bytes to convert
 */
void fv_u8_to_float_sse(float *dst, const uint8_t *src, size_t n);

/**
 * Copies and converts an array of bytes to floats.
 * This is a faster, unrolled version of fv_u8_to_float_sse().
 * Uses SSE instructions (Pentium III and later).
 *
 * Note! This implementation overreads src by 2 bytes. n should be mod 16. 
 *
 * @param dst destination buffer
 * @param src source buffer
 * @param n   number of bytes to convert, multiple of 16.
 */
void fv_u8_to_float_unrolled_sse(float *dst, const uint8_t *src, size_t n);

void fv_u8_to_float_sse2(float *dst, const uint8_t *src, size_t n);

void fv_u8_to_float_mmx_sse2(float *dst, const uint8_t *src, size_t n);
void fv_u8_to_float_unrolled_mmx_sse2(float *dst, const uint8_t *src, size_t n);
void fv_u8_to_float_ssse3(float *dst, const uint8_t *src, size_t n);
void fv_u8_to_float_unrolled_ssse3(float *dst, const uint8_t *src, size_t n);

void fv_u8_to_u16_sse2(uint16_t *dst, const uint8_t *src, size_t n);


#ifdef __cplusplus
}
#endif

#endif /* CONVERT_H */


/**
 * @file      convert_c.c
 * @brief     C implementations of conversion functions
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include <stdio.h>
#include <stdint.h>

#include "convert.h"

void fv_u8_to_float_c(float *dst, const uint8_t *src, size_t n) {
    size_t i;
    for (i = 0; i < n; i++) dst[i] = (float) src[i];
}



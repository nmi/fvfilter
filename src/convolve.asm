;
; @file      convolve.asm
; @brief     x86 SIMD implementations of certain convolution functions
; @author    Niko Mikkilä <nm@phnet.fi>
; @copyright GNU Lesser General Public License version 2.1 or
;            (at your option) any later version
;


%include "x86inc.asm"

%define USE_SSSE3 1

SECTION_RODATA

shuffle_bytes_to_pd: db 0x00,0xff,0xff,0xff,0x01,0xff,0xff,0xff, \
                        0x02,0xff,0xff,0xff,0x03,0xff,0xff,0xff

SECTION .text


INIT_XMM sse

    
%macro CONVOLVE_1x5_SYM_FLOAT_MULADD 2
    movss     m0, %1
    movss     m1, m0
    shufps    m1, m1, 0x39   ; rotate
    movaps    m0, m1
    mulps     m0, %2
    addps     m2, m0
%endmacro

cglobal convolve_1x5_sym_float, 3, 3, 6, img, width, kernel
    movaps    m3, [kernelq]
    movaps    m4, [kernelq+16]
    movaps    m5, [kernelq+32]
    movaps    m1, [imgq] 
    shufps    m1, m1, 0x80   ; {img[1], img[0], img[0], img[0]}
    sub   widthq, 4
    jmp   .start 

.loop:
    CONVOLVE_1x5_SYM_FLOAT_MULADD [imgq + 16], m4
    CONVOLVE_1x5_SYM_FLOAT_MULADD [imgq + 20], m5

    movaps [imgq], m2
    add     imgq, 16

.start:
    movaps    m2, m1
    mulps     m2, m5

    CONVOLVE_1x5_SYM_FLOAT_MULADD [imgq +  8], m4
    CONVOLVE_1x5_SYM_FLOAT_MULADD [imgq + 12], m3

    sub   widthq, 4
    jg     .loop

    CONVOLVE_1x5_SYM_FLOAT_MULADD [imgq + 12], m4
    CONVOLVE_1x5_SYM_FLOAT_MULADD [imgq + 12], m5
    movaps [imgq], m2
    RET


%macro CONVOLVE_5x1_SYM_FLOAT_PROCESS_ROW_OLD 0
    mulps     m0, m7 ; kernel[2]
    movaps    m5, m1
    mulps     m5, m6 ; kernel[1]
    addps     m0, m5
    movaps    m5, m2
    mulps     m5, [kernelq] ; kernel[0]
    addps     m0, m5
    movaps    m5, m3
    mulps     m5, m6 ; kernel[1]
    addps     m0, m5
    movaps    m5, m4
    mulps     m5, m7 ; kernel[2]
    addps     m0, m5
    movaps [imgq], m0
    add     imgq, strideq
%endmacro

%macro CONVOLVE_5x1_SYM_FLOAT_PROCESS_ROW 0
    addps     m0, m4
    mulps     m0, m7 ; kernel[2]
    movaps    m5, m1
    addps     m5, m3
    mulps     m5, m6 ; kernel[1]
    addps     m0, m5
    movaps    m5, m2
    mulps     m5, [kernelq] ; kernel[0]
    addps     m0, m5
    movaps [imgq], m0
    add     imgq, strideq
%endmacro

%macro CONVOLVE_5x1_SYM_FLOAT_NEXT 0
    SWAP 0,1,2,3,4
    dec       heightq
    jz .done
    movaps    m4, [imgq+2*strideq]
%endmacro

; void convolve_5x1_sym_float_4(float *img, size_t stride,
;                               size_t height, float *kernel)
%macro CONVOLVE_5x1_SYM_FLOAT 1
cglobal convolve_5x1_sym_float_%1, 4, 4, 8, img, stride, height, kernel
    movaps    m6, [kernelq+16]
    movaps    m7, [kernelq+32]
    movaps    m2, [imgq]
    ; Extrapolate two rows at the top of the image by copying the first row
    movaps    m0, m2
    movaps    m1, m2
    movaps    m3, [imgq+strideq]
    movaps    m4, [imgq+2*strideq]
    sub  heightq, 3

.loop:
    CONVOLVE_5x1_SYM_FLOAT_PROCESS_ROW
    CONVOLVE_5x1_SYM_FLOAT_NEXT
    CONVOLVE_5x1_SYM_FLOAT_PROCESS_ROW
    CONVOLVE_5x1_SYM_FLOAT_NEXT
    CONVOLVE_5x1_SYM_FLOAT_PROCESS_ROW
    CONVOLVE_5x1_SYM_FLOAT_NEXT
    CONVOLVE_5x1_SYM_FLOAT_PROCESS_ROW
    CONVOLVE_5x1_SYM_FLOAT_NEXT
    CONVOLVE_5x1_SYM_FLOAT_PROCESS_ROW
    CONVOLVE_5x1_SYM_FLOAT_NEXT
    jmp .loop
.done:

    ; Extrapolate two rows at the bottom of the image by copying the last row
    SWAP 0,1,2,3,4
    movaps    m4, [imgq+strideq]
    CONVOLVE_5x1_SYM_FLOAT_PROCESS_ROW
    SWAP 0,1,2,3,4
    movaps    m4, [imgq]
    CONVOLVE_5x1_SYM_FLOAT_PROCESS_ROW

    REP_RET
%endmacro
 
CONVOLVE_5x1_SYM_FLOAT 4



cglobal multiply_float_row, 4, 4, 2, dst, src, width, kernel
    movaps     m1, [kernelq]
.loop
    movaps     m0, [srcq]
    mulps      m0, m1
    movaps [dstq], m0 
    add      dstq, 16
    add      srcq, 16
    sub    widthq, 4
    jg      .loop
    REP_RET

cglobal multiply_add_float_row, 4, 4, 2, dst, src, width, kernel
    movaps     m1, [kernelq]
.loop
    movaps     m0, [srcq]
    mulps      m0, m1
    addps      m0, [dstq] 
    movaps [dstq], m0
    add      dstq, 16
    add      srcq, 16
    sub    widthq, 4
    jg      .loop
    REP_RET

INIT_XMM sse2

cglobal multiply_add_unaligned_float_row, 4, 4, 2, dst, src, width, kernel
    movaps     m1, [kernelq]
.loop
    movdqu     m0, [srcq]
    mulps      m0, m1
    addps      m0, [dstq] 
    movaps [dstq], m0
    add      dstq, 16
    add      srcq, 16
    sub    widthq, 4
    jg      .loop
    REP_RET


cglobal multiply_u16_row, 4, 4, 2, dst, src, width, kernel
    movdqa     m1, [kernelq]
.loop
    movdqa     m0, [srcq]
    pmulhuw     m0, m1
    movdqa [dstq], m0 
    add      dstq, 16
    add      srcq, 16
    sub    widthq, 8
    jg      .loop
    REP_RET

cglobal multiply_add_u16_row, 4, 4, 5, dst, src, width, kernel
    movdqa     m0, [kernelq]
.loop
    movdqa     m1, [srcq]
    movdqa     m2, [srcq+16]
    movdqa     m3, [srcq+32]
    movdqa     m4, [srcq+48]
    pmulhuw    m1, m0
    pmulhuw    m2, m0
    pmulhuw    m3, m0
    pmulhuw    m4, m0
    paddusw    m1, [dstq] 
    paddusw    m2, [dstq+16] 
    paddusw    m3, [dstq+32]
    paddusw    m4, [dstq+48] 
    movdqa [dstq], m1
    movdqa [dstq+16], m2
    movdqa [dstq+32], m3
    movdqa [dstq+48], m4
    add      dstq, 64
    add      srcq, 64
    sub    widthq, 32
    jg      .loop
    REP_RET

cglobal multiply_add_u16_row_single, 4, 4, 2, dst, src, width, kernel
    movdqa     m1, [kernelq]
.loop
    movdqa     m0, [srcq]
    pmulhuw     m0, m1
    paddusw     m0, [dstq] 
    movdqa [dstq], m0
    add      dstq, 16
    add      srcq, 16
    sub    widthq, 8
    jg      .loop
    REP_RET


cglobal multiply_add_unaligned_u16_row_single, 4, 4, 2, dst, src, width, kernel
    movdqa     m1, [kernelq]
.loop
    movdqu     m0, [srcq]
    pmulhuw    m0, m1
    paddusw    m0, [dstq] 
    movdqa [dstq], m0
    add      dstq, 16
    add      srcq, 16
    sub    widthq, 8
    jg      .loop
    REP_RET

cglobal multiply_add_unaligned_u16_row, 4, 4, 5, dst, src, width, kernel
    movdqa     m0, [kernelq]
.loop
    movdqu     m1, [srcq]
    movdqu     m2, [srcq+16]
    movdqu     m3, [srcq+32]
    movdqu     m4, [srcq+48]
    pmulhuw    m1, m0
    pmulhuw    m2, m0
    pmulhuw    m3, m0
    pmulhuw    m4, m0
    paddusw    m1, [dstq] 
    paddusw    m2, [dstq+16] 
    paddusw    m3, [dstq+32] 
    paddusw    m4, [dstq+48] 
    movdqa [dstq], m1
    movdqa [dstq+16], m2
    movdqa [dstq+32], m3
    movdqa [dstq+48], m4
    add      dstq, 64
    add      srcq, 64
    sub    widthq, 32
    jg      .loop
    REP_RET



cglobal unsharp_mask_u16, 5, 5, 6, dst, src, blurred_src, amount, n
    xorps      m0, m0
.loop:
    ; Use unaligned load since src and dst might be non-mod16
    movdqu     m1, [srcq]
    movdqa     m2, m1
    punpcklbw  m1, m0
    punpckhbw  m2, m0
    movdqa     m3, m1
    movdqa     m4, m2
    psllw      m3, 7
    psllw      m4, 7
    movdqa     m5, [blurred_srcq]
    psrlw      m5, 1
    psubsw     m3, m5
    movdqa     m5, [blurred_srcq+16]
    psrlw      m5, 1
    psubsw     m4, m5
    pmulhw     m3, [amountq]
    pmulhw     m4, [amountq]
    paddsw     m3, m1
    paddsw     m4, m2
    packuswb   m3, m4
    movdqu [dstq], m3
    add      srcq, 16
    add      dstq, 16
    add blurred_srcq, 32
    sub        nq, 16
    jg      .loop
    REP_RET
    
cglobal unsharp_mask_float, 5, 5, 6, dst, src, blurred_src, amount, n
    movaps     m0, [amountq]
.loop:
    movaps     m1, [srcq] 
    movaps     m2, m1
    subps      m1, [blurred_srcq]
    mulps      m1, m0
    addps      m1, m2
    
    add      srcq, 16
    add      dstq, 16
    add blurred_srcq, 16
    sub        nq, 4
    jg      .loop
    REP_RET
 


/**
 * @file      convolve.h
 * @brief     Convolution functions
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef CONVOLVE_H
#define CONVOLVE_H

#include <stdint.h>

#ifdef __cplusplus
#include <cstring>
extern "C" {
#endif


void fv_multiply_float_row_sse(float *dst, const float *src, size_t width, const float *kernel);
void fv_multiply_add_float_row_sse(float *dst, const float *src, size_t width, const float *kernel);
void fv_multiply_add_unaligned_float_row_sse2(float *dst, const float *src, size_t width, const float *kernel);

void fv_multiply_u16_row_sse2(uint16_t *dst, const uint16_t *src, size_t width, const uint16_t *kernel);
void fv_multiply_add_u16_row_sse2(uint16_t *dst, const uint16_t *src, size_t width, const uint16_t *kernel);
void fv_multiply_add_unaligned_u16_row_sse2(uint16_t *dst, const uint16_t *src, size_t width, const uint16_t *kernel);

void fv_unsharp_mask_u16_sse2(uint8_t *dst, const uint8_t *src,
                              const uint16_t *blurred_src,
                              const int16_t *amount, unsigned int n);


void fv_convolve_1x5_sym_float_u8_sse(float *dst, const uint8_t *src, size_t width, float *kernel);
void fv_convolve_1x5_sym_float_sse(float *img, size_t width, float *kernel);


/**
 * Vertical in-place 1x5 convolution using a symmetrical
 * kernel to process float data.
 *
 * @param img    image buffer to process
 * @param stride row width in bytes
 * @param height image height in sample rows
 * @param kernel three values from the convolution kernel: [0]=center,
 *               [1]=center+1 and [2]=center+2.
 */
void fv_convolve_5x1_sym_float_4_sse(float *img, size_t stride,
                                     size_t height, float *kernel);

#ifdef __cplusplus
}
#endif

#endif /* CONVOLVE_H */


/**
 * @file      fvconvolve2d.c
 * @brief     Filter for two-dimensional image convolution.
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "fvfilter.h" 
#include "fvmultiarray.h" 
#include "fvutil.h" 
#include "fvvalue.h" 
#include "convert.h" 
#include "convolve.h" 


enum ParamId { 
    PARAM_0,
    PARAM_KERNEL_Y,
    PARAM_KERNEL_CB,
    PARAM_KERNEL_CR,
    PARAM_NORMALIZE,
    PARAM_CPU_FEATURES,
    PARAM_OPTIMIZATION,
    PARAM_LAST
}; 

enum Optimizations {
    OPT_AUTO,
    OPT_FLOAT,
    OPT_U16,
    OPT_FLOAT_SIMD,
    OPT_U16_SIMD,
    OPT_LAST
};

static const FvEnum optimizations[] = {
    {OPT_AUTO,       "automatic",           "auto"      },
    {OPT_FLOAT,      "floating point",      "float"     },
    {OPT_FLOAT_SIMD, "floating point SIMD", "float_simd"},
    {OPT_U16_SIMD,   "16-bit integer SIMD", "int16_simd"},
    {0,              NULL,                  NULL        }
};

#ifdef DEBUG
static const FvEnum *get_optimizations(void) {
    return optimizations;
}
#endif


static FvValueConstraint kernel_constraint = {
        .defval.f = 0.0F,
        .min.f = -INFINITY,
        .max.f = INFINITY,
        .list = NULL,
        .type = FV_TYPE_FLOAT
};

static const char *default_kernel_y_string = "1";
static const char *default_kernel_cb_string = "1";
static const char *default_kernel_cr_string = "1";


/**
 * @internal Filter runtime context
 */
typedef struct PrivateContext {
    FvVectorData frame;

    FvVectorParams vector;

    int (*process_plane)(struct PrivateContext *pc, FvMultiArray *kernel,
                         const uint8_t *in, uint8_t *out,
                         union FvValuePtr buf,
                         uint32_t width, uint32_t height);

    /* Parameter storage */
    FvMultiArray   kernel_y;
    FvMultiArray   kernel_cb;
    FvMultiArray   kernel_cr;
    char          *kernel_y_string;
    char          *kernel_cb_string;
    char          *kernel_cr_string;
    uint32_t normalize;
    uint32_t cpu_features;

    enum Optimizations user_opt;
    enum Optimizations opt;

    /** Maximum amount of vertical padding required by the kernels */
    size_t   padding_v;

    /** Maximum amount of horizontal padding required by the kernels */
    size_t   padding_h;

    uint32_t frame_w, frame_h;

} PrivateContext; 

static int parse_kernel(PrivateContext *pc, FvMultiArray *kernel,
                        const char *input, char **string) {
    FvMultiArray k;
    uint32_t d;
    if (fvmultiarray_from_string(&k, input, &fv_parse_float,
                                 &kernel_constraint,
                                 pc->vector.num_elements, 4,
                                 FV_MULTI_ARRAY_ODD_DIMENSIONS |
                                 FV_MULTI_ARRAY_ALIGN_CENTER)) {
        return 0;
    }
    fvmultiarray_free(kernel);
    if (string) {
        free(*string);
        *string = fvmultiarray_to_string(&k);
    }

    memcpy(kernel, &k, sizeof(FvMultiArray));
    /* Force buffer reinit */
    d = kernel->dimensions[2] / 2;
    if (pc->padding_v < d) {
        pc->frame_w = 0;
        pc->frame_h = 0;
        pc->padding_v = d;
    }
    d = kernel->dimensions[3] / 2;
    if (pc->padding_h < d) {
        pc->frame_w = 0;
        pc->frame_h = 0;
        pc->padding_h = d;
    }
    return 1;
}


#define pad_to_mod32(n) ((1 + (((n) - 1) >> 5)) << 5)

static int init_buffer(PrivateContext *pc, uint32_t width, uint32_t height,
                       enum FvFormat format) {
    FvVectorData *f = &pc->frame;
    size_t s;
    size_t padding_left;
    size_t padding_right;
    size_t element_bytes = (uint32_t) pc->vector.element_bytes;
    size_t stride;

    (void) format; /* Unused parameter */

    /* Calculate aligned padding for the kernel */
    padding_left = pad_to_mod32(pc->padding_h * element_bytes);
    padding_right = pad_to_mod32((pc->padding_h + width) * element_bytes) -
                    (width * element_bytes);
    stride = (element_bytes * width + padding_left + padding_right);
    fv_log_info("Frame padding: left=%d, right=%d, top=%d, bottom=%d (%d x %d)",
                padding_left, padding_right, pc->padding_v,
                pc->padding_v, width, height);
    fv_log_info("Stride: %d", stride);

    f->format.element_bytes = pc->vector.element_bytes;
    f->format.element_type = pc->vector.element_type;
    f->format.num_elements = width * height;
    s = stride * (height + pc->padding_v + pc->padding_v);

    if (f->format.total_bytes < s) {
        fv_free(f->data.u8);
        f->data.u8 = (uint8_t *) fv_malloc(2 * s);
        f->format.total_bytes = s;
    }

    if (f->data.u8 == NULL) {
        f->format.total_bytes = 0;
        return 0;
    }
    pc->frame_w = width;
    pc->frame_h = height;
    return 1;
}

static int process_plane_float_simd(PrivateContext *pc,
                FvMultiArray *kernel, const uint8_t *in, uint8_t *out,
                union FvValuePtr buf, uint32_t width, uint32_t height) {
    size_t element_bytes = (uint32_t) pc->vector.element_bytes;
    size_t kernel_w = kernel->dimensions[3];
    size_t kernel_h = kernel->dimensions[2];
    size_t u8_padding_left;
    size_t u8_padding_right;
    size_t u8_stride;
    size_t bufsize = pc->frame.format.total_bytes;
    size_t padding_h = kernel_w / 2;
    size_t padding_v = kernel_h / 2;
 
    float *buf1, *buf2;
    float *k;

    size_t i;
    size_t float_padding_left;
    size_t float_stride;
    size_t frame_stride = width * element_bytes;

    /* Calculate aligned padding for the kernel */
    u8_padding_left = pad_to_mod32(padding_h * element_bytes);
    u8_padding_right = pad_to_mod32((padding_h + width) * element_bytes) -
                    (width * element_bytes);
    u8_stride = (element_bytes * width + u8_padding_left + u8_padding_right);
    float_padding_left = u8_padding_left / element_bytes;
    float_stride = u8_stride / element_bytes;

    buf1 = (float *) (buf.u8 + u8_padding_left + u8_stride * padding_v);
    buf2 = (float *) (buf1 + (bufsize / element_bytes));

#define KERNEL_PTR(kernel, x, y, z) ((kernel)->array.data.u8 + ((z) * (kernel)->stride[1] + (y) * (kernel)->stride[2] + (x)) * kernel->array.format.vector_bytes)
        
 
    /* Convert to float */
    for (i = 0; i < height; i++) {
        fv_u8_to_float_ssse3(buf1 + i * float_stride, in + i * width, width);
    }

    /* Pad top and bottom sides by duplicating border pixels */

    for (i = 0; i < kernel_h / 2; i++) {
        memcpy(buf1-(i+1)*float_stride, buf1, frame_stride);
        memcpy(buf1+(height+i)*float_stride, buf1+(height-1)*float_stride,
               frame_stride);
    }

    /* Pad left and right sides by duplicating border pixels */

    for (i = 0; i < height + 2 * padding_v; i++) {
        size_t j;
        float  *p = buf.f + i * float_stride;
        for (j = 0; j < float_padding_left; j++) {
            p[j] = p[float_padding_left];
            p[float_padding_left+width+j] = p[float_padding_left+width-1];
        }
    }

    /* Convolve */

    k = (float *) KERNEL_PTR(kernel, padding_h, padding_v,
                             kernel->dimensions[1] / 2);
    for (i=0; i < height; i++) {
        fv_multiply_float_row_sse(&buf2[i*float_stride],
                &buf1[i*float_stride], width, k);
    }
    k = (float *) KERNEL_PTR(kernel, 0, 0, kernel->dimensions[1] / 2);
    for (i=0; i < kernel_h; i++) {
        size_t j;
        for (j=0; j < kernel_w; j++) {
            size_t y;
            if ((i == padding_v) && (j == padding_h)) continue;
            for (y = 0; y < height; y++) {
                fv_multiply_add_unaligned_float_row_sse2(&buf2[y * float_stride],
                        buf1+(padding_v-i)*float_stride+(padding_h-j)+y*float_stride,
                        width, &k[kernel->array.format.num_elements*(i*kernel_w+j)]);
            }
        }
    }

    for (i=0; i < height; i++) {
        size_t j;
        for (j=0; j < width; j++) {
            out[i*width+j] = (uint8_t) lrintf(buf2[i*float_stride+j]);
        }
    }

    return 1;
}


static void update_opt(PrivateContext *pc) {
    /* Reset buffers and kernel */
    pc->frame.format.total_bytes = 0;

    /* Select actual optimization based on user-selected parameter and
     * available CPU features */
    switch (pc->user_opt) {
        case OPT_AUTO:
#if ARCH_X86
            if (pc->cpu_features & FV_CPU_FLAG_SSE2)
                pc->opt = OPT_U16_SIMD;
            else if (pc->cpu_features & FV_CPU_FLAG_SSE)
                pc->opt = OPT_FLOAT_SIMD;
            else if (pc->cpu_features & FV_CPU_FLAG_MMX)
                pc->opt = OPT_U16_SIMD;
            else
#endif
                pc->opt = OPT_FLOAT;
            break;
        case OPT_FLOAT:
            pc->opt = OPT_FLOAT;
            break;
        case OPT_U16:
            pc->opt = OPT_U16;
            break;
        case OPT_FLOAT_SIMD:
#if ARCH_X86
            if (pc->cpu_features & FV_CPU_FLAG_SSE)
                pc->opt = OPT_FLOAT_SIMD;
            else
#endif
                pc->opt = OPT_FLOAT;
            break;
        case OPT_U16_SIMD:
#if ARCH_X86
            if (pc->cpu_features & (FV_CPU_FLAG_MMX | FV_CPU_FLAG_SSE2))
                pc->opt = OPT_U16_SIMD;
            else
#endif
                /* pc->opt = OPT_U16; */
                pc->opt = OPT_FLOAT;
            break;
        default:
            pc->opt = OPT_FLOAT;
    }


    /* Set data format parameters for intermediate calculations */
    switch (pc->opt) {
        case OPT_U16:
            pc->vector.total_bytes   = 4;
            pc->vector.num_elements  = 2;
            pc->vector.element_bytes = 2;
            pc->vector.element_type  = FV_TYPE_UINT16;
            //pc->process_plane = process_plane_u16;
            pc->process_plane = NULL;
            fv_log_info("fvconvolve2d implementation: int16");
            break;
#if HAVE_ASM
        case OPT_FLOAT_SIMD:
            if (pc->cpu_features & FV_CPU_FLAG_AVX) {
                pc->vector.total_bytes  = 32;
                pc->vector.num_elements = 8;
            } else {
                pc->vector.total_bytes  = 16;
                pc->vector.num_elements = 4;
            }
            pc->vector.element_bytes = 4;
            pc->vector.element_type  = FV_TYPE_FLOAT;
            pc->process_plane = process_plane_float_simd;
            fv_log_info("fvconvolve2d implementation: float_simd");
            break;
        case OPT_U16_SIMD:
            if (pc->cpu_features & FV_CPU_FLAG_SSE2) {
                pc->vector.total_bytes  = 16;
                pc->vector.num_elements = 8;
            } else {
                pc->vector.total_bytes  = 8;
                pc->vector.num_elements = 4;
            }
            pc->vector.element_bytes = 2;
            pc->vector.element_type  = FV_TYPE_UINT16;
            //pc->process_plane = process_plane_u16_simd;
            pc->process_plane = NULL;

            fv_log_info("fvconvolve2d implementation: int16_simd");
            break;
#endif
        case OPT_FLOAT:
        default:
            pc->vector.total_bytes   = 4;
            pc->vector.num_elements  = 1;
            pc->vector.element_bytes = 4;
            pc->vector.element_type  = FV_TYPE_FLOAT;
            //pc->process_plane = process_plane_float;
            pc->process_plane = NULL;
            fv_log_info("fvconvolve2d implementation: float");
            break;
    }

    parse_kernel(pc, &pc->kernel_y, default_kernel_y_string,
                 &pc->kernel_y_string);
    parse_kernel(pc, &pc->kernel_cb, default_kernel_cb_string,
                 &pc->kernel_cb_string);
    parse_kernel(pc, &pc->kernel_cr, default_kernel_cr_string,
                 &pc->kernel_cr_string);
}


static int filter_init(FvFilterContext *fc) {
#ifdef DEBUG
    char *cpu_features_string;
#endif
    PrivateContext *pc = (PrivateContext *) calloc(1, sizeof(PrivateContext));
    if (pc == NULL) return 0;

    fc->filter = &fvconvolve2d;
    fc->priv_size = sizeof(PrivateContext);
    fc->priv = pc;

    pc->kernel_y_string  = NULL;
    pc->kernel_cb_string = NULL;
    pc->kernel_cr_string = NULL;

    pc->normalize = 1;
    pc->padding_h = 0;
    pc->padding_v = 0;
    pc->frame_w = 0;
    pc->frame_h = 0;

    pc->cpu_features = fv_get_cpu_features();
    pc->user_opt = OPT_AUTO;
    update_opt(pc);

#ifdef DEBUG
    cpu_features_string = fv_list_flags(
            fv_get_cpu_feature_flag_descriptions(),
            fv_printable_cpu_features(pc->cpu_features), ",");
    if (cpu_features_string != NULL)
        fv_log_info("Detected CPU features: %s\n", cpu_features_string);
    free(cpu_features_string);
#endif
    return 1;
}

static void filter_uninit(FvFilterContext *fc) {
    PrivateContext *pc = (PrivateContext *) fc->priv;
    if (pc == NULL) return;
    fv_free(pc->frame.data.u8);
    fvmultiarray_free(&pc->kernel_y);
    fvmultiarray_free(&pc->kernel_cb);
    fvmultiarray_free(&pc->kernel_cr);
    free(pc);
    fc->priv = NULL;
}

static int filter_set_input_format(FvFilterContext *fc,
        enum FvFormat format, uint32_t width, uint32_t height) {
    PrivateContext *pc = (PrivateContext *) fc->priv;
    if ((format > FV_FORMAT_0) && (format < FV_FORMAT_LAST)) {
        fc->in_format = format;
    }
    fc->in_width = width;
    fc->in_height = height;    

    if (pc == NULL) return 0;
    return 1;
}

static int filter_set_parameter(FvFilterContext *fc, unsigned int param_id,
        const FvValue *value) {
    int    ret;
    PrivateContext *pc = (PrivateContext *) fc->priv;
    switch (param_id) {
        case PARAM_KERNEL_Y:
            return parse_kernel(pc, &pc->kernel_y, value->value.str,
                                &pc->kernel_y_string);
        case PARAM_KERNEL_CB:
            return parse_kernel(pc, &pc->kernel_cb, value->value.str,
                                &pc->kernel_y_string);
        case PARAM_KERNEL_CR:
            return parse_kernel(pc, &pc->kernel_cr, value->value.str,
                                &pc->kernel_y_string);
        case PARAM_NORMALIZE:
            return fv_value_get_checked_uint32(value, &pc->normalize);
        case PARAM_CPU_FEATURES:
            ret = fv_value_get_checked_flags(value, &pc->cpu_features);
            if (ret) {
                update_opt(pc);
#ifdef DEBUG
                {
                char *cpu_features_string = fv_list_flags(
                        fv_get_cpu_feature_flag_descriptions(),
                        fv_printable_cpu_features(pc->cpu_features), ",");
                fv_log_info("User-selected CPU features: %s\n",
                            cpu_features_string);
                free(cpu_features_string);
                }
#endif
                return 1;
            } else return 0;
        case PARAM_OPTIMIZATION:
            ret = fv_value_get_checked_enum(value, (int32_t *) &pc->user_opt);
            if (ret) {
                update_opt(pc);
                return 1;
            } return 0;
        default:
            return 0;
    }
}

static int filter_get_parameter(FvFilterContext *fc, unsigned int param_id,
        FvValue *value) {
    PrivateContext *pc = (PrivateContext *) fc->priv;
    char *str;
    switch (param_id) {
        case PARAM_NORMALIZE:
            fv_value_set_uint32(value, pc->normalize);
            return 1;
        case PARAM_CPU_FEATURES:
            fv_value_set_flags(value, pc->cpu_features);
            return 1;
        case PARAM_OPTIMIZATION:
            fv_value_set_enum(value, pc->opt);
            return 1;
        case PARAM_KERNEL_Y:
            str = strdup(pc->kernel_y_string);
            fv_value_set_string(value, str);
            return 1;
        case PARAM_KERNEL_CB:
            str = strdup(pc->kernel_cb_string);
            fv_value_set_string(value, str);
            return 1;
        case PARAM_KERNEL_CR:
            str = strdup(pc->kernel_cr_string);
            fv_value_set_string(value, str);
            return 1;
        default:
            return 0;
    }
}


static int filter_process(FvFilterContext *fc,
                          const uint8_t *in, uint8_t *out, size_t size) {
    int ret;
    uint32_t luma_size, chroma_size;
    PrivateContext *pc = (PrivateContext *) fc->priv;

    luma_size = fc->in_width * fc->in_height;
    chroma_size = luma_size >> 2;

    if (3 * fc->in_width * fc->in_height / 2 != size) {
        memcpy(out, in, size);
        return 0;
    }

    if ((pc->frame_w != fc->in_width) || (pc->frame_h != fc->in_height)) {
        ret = init_buffer(pc, fc->in_width, fc->in_height, fc->in_format);
        if (!ret) {
            memcpy(out, in, size);
            return 0;
        }
    }

        pc->process_plane(pc, &pc->kernel_y, in, out,
                          pc->frame.data,
                          fc->in_width, fc->in_height);

        pc->process_plane(pc, &pc->kernel_cb, in + luma_size,
                          out + luma_size,
                          pc->frame.data,
                          fc->in_width >> 1, fc->in_height >> 1);

        pc->process_plane(pc, &pc->kernel_cr, in + luma_size + chroma_size,
                          out + luma_size + chroma_size,
                          pc->frame.data,
                          fc->in_width >> 1, fc->in_height >> 1);
    return 1;
}

FvFilter fvconvolve2d = {
    .name           = "fvconvolve2d",
    .longname       = "2D convolution",
    .classification = "Filter/Effect/Video",
    .description    = "Generic two-dimensional convolution filter",
    .author         = "Niko Mikkilä <nm@phnet.fi>",

    .formats        = (enum FvFormat[]) {FV_FORMAT_I420, FV_FORMAT_YV12, FV_FORMAT_0},
    .parameters     = (const FvParam[]) {
          { PARAM_KERNEL_Y,
            "kernel-y", "Luma kernel",
            "Convolution matrix for luminance as a list of decimal numbers "
            "used as multipliers. "
            "The matrix is square with odd width and height. 25 values "
            "form a 5x5 kernel, and 6 values form a 3x3 kernel with a zero "
            "row at the bottom.",
            FV_PARAM_STRING("") },
          { PARAM_KERNEL_CB,
            "kernel-cb", "Chroma (Cb) kernel",
            "Convolution matrix for Cb chrominance as a list of decimal "
            "numbers. Note that chroma channels in ordinary 4:2:0 video "
            "have half the resolution of luma, so the kernel should also "
            "be smaller.",
            FV_PARAM_STRING("") },
          { PARAM_KERNEL_CR,
            "kernel-cr", "Chroma (Cr) kernel",
            "Convolution matrix for Cr chrominance as a list of decimal "
            "numbers. Note that chroma channels in YV12 video have half "
            "the resolution of luma, so the kernel should also be smaller.",
            FV_PARAM_STRING("") },
          { PARAM_NORMALIZE,
            "normalize", "Normalize kernels",
            "0 to use the kernel without modification, 1 to normalize "
            "the kernel so that the sum of all elements is 1.0",
            FV_PARAM_RANGE_UINT32(0, 1, 1) },
#ifdef DEBUG
          { PARAM_CPU_FEATURES,
            "cpufeatures", "CPU feature flags",
            "Manual override of detected CPU features. Intended to be used "
            "for debugging and filter development.",
            FV_PARAM_FLAGS(fv_get_cpu_feature_flag_descriptions, 0) },
          { PARAM_OPTIMIZATION,
            "opt", "Filter optimization",
            "Manual selection of an optimized filter implementation.",
            FV_PARAM_ENUM(get_optimizations, OPT_AUTO) },
#endif
          FV_PARAM_END
    },
    .init             = filter_init,
    .uninit           = filter_uninit,
    .set_input_format = filter_set_input_format,
    .set_parameter    = filter_set_parameter,
    .get_parameter    = filter_get_parameter,
    .process          = filter_process
};


/**
 * @file      fvfilter.c
 * @brief     Filter API functions
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include "config.h"

#include <assert.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

#include "fvfilter.h"
#include "fvutil.h"
#include "fvvalue.h"


/**
 * @internal Maximum size of a list that holds references to registered
 * filters.
 */
#define FV_MAX_REGISTERED_FILTERS 10


/** @internal Private list of registered filters. */
static FvFilter *registered_filters[FV_MAX_REGISTERED_FILTERS];

/** @internal Number of registered filters. */ 
static int next_registered_filter = 0;

int fvfilter_register(FvFilter *f) {
    if (next_registered_filter >= FV_MAX_REGISTERED_FILTERS) {
        fv_log_error("Maximum number of filters registered (%d). "
                     "Unable to register filter \"%s\"\n",
                FV_MAX_REGISTERED_FILTERS, f->name);
        return ENOMEM;
    }
    registered_filters[next_registered_filter++] = f;
    return 0;
}

void fvfilter_register_all(void) {
    static int initialized;
    if (initialized) return;
    initialized = 1;

    fvfilter_register(&fvsharpen);
    fvfilter_register(&fvconvolve2d);
}

const FvFilter *fvfilter_get_by_name(const char *name) {
    int i;
    for (i = 0; registered_filters[i]; i++) {
        if (!strcmp(registered_filters[i]->name, name))
            return registered_filters[i];
    }
    return NULL;
}

FvFilter **fv_filter_next(FvFilter **filter) {
    return filter ? ++filter : &registered_filters[0];
}


void fv_param_set_default(const FvParam *p, FvValue *v) {
    *v = p->defaultval;
} 


int fv_param_validate(const FvParam *p, FvValue *v) {
    if (p->defaultval.type == v->type) return 0;
    else {
        p->set_default(p, v);
        return 1;
    }
}

int fv_param_validate_enum(const FvParam *p, FvValue *v) {
    const FvEnum *e = fv_param_get_enum_info(p);
    int invalid = 1;

    if (e == NULL) {
        p->set_default(p, v);
        return 1;
    }
    for (; e->name != NULL; e++) {
        if (e->value == v->value.v_enum) invalid = 0;
    }
    if (invalid) p->set_default(p, v);
    return invalid;
}


 
int fv_param_validate_flags(const FvParam *p, FvValue *v) {
    const FvFlag *f = fv_param_get_flags_info(p);
    uint32_t flags = 0;

    if (f == NULL) {
        p->set_default(p, v);
        return 1;
    }
    /* Check that no flags outside spec are set */
    for (; f->name != NULL; f++) {
        flags |= f->value;
    }
    if ((v->value.v_uint32 & flags) != v->value.v_flags) {
        p->set_default(p, v);
        return 1;
    }
    return 0;
}
        

/**
 * @internal Macro to generate range-validating functions for basic types.
 * Validating functions are used through function pointers in parameter
 * specifications.
 *
 * @param uctype upper-case type name as in FV_TYPE_x definitions
 * @param lctype lower-case type names as in FvValue struct union names
 * @param ctype  C type name 
 */
#define DEFINE_FV_PARAM_VALIDATE_RANGE(uctype, lctype, ctype)             \
    int fv_param_validate_range_##lctype(const FvParam *p, FvValue *v) {  \
        const ctype *minmax;                                              \
        ctype val;                                                        \
        if (v->type != FV_TYPE_##uctype) {                                \
            *v = p->defaultval;                                           \
            return 1;                                                     \
        }                                                                 \
        minmax = (const ctype *) p->priv;                                 \
        val = v->value.v_##lctype;                                        \
        if (val < minmax[0]) {                                            \
            v->value.v_##lctype = minmax[0];                              \
            return 1;                                                     \
        } else if (val > minmax[1]) {                                     \
            v->value.v_##lctype = minmax[1];                              \
            return 1;                                                     \
        } else return 0;                                                  \
    }

DEFINE_FV_PARAM_VALIDATE_RANGE(   CHAR,    char,      char)
DEFINE_FV_PARAM_VALIDATE_RANGE(  UINT8,   uint8,   uint8_t)
DEFINE_FV_PARAM_VALIDATE_RANGE(  INT32,   int32,   int32_t)
DEFINE_FV_PARAM_VALIDATE_RANGE( UINT32,  uint32,  uint32_t)
DEFINE_FV_PARAM_VALIDATE_RANGE(  INT64,   int64,   int64_t)
DEFINE_FV_PARAM_VALIDATE_RANGE( UINT64,  uint64,  uint64_t)
DEFINE_FV_PARAM_VALIDATE_RANGE(  FLOAT,   float,     float)
DEFINE_FV_PARAM_VALIDATE_RANGE( DOUBLE,  double,    double)

/**
 * @internal Macro to generate comparison functions for basic types.
 * Comparison functions are used through function pointers in parameter
 * specifications.
 */
#define DEFINE_FV_PARAM_CMP(uctype, lctype, ctype)                        \
    int fv_param_cmp_##lctype(const FvParam *p,                           \
                              const FvValue *v1,                          \
                              const FvValue *v2) {                        \
        (void) p;                                                         \
        if (v1->value.v_##lctype < v2->value.v_##lctype) return -1;       \
        else return (v1->value.v_##lctype > v2->value.v_##lctype);        \
    }
 
DEFINE_FV_PARAM_CMP(   CHAR,    char,      char)
DEFINE_FV_PARAM_CMP(  UINT8,   uint8,   uint8_t)
DEFINE_FV_PARAM_CMP(  INT32,   int32,   int32_t)
DEFINE_FV_PARAM_CMP( UINT32,  uint32,  uint32_t)
DEFINE_FV_PARAM_CMP(  INT64,   int64,   int64_t)
DEFINE_FV_PARAM_CMP( UINT64,  uint64,  uint64_t)
DEFINE_FV_PARAM_CMP(   ENUM,    enum,   int32_t)
DEFINE_FV_PARAM_CMP(  FLAGS,   flags,  uint32_t)
DEFINE_FV_PARAM_CMP(  FLOAT,   float,     float)
DEFINE_FV_PARAM_CMP( DOUBLE,  double,    double)

int fv_param_cmp_string(const FvParam *p, const FvValue *v1,
                                          const FvValue *v2) {
    (void) p; /* Unused parameter */
    return strcmp(fv_value_get_string(v1), fv_value_get_string(v2));
}



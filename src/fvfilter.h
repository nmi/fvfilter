/**
 * @file      fvfilter.h
 * @brief     Filter API headers
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef FVFILTER_H
#define FVFILTER_H

#include <stdio.h>
#include <stdint.h>

#include "fvutil.h"
#include "fvvalue.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Video formats.
 */
enum FvFormat {
    FV_FORMAT_0,
    FV_FORMAT_I420,
    FV_FORMAT_YV12,
    FV_FORMAT_LAST
};


/**
 * Parameter specification structure for user-modifiable filter parameters.
 */
typedef struct FvParam {
    const unsigned int id;        /**< Parameter identifier             */
    const char    *name;          /**< Short name                       */
    const char    *longname;      /**< Display name                     */
    const char    *description;   /**< Full description                 */

    const FvValue  defaultval;    /**< Default parameter value and type */
    const void    *priv;          /**< Private storage                  */

    /**
     * Writes the default value of a FvParam specification to a given
     * FvValue.
     *
     * @param      p parameter specification
     * @param[out] v value to set
     */
    void (*set_default)(const struct FvParam *p, FvValue *v);

    /**
     * Ensures that an FvValue complies with a parameter specification.
     * If the value is invalid, it is overwritten with the closest
     * valid value.
     *
     * @param         p parameter specification
     * @param[in,out] v FvValue to validate
     *
     * @return whether modifying v was necessary to ensure validity
     */
    int (*validate)(const struct FvParam *p, FvValue *v);

    /**
     * Compares two FvValues according to a parameter specification.
     *
     * @param v1 first value
     * @param v2 second value
     *
     * @return -1, 0 or 1, if v1 is less than, equal or greater than v2,
     *         respectively
     */ 
    int (*cmp)(const struct FvParam *p, const FvValue *v1,
                                         const FvValue *v2);
} FvParam;

/**
 * Writes the default value of a FvParam specification to a given FvValue.
 * This is a generic implementation for all FvValue types.
 *
 * @param      p parameter specification
 * @param[out] v value to set
 */
void fv_param_set_default(const FvParam *p, FvValue *v); 

/**
 * @name FvParam validate() function implementations.
 */
/*@{*/

/**
 * FvParam validation function that fits a value within a predefined
 * range.
 *
 * @param         p parameter specification
 * @param[in,out] v FvValue to validate
 *
 * @return whether modifying v was necessary to ensure validity
 */
int fv_param_validate_range_char(  const FvParam *p, FvValue *v);
int fv_param_validate_range_uint8( const FvParam *p, FvValue *v);
int fv_param_validate_range_int32( const FvParam *p, FvValue *v);
int fv_param_validate_range_uint32(const FvParam *p, FvValue *v);
int fv_param_validate_range_int64( const FvParam *p, FvValue *v);
int fv_param_validate_range_uint64(const FvParam *p, FvValue *v);
int fv_param_validate_range_float( const FvParam *p, FvValue *v);
int fv_param_validate_range_double(const FvParam *p, FvValue *v);

/**
 * Ensures that an enumeration value fits a list of allowed values.
 * If there are problems, v gets set to the default value.
 *
 * @param         p parameter specification
 * @param[in,out] v value to validate
 *
 * @return whether modifying v was necessary to ensure validity
 */
int fv_param_validate_enum(const FvParam *p, FvValue *v);


/**
 * Ensures that a flag string or a flag field parses correctly against
 * a flag description. If there are problems, v gets set to the default
 * value.
 *
 * @param         p parameter specification
 * @param[in,out] v value to validate
 *
 * @return whether modifying v was necessary to ensure validity
 */
int fv_param_validate_flags(const FvParam *p, FvValue *v);


/**
 * Ensures that the type of a given value corresponds to a parameter
 * specification. If the type does not match, default parameter value
 * is set.
 *
 * @param         p parameter specification
 * @param[in,out] v value to validate
 *
 * @return whether modifying v was necessary to ensure validity
 */
int fv_param_validate(const FvParam *p, FvValue *v);

/*@}*/

/**
 * @name FvParam cmp() function implementations.
 */

/*@{*/
/**
 * FvParam cmp() function implementation that compares two values.
 *
 * @param  p parameter specification
 * @param v1 first value
 * @param v2 second value
 *
 * @return -1, 0 or 1, if v1 is less than, equal or greater than v2,
 *         respectively
 */
int fv_param_cmp_char(  const FvParam *p, const FvValue *v1,
                                          const FvValue *v2);
int fv_param_cmp_uint8( const FvParam *p, const FvValue *v1,
                                          const FvValue *v2);
int fv_param_cmp_int32( const FvParam *p, const FvValue *v1,
                                          const FvValue *v2);
int fv_param_cmp_uint32(const FvParam *p, const FvValue *v1,
                                          const FvValue *v2);
int fv_param_cmp_int64( const FvParam *p, const FvValue *v1,
                                          const FvValue *v2);
int fv_param_cmp_uint64(const FvParam *p, const FvValue *v1,
                                          const FvValue *v2);
int fv_param_cmp_enum(  const FvParam *p, const FvValue *v1,
                                          const FvValue *v2);
int fv_param_cmp_flags( const FvParam *p, const FvValue *v1,
                                          const FvValue *v2);
int fv_param_cmp_float( const FvParam *p, const FvValue *v1,
                                          const FvValue *v2);
int fv_param_cmp_double(const FvParam *p, const FvValue *v1,
                                          const FvValue *v2);
int fv_param_cmp_string(const FvParam *p, const FvValue *v1,
                                          const FvValue *v2);
/*@}*/

/**
 * @internal Macro to generate struct definitions for range-validated
 * basic types.
 *
 * @param uctype  upper-case type name as in FV_TYPE_x definitions
 * @param lctype  lower-case type names as in FvValue struct union names
 * @param ctype   C type name 
 * @param _defval default parameter value 
 * @param _min    minimum parameter value
 * @param _max    maximum parameter value
 */
#define FV_PARAM_RANGE(uctype, lctype, ctype, _defval, _min, _max)        \
        .defaultval = {                                                   \
            .type = FV_TYPE_##uctype,                                     \
            .value.v_##lctype = (_defval)                                 \
        },                                                                \
        .priv = (ctype[]) {(_min), (_max)},                               \
        .set_default = fv_param_set_default,                              \
        .validate = fv_param_validate_range_##lctype,                     \
        .cmp = fv_param_cmp_##lctype


/**
 * @name Macros to help with declaring FvParam structs.
 */
/*@{*/
/**
 * Macro to include within an FvParam struct declaration to create
 * a range-validating parameter. Fills fields .defaultval and .priv
 * and function pointers .set_default(), .validate() and .cmp().
 *
 * @param _defval default parameter value 
 * @param _min    minimum parameter value
 * @param _max    maximum parameter value
 */
#define FV_PARAM_RANGE_CHAR(  _defval, _min, _max) \
        FV_PARAM_RANGE(CHAR,     char,     char, (_defval), (_min), (_max)) 

#define FV_PARAM_RANGE_UINT8( _defval, _min, _max) \
        FV_PARAM_RANGE(UINT8,   uint8,  uint8_t, (_defval), (_min), (_max)) 

#define FV_PARAM_RANGE_INT32( _defval, _min, _max) \
        FV_PARAM_RANGE(INT32,   int32,  int32_t, (_defval), (_min), (_max)) 

#define FV_PARAM_RANGE_UINT32(_defval, _min, _max) \
        FV_PARAM_RANGE(UINT32, uint32, uint32_t, (_defval), (_min), (_max)) 

#define FV_PARAM_RANGE_INT64( _defval, _min, _max) \
        FV_PARAM_RANGE(INT64,   int64,  int64_t, (_defval), (_min), (_max)) 

#define FV_PARAM_RANGE_UINT64(_defval, _min, _max) \
        FV_PARAM_RANGE(UINT64, uint64, uint64_t, (_defval), (_min), (_max)) 

#define FV_PARAM_RANGE_FLOAT( _defval, _min, _max) \
        FV_PARAM_RANGE(FLOAT,   float,    float, (_defval), (_min), (_max)) 

#define FV_PARAM_RANGE_DOUBLE(_defval, _min, _max) \
        FV_PARAM_RANGE(DOUBLE, double,   double, (_defval), (_min), (_max)) 
/**@}*/

#define FV_PARAM_STRING(_defval)                                          \
         .defaultval = {                                                  \
            .type = FV_TYPE_STRING,                                       \
            .value.v_string = (_defval)                                   \
        },                                                                \
        .priv = NULL,                                                     \
        .set_default = fv_param_set_default,                              \
        .validate = fv_param_validate,                                    \
        .cmp = fv_param_cmp_string

       

/**
 * @internal Private data for an enumerated parameter
 */
struct fv_param_enum_data {
    /**
     * @internal Callback function that returns the definition of
     * an enumeration.
     *
     * @return pointer to a NULL-terminated FvEnum array
     */
    const FvEnum *(*get_enum)(void);
};

/**
 * Returns the FvEnum struct of an FvParam that contains
 * an FV_TYPE_ENUM value.
 *
 * @param param an FvParam instance
 * @return pointer to an FvEnum or NULL if FvParam does not contain
 *         an enumeration.
 */
static inline const FvEnum *fv_param_get_enum_info(const FvParam *param) {
    const struct fv_param_enum_data *data;
    if (param->defaultval.type != FV_TYPE_ENUM) return NULL;
    data = (const struct fv_param_enum_data *) param->priv;
    return data->get_enum();
}

/**
 * @internal Macro to generate struct definitions for an enumeration.
 *
 * @param get_flags callback function that returns pointer to an FvEnum
 *                  array
 * @param defval    default value as int32_t
 */
#define FV_PARAM_ENUM(get_enum, defval)                                   \
        .defaultval = {                                                   \
            .type = FV_TYPE_ENUM,                                         \
            .value.v_int32 = (defval)                                     \
        },                                                                \
        .priv = (struct fv_param_enum_data[]) {{(get_enum)}},             \
        .set_default = fv_param_set_default,                              \
        .validate = fv_param_validate_enum,                               \
        .cmp = fv_param_cmp_enum


/**
 * @internal Private data for a flags parameter
 */
struct fv_param_flags_data {
    /**
     * @internal Callback function that returns the definitions of a set
     * of flags.
     *
     * @return pointer to a NULL-terminated FvFlag array
     */
    const FvFlag *(*get_flags)(void);
};


/**
 * Returns the FvFlag struct of an FvParam that contains
 * an FV_TYPE_FLAGS value.
 *
 * @param param an FvParam instance
 * @return pointer to an FvEnum or NULL if FvParam does not contain flags.
 */
static inline const FvFlag *fv_param_get_flags_info(const FvParam *param) {
    const struct fv_param_flags_data *data;
    if (param->defaultval.type != FV_TYPE_FLAGS) return NULL;
    data = (const struct fv_param_flags_data *) param->priv;
    return data->get_flags();
}

  
/**
 * Macro to include within an FvParam struct declaration to create
 * a flag string parameter.
 *
 * @param get_flags callback function that returns pointer to an FvFlag
 *                  array
 * @param defval    default value as uint32_t
 */
#define FV_PARAM_FLAGS(get_flags, defval)                                 \
        .defaultval = {                                                   \
            .type = FV_TYPE_FLAGS,                                        \
            .value.v_uint32 = (defval)                                    \
        },                                                                \
        .priv = (struct fv_param_flags_data[]) {{(get_flags)}},           \
        .set_default = fv_param_set_default,                              \
        .validate = fv_param_validate_flags,                              \
        .cmp = fv_param_cmp_flags


/**
 * Macro to create an empty FvParam declaration.
 * This can be used to signal the end of an FvParam list.
 */
#define FV_PARAM_END {                                                    \
    .id = 0, .name = NULL, .longname = NULL, .description = NULL,         \
    .defaultval = {.type = FV_TYPE_NONE, .value.v_int64 = 0},             \
    .priv = NULL, .validate = NULL                                        \
}


struct FvFilterContext;
typedef struct FvFilterContext FvFilterContext;

/**
 * Filter definition.
 * Includes description of the filter,
 * parameters and callback functions.
 */
typedef struct FvFilter {
    const char *name;
    const char *longname;
    const char *classification;   /**< GStreamer element classification */
    const char *description;
    const char *author;

    const enum FvFormat *formats;      /**< Supported video formats */
    const FvParam       *parameters;   /**< Filter parameters       */

    /**
     * Initializes the filter.
     * @param  f filter context to initialize
     * @return 1 on success, 0 on failure
     */
    int (*init)(FvFilterContext *fc);

    /**
     * Uninitializes the filter and frees all private data.
     * @param f filter context to uninitialize
     */
    void (*uninit)(FvFilterContext *fc);

    /**
     * Sets input format and frame size.
     * @param  f      filter context
     * @param  format input video format. Can be FV_FORMAT_LAST, in which
     *                case the previously set format remains. 
     * @param  width  frame width
     * @param  height frame height
     * @return 1 on success, 0 on failure
     */ 
    int (*set_input_format)(FvFilterContext *fc, enum FvFormat format,
                            uint32_t width, uint32_t height);

    /**
     * Sets parameter value.
     * @param  f      filter context
     * @param  param_id parameter id
     * @param  value    value to set
     * @return 1 on success, 0 on failure 
     */
    int (*set_parameter)(FvFilterContext *fc, unsigned int param_id,
                         const FvValue *value);
    /**
     * Gets parameter value.
     * @param      f        filter context
     * @param      param_id parameter id
     * @param[out] value    retrieved parameter value is written here 
     * @return     1 on success, 0 on failure 
     */
    int (*get_parameter)(FvFilterContext *fc, unsigned int param_id,
                         FvValue *value);

    /**
     * Processes given frame and writes the output to another buffer.
     * @param  f    filter context
     * @param  in   input frame buffer
     * @param  out  output frame buffer
     * @param  size buffer size in bytes
     * @return 1 on success, 0 on failure
     */
    int (*process)(FvFilterContext *fc, const uint8_t *in, uint8_t *out,
                   size_t size);

} FvFilter;


/**
 * Runtime filter context.
 */
struct FvFilterContext {
    const FvFilter *filter;
    void           *priv;          /**< Pointer to private data          */
    size_t          priv_size;     /**< Size of private data             */

    uint32_t        in_width;      /**< Width of input frames in pixels  */ 
    uint32_t        in_height;     /**< Height of input frames in pixels */ 
    enum FvFormat   in_format;     /**< Input format                     */
};

/**
 * Registers a filter.
 *
 * @param f filter to register
 * @return 0 on success, ENOMEM if maximum number of filters have
 *         already been registered
 */
int fvfilter_register(FvFilter *f);

/**
 * Registers all filters included in the library.
 */
void fvfilter_register_all(void);

/**
 * Returns a filter from a global list of registered filters by exact name.
 *
 * @param name filter name
 * @return pointer to a filter struct
 */
const FvFilter *fvfilter_get_by_name(const char *name);

/**
 * Iterates over a global list of pointers to registered filters.
 *
 * @param filter pointer to current position in the list. If NULL,
 *               pointer to the head of the list is returned. 
 * @return pointer to the next position in the list of registered filters
 */
FvFilter **fv_filter_next(FvFilter **filter);


/* Built-in filters */
extern FvFilter fvsharpen;
extern FvFilter fvconvolve2d;

#ifdef __cplusplus
}
#endif

#endif /* FVFILTER_H */


/**
 * @file      fvgstreamer.c
 * @brief     GStreamer wrapper for fvfilter
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <stdio.h>
#include <stdint.h>

/* Turn off specific GCC warnings triggered by GStreamer and GLib headers */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpadded"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#include <gst/gst.h>
#include <gst/video/video.h>
#include <gst/video/gstvideofilter.h>
#pragma GCC diagnostic pop

#include "fvfilter.h"

gboolean fvtsmooth_init(GstPlugin *plugin);

#ifndef PACKAGE
#  define PACKAGE "fvfilter"
#endif

GST_DEBUG_CATEGORY_STATIC(gst_forevid_debug_category);
#define GST_CAT_DEFAULT gst_forevid_debug_category

/**
 * @internal Converts FvType to GType.
 *
 * @param t type to convert
 * @return a GType corresponding to the given FvType
 */
static inline GType fv_to_g_type(enum FvType t) {
    switch (t) {
        case FV_TYPE_NONE:    return G_TYPE_NONE;
        case FV_TYPE_CHAR:    return G_TYPE_CHAR;
        case FV_TYPE_UINT8:   return G_TYPE_UCHAR;
        case FV_TYPE_INT32:   return G_TYPE_INT;
        case FV_TYPE_UINT32:  return G_TYPE_UINT;
        case FV_TYPE_INT64:   return G_TYPE_INT64;
        case FV_TYPE_UINT64:  return G_TYPE_UINT64;
        case FV_TYPE_ENUM:    return G_TYPE_ENUM;
        case FV_TYPE_FLAGS:   return G_TYPE_FLAGS;
        case FV_TYPE_FLOAT:   return G_TYPE_FLOAT;
        case FV_TYPE_DOUBLE:  return G_TYPE_DOUBLE;
        case FV_TYPE_STRING:  return G_TYPE_STRING;
        case FV_TYPE_POINTER: return G_TYPE_POINTER;
        default: return G_TYPE_NONE;
    }
}

/**
 * @internal Converts GType to FvType.
 *
 * @param t type to convert
 * @return an FvType corresponding to the given GType
 */
static inline enum FvType g_to_fv_type(GType t) {
    switch (t) {
        case G_TYPE_NONE:    return FV_TYPE_NONE;
        case G_TYPE_CHAR:    return FV_TYPE_CHAR;
        case G_TYPE_UCHAR:   return FV_TYPE_UINT8;
        case G_TYPE_INT:     return FV_TYPE_INT32;
        case G_TYPE_UINT:    return FV_TYPE_UINT32;
        case G_TYPE_INT64:   return FV_TYPE_INT64;
        case G_TYPE_UINT64:  return FV_TYPE_UINT64;
        case G_TYPE_ENUM:    return FV_TYPE_ENUM;
        case G_TYPE_FLAGS:   return FV_TYPE_FLAGS;
        case G_TYPE_FLOAT:   return FV_TYPE_FLOAT;
        case G_TYPE_DOUBLE:  return FV_TYPE_DOUBLE;
        case G_TYPE_STRING:  return FV_TYPE_STRING;
        case G_TYPE_POINTER: return FV_TYPE_POINTER;
        default: return FV_TYPE_NONE;
    }
}

/**
 * @internal Converts FvValue to GValue.
 *
 * @param[in]  in  value to convert
 * @param[out] out pointer to a GValue for storing the converted value
 */
static inline void fv_to_g_value(const FvValue *in, GValue *out) {
    out->g_type = fv_to_g_type(in->type);
    out->data[0].v_int64 = in->value.v_int64;
}

/**
 * @internal Converts GValue to FvValue.
 *
 * @param[in]  in  value to convert
 * @param[out] out pointer to an FvValue for storing the converted value
 */
static inline void g_to_fv_value(const GValue *in, FvValue *out) {
    GType in_type = in->g_type;

    if (G_TYPE_IS_ENUM(in_type))
        out->type = FV_TYPE_ENUM;
    else if (G_TYPE_IS_FLAGS(in_type))
        out->type = FV_TYPE_FLAGS;
    else
        out->type = g_to_fv_type(in->g_type);

    out->value.v_int64 = in->data[0].v_int64;
}

/**
 * @internal Creates a parameter specification that wraps a given FvParam.
 *
 * Default parameter value along with minimum and maximum values are copied
 * from FvParam to the initialized struct so that other programs such as
 * gst-inspect can see them.
 *
 * @param param       FvParam instance that will be wrapped by the parameter
 *                    specification
 * @param filter_name name of the filter that the parameter belongs to
 *
 * @return a parameter specification instance or NULL if a GParamSpec
 *         class matching the given FvParam is not found
 */
static GParamSpec *fvparam_to_gparamspec(const FvParam *param,
                                         const char *filter_name) {
    char          *tmp_name, *type_name;
    const FvValue *defaultval = &param->defaultval;
    GParamSpec    *spec = NULL;
    GType          type = fv_to_g_type(defaultval->type);


/* Turn off c++-compat warnings because GParamFlags is an enumeration
 * that is incorrectly used as a flag container (bitfield) in the GParamSpec
 * struct. There's nothing we can do about it here. */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wc++-compat"

    GParamFlags flags = G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS;

#pragma GCC diagnostic pop

    switch (type) {
        case G_TYPE_CHAR:
            spec = (GParamSpec *) g_param_spec_char(
                    param->name, param->longname, param->description,
                    ((const char *) param->priv)[0],              /* min */
                    ((const char *) param->priv)[1],              /* max */
                    param->defaultval.value.v_char, flags);
            break;
        case G_TYPE_UCHAR:
            spec = (GParamSpec *) g_param_spec_uchar(
                    param->name, param->longname, param->description,
                    ((const uint8_t *) param->priv)[0],           /* min */
                    ((const uint8_t *) param->priv)[1],           /* max */
                    param->defaultval.value.v_uint8, flags);
            break;
        case G_TYPE_INT:
            spec = (GParamSpec *) g_param_spec_int(
                    param->name, param->longname, param->description,
                    ((const int32_t *) param->priv)[0],           /* min */
                    ((const int32_t *) param->priv)[1],           /* max */
                    param->defaultval.value.v_int32, flags);
            break;
        case G_TYPE_UINT:
            spec = (GParamSpec *) g_param_spec_uint(
                    param->name, param->longname, param->description,
                    ((const uint32_t *) param->priv)[0],          /* min */
                    ((const uint32_t *) param->priv)[1],          /* max */
                    param->defaultval.value.v_uint32, flags);
            break;
        case G_TYPE_INT64:
            spec = (GParamSpec *) g_param_spec_int64(
                    param->name, param->longname, param->description,
                    ((const int64_t *) param->priv)[0],           /* min */
                    ((const int64_t *) param->priv)[1],           /* max */
                    param->defaultval.value.v_int64, flags);
            break;
        case G_TYPE_UINT64:
            spec = (GParamSpec *) g_param_spec_uint64(
                    param->name, param->longname, param->description,
                    ((const uint64_t *) param->priv)[0],          /* min */
                    ((const uint64_t *) param->priv)[1],          /* max */
                    param->defaultval.value.v_uint64, flags);
            break;
        case G_TYPE_ENUM:
            tmp_name  = g_strconcat("gst_", filter_name, "_", param->name,
                                    NULL);
            type_name = fv_str_underscore_to_camelcase(tmp_name);
            g_free(tmp_name);
            spec = (GParamSpec *) g_param_spec_enum(
                    param->name, param->longname, param->description,
                    g_enum_register_static(type_name, (const GEnumValue *)
                            fv_param_get_enum_info(param)),
                    param->defaultval.value.v_enum, flags);
            free(type_name);
            break;
        case G_TYPE_FLAGS:
            tmp_name  = g_strconcat("gst_", filter_name, "_", param->name,
                                    NULL);
            type_name = fv_str_underscore_to_camelcase(tmp_name);
            g_free(tmp_name);
            spec = (GParamSpec *) g_param_spec_flags(
                    param->name, param->longname, param->description,
                    g_flags_register_static(type_name, (const GFlagsValue *)
                            fv_param_get_flags_info(param)),
                    param->defaultval.value.v_flags, flags);
            free(type_name);
            break;
        case G_TYPE_FLOAT:
            spec = (GParamSpec *) g_param_spec_float(
                    param->name, param->longname, param->description,
                    ((const float *) param->priv)[0],             /* min */
                    ((const float *) param->priv)[1],             /* max */
                    param->defaultval.value.v_float, flags);
            break;
        case G_TYPE_DOUBLE:
            spec = (GParamSpec *) g_param_spec_double(
                    param->name, param->longname, param->description,
                    ((const double *) param->priv)[0],            /* min */
                    ((const double *) param->priv)[1],            /* max */
                    param->defaultval.value.v_double, flags);
            break;
        case G_TYPE_STRING:
            spec = (GParamSpec *) g_param_spec_string(
                    param->name, param->longname, param->description,
                    param->defaultval.value.v_string, flags);
            break;
        default:
            break;
    }
    return spec;
}

/** @internal Mapping from FvFormat to GstVideoFormat. */
static GstVideoFormat fv_to_gst_format_mapping[] = {
    [FV_FORMAT_0]    = GST_VIDEO_FORMAT_UNKNOWN,
    [FV_FORMAT_I420] = GST_VIDEO_FORMAT_I420,
    [FV_FORMAT_YV12] = GST_VIDEO_FORMAT_YV12,
    [FV_FORMAT_LAST] = GST_VIDEO_FORMAT_UNKNOWN
};

/**
 * @internal Converts enum FvFormat identifiers to corresponding
 * GstVideoFormat identifiers.
 *
 * @param f an enum FvFormat identifier
 * @return a GstVideoFormat identifier corresponding to f
 */ 
static inline GstVideoFormat fv_to_gst_format(enum FvFormat f) {
    if ((f == FV_FORMAT_0) || (f >= FV_FORMAT_LAST))
        return GST_VIDEO_FORMAT_UNKNOWN;
    else
        return fv_to_gst_format_mapping[f];
}

/**
 * @internal Converts GstVideoFormat identifiers to corresponding
 * enum FvFormat identifiers. 
 *
 * @param gf a GstVideoFormat identifier
 * @return an enum FvFormat identifier corresponding to gf
 */
static inline enum FvFormat gst_to_fv_format(GstVideoFormat gf) {
    switch (gf) {
        case GST_VIDEO_FORMAT_I420: return FV_FORMAT_I420;
        case GST_VIDEO_FORMAT_YV12: return FV_FORMAT_YV12;
        default: return FV_FORMAT_0;
    }
}


/**
 * @internal Custom GstVideoFilterClass extension that wraps an FvFilter.
 */
typedef struct GstFvFilterClass {
    GstVideoFilterClass parent;
    const FvFilter *filter;
} GstFvFilterClass;

/**
 * @internal Custom GstVideoFilter extension that wraps an FvFilterContext.
 */
typedef struct GstFvFilter {
    GstVideoFilter parent;
    /*GstPad     *sinkpad, *srcpad;*/
    FvFilterContext priv;
} GstFvFilter;


/**
 * @internal Initializes a GstFvFilter instance.
 *
 * @param f a GstFvFilter instance to initialize
 */
static void gst_fvfilter_init(GstFvFilter *f) {
    GstFvFilterClass *klass = (GstFvFilterClass *) G_OBJECT_GET_CLASS(f);
    klass->filter->init(&f->priv);
}

/**
 * @internal Sets a GstFvFilter parameter value.
 * This function calls the underlying FvFilter::set_parameter() function.
 *
 * Overrides GObject::set_property().
 *
 * @param obj     GstFvFilter instance
 * @param prop_id parameter identifier
 * @param value   GValue of correct type
 * @param pspec   parameter specification
 */
static void gst_fvfilter_set_property(GObject *obj, guint prop_id,
                                      const GValue *value,
                                      GParamSpec *pspec) {
    GstFvFilter     *gstf = (GstFvFilter *) obj;
    FvFilterContext *fc   = &gstf->priv;
    const FvFilter  *f    = fc->filter;
    const FvParam   *fp   = &f->parameters[prop_id-1];
    FvValue          fvv;

    if (value->g_type != G_PARAM_SPEC_VALUE_TYPE(pspec)) {
        g_critical("%s: Type mismatch when setting property '%s'. "
                   "Expected %s, got %s", f->name, pspec->name,
                   g_type_name(G_PARAM_SPEC_VALUE_TYPE(pspec)),
                   g_type_name(value->g_type));
        return;
    }

    g_to_fv_value(value, &fvv);

    if (fp->validate(fp, &fvv)) {
        gchar *value_content = g_strdup_value_contents(value);
        g_warning("%s: Unable to set value of property '%s' to '%s'. "
                  "Value out of spec, using default instead.",
                  f->name, pspec->name, value_content);
        g_free(value_content);
    }

    if (!f->set_parameter(fc, fp->id, &fvv)) {
        gchar *value_content = g_strdup_value_contents(value);
        g_critical("%s: Unable to set value of property '%s' to '%s'. "
                   "Internal error.",
                   f->name, pspec->name, value_content);
        g_free(value_content);
    }
}

/**
 * @internal Gets a GstFvFilter parameter value.
 * This function calls the underlying FvFilter::get_parameter() function.
 *
 * Overrides GObject::get_property().
 *
 * @param obj        GstFvFilter instance
 * @param prop_id    parameter identifier
 * @param value[out] pointer to a GValue where the value will be written to
 * @param pspec      parameter specification
 */
static void gst_fvfilter_get_property(GObject *obj, guint prop_id,
                                      GValue *value,
                                      GParamSpec *pspec) {
    GstFvFilter     *gstf = (GstFvFilter *) obj;
    FvFilterContext *fc   = &gstf->priv;
    const FvFilter  *f    = fc->filter;
    const FvParam   *fp   = &f->parameters[prop_id-1];

    FvValue v;
    GType   prop_type = G_PARAM_SPEC_VALUE_TYPE(pspec);

    (void) pspec;  /* Unused parameter */

    f->get_parameter(fc, fp->id, &v);
    fv_to_g_value(&v, value);

    if (G_VALUE_TYPE(value) != prop_type) {
        /* Fix enum and flag types, log an error on other mismatches */
        switch (G_VALUE_TYPE(value)) {
            case G_TYPE_ENUM:
                if (G_TYPE_IS_ENUM(prop_type)) value->g_type = prop_type;
                break;
            case G_TYPE_FLAGS:
                if (G_TYPE_IS_FLAGS(prop_type)) value->g_type = prop_type;
                break;
            default:
                g_critical("%s: Invalid value type when reading "
                           "property '%s'. Expected %s, got %s.",
                           f->name, pspec->name,
                           g_type_name(G_PARAM_SPEC_VALUE_TYPE(pspec)),
                           g_type_name(G_VALUE_TYPE(value)));
                break;
        }
    }
}

/**
 * @internal Sets the input and output formats of a filter.
 *
 * Overrides GstBaseTransform::set_caps().
 *
 * @param btrans  a GstFvFilter instance
 * @param incaps  input caps
 * @param outcaps output caps
 * @return TRUE on success, FALSE on failure
 */
static gboolean gst_fvfilter_set_caps(GstBaseTransform *btrans,
                                    GstCaps *incaps, GstCaps *outcaps) {
    GstFvFilter     *f  = (GstFvFilter *) btrans;
    FvFilterContext *fc = &f->priv;
    GstVideoFormat gstformat;
    int width, height;
    gboolean ret = FALSE;

    (void) outcaps; /* Unused parameter */

    if (gst_video_format_parse_caps(incaps, &gstformat, &width, &height)) {
        if (width < 0) width = 0;
        if (height < 0) height = 0;
        ret = fc->filter->set_input_format(fc, gst_to_fv_format(gstformat),
                                           (uint32_t) width,
                                           (uint32_t) height);
    }
    return ret;
}

/**
 * @internal Stops and uninitializes a filter.
 *
 * Overrides GstBaseTransform::stop().
 *
 * @param btrans a GstFvFilter instance
 * @return always TRUE
 */
static gboolean gst_fvfilter_stop(GstBaseTransform *btrans) {
    GstFvFilter     *f  = (GstFvFilter *) btrans;
    FvFilterContext *fc = &f->priv;
    fc->filter->uninit(fc);
    return TRUE;
}

/**
 * @internal Filters a frame.
 *
 * Overrides GstBaseTransform::transform().
 *
 * @param btrans a GstFvFilter instance
 * @param in     input buffer
 * @param out    output buffer
 * @return status as a GstFlowReturn value
 */
static GstFlowReturn gst_fvfilter_transform(GstBaseTransform *btrans,
                                          GstBuffer *in, GstBuffer *out) {
    GstFvFilter     *f  = (GstFvFilter *) btrans;
    FvFilterContext *fc = &f->priv;
    guint8    *idata  = GST_BUFFER_DATA(in);
    guint8    *odata  = GST_BUFFER_DATA(out);
    guint      size   = GST_BUFFER_SIZE(in);

    if (fc->filter->process(fc, idata, odata, size)) {
        return GST_FLOW_OK;
    } else {
        return GST_FLOW_NOT_LINKED;
    }
}

/**
 * @internal Initializes a GstFvFilterClass subclass that wraps a single
 * FvFilter.
 *
 * @param klass GstFvFilterClass to initialize
 * @param class_data pointer to a FvFilter struct to wrap around
 */
static void gst_fvfilter_class_init(GstFvFilterClass *klass,
                                    gconstpointer *class_data) {
    GObjectClass          *gobject_class = (GObjectClass *) klass;
    GstElementClass       *element_class = GST_ELEMENT_CLASS(klass);
    GstBaseTransformClass *btrans_class  = (GstBaseTransformClass *) klass;
    const FvFilter        *fvf           = (const FvFilter *) class_data;
    const enum FvFormat   *format;
    const FvParam         *param;
    GstCaps               *caps;
    guint i;
    /* GType paramspecfv_type = g_paramspecfv_get_type(); */

    klass->filter = fvf;
    if (fvf == NULL) return;

    gst_element_class_set_details_simple(element_class,
            fvf->longname,
            fvf->classification,
            fvf->description,
            fvf->author);

    /* Scan the list of formats */
    format = fvf->formats;
    caps = gst_caps_new_empty();
    while (*format != 0) {
        GstVideoFormat gstvf = fv_to_gst_format(*format);
        gst_caps_merge(caps, gst_video_format_new_template_caps(gstvf));
        format++;
    }
    gst_caps_ref(caps);
    gst_element_class_add_pad_template(element_class,
            gst_pad_template_new("src", GST_PAD_SRC, GST_PAD_ALWAYS, caps));

    gst_element_class_add_pad_template(element_class,
            gst_pad_template_new("sink", GST_PAD_SINK, GST_PAD_ALWAYS, caps));

    /* Install properties */
    gobject_class->set_property = gst_fvfilter_set_property;
    gobject_class->get_property = gst_fvfilter_get_property;

    for (i = 1, param = fvf->parameters; param->name; i++, param++) {
        GParamSpec *pspec = fvparam_to_gparamspec(param, fvf->name);
        g_object_class_install_property(gobject_class, i, pspec);
    }

    btrans_class->set_caps  = GST_DEBUG_FUNCPTR(gst_fvfilter_set_caps);
    btrans_class->transform = GST_DEBUG_FUNCPTR(gst_fvfilter_transform);
    btrans_class->stop      = GST_DEBUG_FUNCPTR(gst_fvfilter_stop);
}

/**
 * @internal Initializes the forevid GStreamer plugin along with all
 * built-in filter elements.
 *
 * @param plugin a GstPlugin instance to initialize
 * @return TRUE on success, FALSE if one of the elements fails to register
 */
static gboolean gst_plugin_init(GstPlugin *plugin) {
    gboolean   ret  = TRUE;
    FvFilter **fptr = NULL;

    GST_DEBUG_CATEGORY_INIT(gst_forevid_debug_category, "forevid", 0,
                            "Forevid filters shared debug");

    fvfilter_register_all();
    while (*(fptr = fv_filter_next(fptr))) {
        FvFilter *filter = *fptr;
        GType type;
        GTypeInfo info;

        /* Fill GTypeInfo to dynamically create a class for an element */
        info.class_size = sizeof(GstFvFilterClass);
        info.base_init = NULL;
        info.base_finalize = NULL;
        info.class_init = (GClassInitFunc) gst_fvfilter_class_init;
        info.class_finalize = NULL;
        info.class_data = filter;
        info.instance_size = sizeof(GstFvFilter),
        info.n_preallocs = 0;
        info.instance_init = (GInstanceInitFunc) gst_fvfilter_init;
        info.value_table = NULL;

        /* Register a type and set up the class */
        type = g_type_register_static(GST_TYPE_VIDEO_FILTER,
                                      filter->name, &info, (GTypeFlags) 0);

        ret &= gst_element_register(plugin, filter->name,
                                    GST_RANK_NONE, type);
    }
    ret &= fvtsmooth_init(plugin);

    return ret;
}

/** @internal Plugin descriptor */
GstPluginDesc gst_plugin_desc = {
    GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    "forevid",
    "Forensic video filters",
    gst_plugin_init,
    VERSION,
    "LGPL",
    PACKAGE,
    "forevid",
    "http://www.forevid.org",
     __GST_PACKAGE_RELEASE_DATETIME,
    GST_PADDING_INIT
};


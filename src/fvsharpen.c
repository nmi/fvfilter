/**
 * @file      fvsharpen.c
 * @brief     Filter for unsharp masking and gaussian blur.
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "fvfilter.h" 
#include "fvutil.h" 
#include "convert.h" 
#include "convolve.h" 


#ifdef HAVE_OPENCL

#include <CL/opencl.h>
#include "fvopencl.h" 

#define CL_KERNEL(src) #src
static const char *opencl_kernel_float =
#include "fvsharpen.cl"

typedef struct CLContext {
    cl_platform_id   platform_id;
    cl_device_id     device_id;
    cl_context       context;
    cl_command_queue commandq;
    cl_program       program;
    cl_kernel        kernel;
    cl_mem           input;
    cl_mem           output;
    int              use_gpu;
    int              status;
} CLContext;

#endif


enum ParamId { 
    PARAM_0,
    PARAM_SIGMA,
    PARAM_KERNEL_SIZE,
    PARAM_AMOUNT,
    PARAM_COMPONENTS,
    PARAM_CPU_FEATURES,
    PARAM_OPTIMIZATION,
    PARAM_LAST
}; 

#define GAUSSIAN_MULT 3.0
#define GAUSSIAN_SAMPLES 10

#define PI 3.14159265358979323846

enum Optimizations {
    OPT_AUTO,
    OPT_FLOAT,
    OPT_FLOAT_SIMD,
    OPT_FLOAT_OPENCL,
    OPT_U16_SIMD,
    OPT_U16,
    OPT_LAST
};

static const FvEnum optimizations[] = {
    {OPT_AUTO,         "automatic",             "auto"      },
    {OPT_FLOAT,        "floating point",        "float"     },
    {OPT_FLOAT_SIMD,   "floating point SIMD",   "float_simd"},
    {OPT_FLOAT_OPENCL, "floating point OpenCL", "float_opencl"},
    {OPT_U16_SIMD,     "16-bit integer SIMD",   "int16_simd"},
    {0,                NULL,                    NULL        }
};

#ifdef DEBUG
static const FvEnum *get_optimizations(void) {
    return optimizations;
}
#endif


/**
 * @internal Filter runtime context
 */
typedef struct PrivateContext {
    FvVectorData frame;
    FvVectorData luma_kernel;
    FvVectorData chroma_kernel;
    FvVectorData amount_vector;

    FvVectorParams vector;

    int (*process_plane)(struct PrivateContext *pc, FvVectorData *kernel,
                         const uint8_t *in, uint8_t *out, uint8_t *buf,
                         uint32_t width, uint32_t height);

    /* Parameter storage */
    double     sigma;
    double     amount;
    uint32_t   kernel_size;
    uint32_t   components;
    uint32_t   cpu_features;
    enum Optimizations user_opt;
    enum Optimizations opt;

    /* Frame padding */
    uint32_t   padding_left;
    uint32_t   padding_right;
    uint32_t   padding_top;
    uint32_t   padding_bottom;
    uint32_t   stride;

#ifdef HAVE_OPENCL
    CLContext  opencl_context;
#endif
} PrivateContext; 


#define pad_to_mod32(n) ((1 + (((n) - 1) >> 5)) << 5)


static int init_buffer(PrivateContext *pc, uint32_t width, uint32_t height,
                       enum FvFormat format) {
    FvVectorData *f = &pc->frame;
    size_t s;
    (void) format; /* Unused parameter */

    /* Calculate aligned padding for the kernel */
    pc->padding_left = pad_to_mod32(
            ((uint32_t) pc->luma_kernel.format.num_elements - 1) *
             (uint32_t) pc->vector.element_bytes);
    pc->padding_right = pad_to_mod32(
            ((uint32_t) pc->luma_kernel.format.num_elements - 1 + width) *
            (uint32_t) pc->vector.element_bytes) -
            (width * (uint32_t) pc->vector.element_bytes);
    pc->padding_top = (uint32_t) pc->luma_kernel.format.num_elements - 1;
    pc->padding_bottom = pc->padding_top;
    pc->stride = ((uint32_t) pc->vector.element_bytes * width +
                  pc->padding_left + pc->padding_right);
    fv_log_info("Frame padding: left=%d, right=%d, top=%d, bottom=%d (%d x %d)",
                pc->padding_left, pc->padding_right, pc->padding_top,
                pc->padding_bottom, width, height);
    fv_log_info("Stride: %d", pc->stride);

    f->format.element_bytes = pc->vector.element_bytes;
    f->format.element_type = pc->vector.element_type;
    f->format.num_elements = width * height;
    s = pc->stride * (height + pc->padding_top + pc->padding_bottom);

    if (f->format.total_bytes < s) {
        fv_free(f->data.u8);
        f->data.u8 = (uint8_t *) fv_malloc(6 * s);
        f->format.total_bytes = s;
    }

    if (f->data.u8 == NULL) {
        f->format.total_bytes = 0;
        return 0;
    }
    return 1;
}

static void generate_kernel_float(FvVectorData *kernel, size_t vector_size,
                                  double sigma) {
    float  matrix_sum;
    double sample_width = 1.0 / (double) GAUSSIAN_SAMPLES;
    double a = 1.0 / (sigma * sqrt(2.0 * PI));
    size_t i, j; 

    if (!kernel->format.total_bytes) return;

    for (i = 0; i < kernel->format.num_elements; i++) {
        double x = -0.5 + (double) i;
        double sum = 0.0;
        for (j = 0; j < GAUSSIAN_SAMPLES; j++) {
            double gaussian = a * exp(-x * x / (2.0 * sigma * sigma));
            sum += gaussian;
            x += sample_width;
        }
        kernel->data.f[vector_size * i] =
                (float) (sum / (double) GAUSSIAN_SAMPLES);
    }

    /* Calculate sum of kernel values to adjust to sampling errors */
    matrix_sum = kernel->data.f[0];
    for (i = 1; i < kernel->format.num_elements; i++) {
        matrix_sum += 2 * kernel->data.f[vector_size * i];
    }

    /* Compensate the error and broadcast over each vector */
    for (i = 0; i < kernel->format.num_elements; i++) {
        size_t p = vector_size * i;
        kernel->data.f[p] /= matrix_sum;
        for (j = 1; j < vector_size; j++) {
            kernel->data.f[p + j] = kernel->data.f[p];
        }
    }

#ifdef DEBUG
    fv_log_info("Gaussian kernel:");
    fputs("        [ ", fv_get_log_target());
    for (i = 0; i < kernel->format.num_elements; i++) {
        fprintf(fv_get_log_target(), "%f ",
                kernel->data.f[vector_size * i]);
    }
    fputs("]\n", fv_get_log_target());
#endif
}

static void convert_kernel_float_to_u16(FvVectorData *kernel,
                                        size_t float_vector_size) {
    size_t i;
    int kernel_sum = 0, num_nonzero = 0, err_sign, err_div, err_remainder;
    if (!kernel->format.total_bytes) return;

    for (i = 0; i < kernel->format.num_elements; i++) {
        uint16_t value = (uint16_t) lrint(65535.0 *
                kernel->data.f[float_vector_size * i]);
        kernel->data.u16[float_vector_size * 2 * i] = value;
        kernel_sum += value;
        if (i > 0) kernel_sum += value;
        if (value > 0) num_nonzero++;
    }

    kernel_sum -= 65535;
    if (kernel_sum >= 0) {
        err_sign = 1;
    } else {
        err_sign = -1;
        kernel_sum = -kernel_sum;
    }
    err_div = kernel_sum / (num_nonzero * 2 - 1);
    err_div *= err_sign;
    err_remainder = kernel_sum - err_div * (num_nonzero * 2 - 1); 

    fv_log_info("Gaussian kernel rounding error: %d", err_sign * kernel_sum);
    fv_log_info("Adjusting each non-zero element by %d and "
                "the %d center elements by %d",
                err_div, err_remainder, err_remainder ? err_sign : 0);
    err_remainder = (err_remainder + 1) / 2;

    /* Error correction and vector broadcast*/
    for (i = 0; i < kernel->format.num_elements; i++) {
        size_t p = float_vector_size * 2 * i;
        size_t j;
        if (i < (size_t) num_nonzero) {
            int value = (int) kernel->data.u16[p] - err_div;
            if (i < (size_t) err_remainder) {
                value -= err_sign; 
            }
            if (value < 0) value = 0;
            kernel->data.u16[p] = (uint16_t) value;
        }
        for (j = 1; j < float_vector_size * 2; j++) {
            kernel->data.u16[p + j] = kernel->data.u16[p];
        }
    }

#ifdef DEBUG
    fv_log_info("Gaussian kernel:");
    fputs("        [ ", fv_get_log_target());
    for (i = 0; i < kernel->format.num_elements * float_vector_size * 2; i++) {
        fprintf(fv_get_log_target(), "%d ",
                kernel->data.u16[i]);
                //kernel->data.u16[float_vector_size * 2 * i]);
    }
    fputs("]\n", fv_get_log_target());
#endif
}


static int init_kernel(PrivateContext *pc) {
    uint32_t luma_kernel_size, chroma_kernel_size;
    
    if (pc->kernel_size == 0) {
        luma_kernel_size = (uint32_t) (pc->sigma * GAUSSIAN_MULT) + 1;
    } else {
        /* Only half of the kernel is stored internally, so we'll
         * cut the size in half here. */
        luma_kernel_size = (pc->kernel_size + 1) / 2;
    }
    /* Store another kernel for subsampled chroma */
    chroma_kernel_size = (luma_kernel_size + 1) / 2;
    
    fv_free(pc->luma_kernel.data.u8);
    pc->luma_kernel.data.u8 = (uint8_t *)
            fv_malloc((luma_kernel_size + chroma_kernel_size) *
                      pc->vector.total_bytes);
    if (pc->luma_kernel.data.u8 == NULL) {
        pc->chroma_kernel.data.u8 = NULL;
        pc->luma_kernel.format.total_bytes = 0;
        pc->chroma_kernel.format.total_bytes = 0;
        return 0;
    }

    pc->luma_kernel.format.element_bytes   = pc->vector.element_bytes;
    pc->luma_kernel.format.element_type    = pc->vector.element_type;
    pc->luma_kernel.format.num_elements    = luma_kernel_size;
    pc->luma_kernel.format.total_bytes     = luma_kernel_size *
                                             pc->vector.total_bytes;

    pc->chroma_kernel.format.num_elements  = chroma_kernel_size;
    pc->chroma_kernel.format.element_bytes = pc->vector.element_bytes;
    pc->chroma_kernel.format.element_type  = pc->vector.element_type;
    pc->chroma_kernel.format.total_bytes   = chroma_kernel_size *
                                             pc->vector.total_bytes;
    pc->chroma_kernel.data.u8 =
            &pc->luma_kernel.data.u8[pc->luma_kernel.format.total_bytes];
    
    if (pc->vector.element_type == FV_TYPE_FLOAT) {
        size_t vs = pc->vector.num_elements;
        generate_kernel_float(&pc->luma_kernel,   vs, pc->sigma);
        generate_kernel_float(&pc->chroma_kernel, vs, pc->sigma / 2.0);
    } else {
        size_t vs = pc->vector.num_elements / 2;
        generate_kernel_float(&pc->luma_kernel,   vs, pc->sigma);
        generate_kernel_float(&pc->chroma_kernel, vs, pc->sigma / 2.0);

        convert_kernel_float_to_u16(&pc->luma_kernel,   vs);
        convert_kernel_float_to_u16(&pc->chroma_kernel, vs);
    }
    return 1;
}

#define SATURATE_8_BITS_FULL(val) \
    (((val) > 255) ? 255 : ((val) < 0) ? 0 : (val))

static int process_plane_float(PrivateContext *pc, FvVectorData *kernel,
                         const uint8_t *in, uint8_t *out, uint8_t *buf,
                         uint32_t width, uint32_t height) {
    float *buf1 = (float *) buf;
    float *buf2 = &buf1[width * height];
    float *k = kernel->data.f;
    size_t ksize = kernel->format.num_elements;
    size_t i, j, p;
    float  amount = (float) pc->amount;

    /* Blur horizontally */
    for (i = 0; i < height; i++) {
        size_t row = i * width;
        for (j = 0; j < width; j++)
            buf1[row + j] = k[0] * (float) in[row + j];
    }
    for (p = 1; p < ksize; p++) {
        float gaussian = k[p]; 
        for (i = 0; i < height; i++) {
           size_t row = i * width;
           size_t rowend = row + width - 1;
           for (j = 0; j < width - p; j++)
               buf1[row + j + p] += gaussian * (float) in[row + j];
           for (j = p; j < width; j++)
               buf1[row + j - p] += gaussian * (float) in[row + j];

           /* Balance blur at left and right edge by extrapolating with
            * the first and last column of input */
           for (j = 0; j < p; j++) {
               buf1[row + j] += gaussian * (float) in[row];
               buf1[rowend - j] += gaussian * (float) in[rowend];
            }
        }
    }
    /* Blur vertically */
    for (i = 0; i < height; i++) {
        size_t row = i * width;
        for (j = 0; j < width; j++)
            buf2[row + j] = k[0] * buf1[row + j];
    }
    for (p = 1; p < ksize; p++) {
        float gaussian = k[p]; 
        for (i = 0; i < height - p; i++) {
           size_t row1 = i * width;
           size_t row2 = (i + p) * width;
           for (j = 0; j < width; j++)
               buf2[row2 + j] += gaussian * buf1[row1 + j];
           for (j = 0; j < width; j++)
               buf2[row1 + j] += gaussian * buf1[row2 + j];
        }

        /* Balance blur at top and bottom edge by extrapolating
         * the first and last row of input */
        for (i=0; i < p; i++) {
            size_t row = i * width;
            size_t lastrow = (height - 1) * width; 
            for (j = 0; j < width; j++) {
                buf2[row + j] += gaussian * (float) buf1[j];
            }
            row = lastrow - row;
            for (j = 0; j < width; j++) {
                buf2[row + j] += gaussian * (float) buf1[lastrow + j];
            }
        }
    }

    for (i = 0; i < width * height; i++) {
       int value = (int) in[i] + (int) lrintf(amount *
                                              ((float) in[i] - buf2[i]));
       out[i] = (uint8_t) SATURATE_8_BITS_FULL(value);
    } 
    return 1;

}

#ifdef HAVE_ASM
static int process_plane_float_simd_5x5(PrivateContext *pc,
                                        FvVectorData *kernel,
                                        const uint8_t *in, uint8_t *out,
                                        uint8_t *buf,
                                        uint32_t width, uint32_t height) {
    float *buf1 = pc->frame.data.f;
    float *k = kernel->data.f;
    size_t i, size = width * height;
    float  amount = (float) pc->amount;

    fv_u8_to_float_sse2(buf1, in, size);
    /*fv_emms();*/

    for (i = 0; i < height; ++i) {
        fv_convolve_1x5_sym_float_sse(&buf1[i*width], width, k);
    }

    for (i = 0; i < width/4; ++i) {
        fv_convolve_5x1_sym_float_4_sse(&buf1[i*4], width * sizeof(float),
                                        height, k);
    }
    for (i = 0; i < size; i++) {
       int value = (int) in[i] + (int) lrintf(amount *
                                              ((float) in[i] - buf1[i]));
       out[i] = (uint8_t) SATURATE_8_BITS_FULL(value);
    }

    return 1;
}

static int process_plane_float_simd(PrivateContext *pc,
                                    FvVectorData *kernel,
                                    const uint8_t *in, uint8_t *out,
                                    uint8_t *buf,
                                    uint32_t width, uint32_t height) {
    float   *in_float = (float *) buf;
    uint8_t *buf1     = buf + pc->frame.format.total_bytes;
    uint8_t *buf2     = buf1 + pc->frame.format.total_bytes;

    float *dst, *src;
    float *k = kernel->data.f;
    size_t ksize = kernel->format.num_elements;
    size_t i, j;
    size_t float_padding_left = pc->padding_left / 4;
    size_t float_padding_right = pc->padding_right / 4;
    size_t float_stride = pc->stride / 4;
    size_t frame_stride = width * pc->frame.format.element_bytes;
    float  amount = (float) pc->amount;

    /* Convert to float */
    dst = in_float;
    for (i = 0; i < height; i++) {
//        for (j = 0; j < 50; j++) {
            fv_u8_to_float_sse2(dst, in + i * width, width);
//        }
        dst += float_stride;
    }
    fv_emms();
//    return 1;
    memcpy(buf1, in_float, pc->stride * height);
    /* Pad top and bottom sides by duplicating border pixels */

    /* Top */
    dst = (float *) (buf1 - pc->stride);
    for (i = 1; i < ksize; i++) {
        memcpy(dst, buf1, frame_stride);
        dst -= float_stride;
    }
    /* Bottom */
    src = (float *) (buf1 + (size_t) ((height - 1) * pc->stride));
    dst = src + float_stride;
    for (i = 1; i < ksize; i++) {
        memcpy(dst, src, frame_stride);
        dst += float_stride;
    }

    /* Apply gaussian blur vertically */
    dst = (float *) buf2;
    src = (float *) buf1;
    for (i=0; i < height; i++) {
        fv_multiply_float_row_sse(dst, src, width, &k[0]);
        for (j = 1; j < ksize; j++) {
            fv_multiply_add_float_row_sse(dst, src - j * float_stride,
                                          width, &k[4*j]);
            fv_multiply_add_float_row_sse(dst, src + j * float_stride,
                                          width, &k[4*j]);
        }
        dst += float_stride;
        src += float_stride;
    }

    /* Pad left and right sides by duplicating border pixels */

    /* Left */
    src = (float *) buf2;
    for (i = 0; i < height; i++) {
        dst = src - float_padding_left;
        for (j = 0; j < float_padding_left; j++) {
            dst[j] = *src;
        }
        src += float_stride;
    }
    /* Right */
    src = &((float *) buf2)[width - 1];
    for (i = 0; i < height; i++) {
        dst = &src[1];
        for (j = 0; j < float_padding_right; j++) {
            dst[j] = *src;
        }
        src += float_stride;
    }

    /* Apply gaussian blur horizontally */
    dst = (float *) buf1;
    src = (float *) buf2;
    for (i=0; i < height; i++) {
        fv_multiply_float_row_sse(dst, src, width, &k[0]);
        for (j = 1; j < ksize; j++) {
            fv_multiply_add_unaligned_float_row_sse2(dst, src - j,
                                                     width, &k[4*j]);
            fv_multiply_add_unaligned_float_row_sse2(dst, src + j,
                                                     width, &k[4*j]);
        }
        dst += float_stride;
        src += float_stride;
    }


    src = (float *) buf1;
    for (i = 0; i < height; i++) {
        size_t row = i * width;
        for (j = 0; j < width; j++) {
            size_t pos = row + j;
            int value = (int) lrintf(in_float[j] + amount * (in_float[j] - src[j]));
            out[pos] = (uint8_t) SATURATE_8_BITS_FULL(value);
        }
        in_float += float_stride;
        src += float_stride;
    }

    return 1;
}

#endif


#ifdef HAVE_OPENCL

static int init_opencl(PrivateContext *pc);
static int init_opencl_kernel(PrivateContext *pc);

static int init_opencl(PrivateContext *pc) {
    char    str[1024];
    cl_int  ret;
    cl_uint n;
    CLContext *context = &pc->opencl_context;

    context->status = 0;
    str[0] = '\0';

    /* Pick the first platform */
    ret = clGetPlatformIDs(1, &context->platform_id, &n);
    if (ret != CL_SUCCESS) {
        fv_log_error("Failed to get OpenCL platform IDs: %s",
                     clErrorString(ret));
        return 1;
    }
    fv_log_info("Number of OpenCL platforms: %d", n);

    ret = clGetPlatformInfo(context->platform_id, CL_PLATFORM_NAME,
                            1024, &str, NULL);
    if (ret != CL_SUCCESS) {
        fv_log_error("Failed to read OpenCL platform name: %s",
                     clErrorString(ret));
        return 1;
    }
    fv_log_info("Selected OpenCL platform: %s", str);

    /* Pick the first device group */
    ret = clGetDeviceIDs(context->platform_id,
            context->use_gpu ? CL_DEVICE_TYPE_GPU : CL_DEVICE_TYPE_CPU, 1,
            &context->device_id, NULL);
    if (ret != CL_SUCCESS) {
        fv_log_error("Failed to create OpenCL device group: %s",
                     clErrorString(ret));
        return 1;
    }

    /* Create a context */
    context->context = clCreateContext(0, 1, &context->device_id,
                                       NULL, NULL, &ret);
    if (ret != CL_SUCCESS) {
        fv_log_error("Failed to create OpenCL context: %s",
                     clErrorString(ret));
        return 1;
    }

    /* Create a queue */
    context->commandq = clCreateCommandQueue(context->context,
                                             context->device_id, 0, &ret);
    if (ret != CL_SUCCESS) {
        fv_log_error("Failed to create OpenCL command queue: %s",
                     clErrorString(ret));
        return 1;
    }
    context->status = 1;

    return init_opencl_kernel(pc);
}

static int init_opencl_kernel(PrivateContext *pc) {
    cl_int ret;
    CLContext *context = &pc->opencl_context;
    context->program = clCreateProgramWithSource(context->context, 1,
            &opencl_kernel_float, NULL, &ret);
    if (ret != CL_SUCCESS) {
        fv_log_error("Failed to create OpenCL program object: %s",
                     clErrorString(ret));
        return 1;
    }

    ret = clBuildProgram(context->program, 0, NULL, "", NULL, NULL);
    if (ret != CL_SUCCESS) {
        fv_log_error("Failed to compile OpenCL kernel: %s",
                     clErrorString(ret));
        return 1;
    }

    context->kernel = clCreateKernel(context->program, "convolve", NULL);
    return 0;
}

static int init_opencl_buffers(PrivateContext *pc, size_t w, size_t h) {
    cl_bool image_support;
    cl_int  ret;
    cl_image_format fmt = {CL_INTENSITY, CL_UNORM_INT8};
    CLContext *context = &pc->opencl_context;
    /*context->buffer = clCreateBuffer(context->context, CL_MEM_READ_WRITE,
            MEM_SIZE * sizeof(char), NULL, &ret);
    */

    clGetDeviceInfo(context->device_id, CL_DEVICE_IMAGE_SUPPORT,
                    sizeof(cl_bool), &image_support, NULL);
    if (!image_support) {
        fv_log_error("OpenCL device does not provide image support");
    }
    context->input = clCreateImage2D(context->context, CL_MEM_READ_ONLY,
            &fmt, w, h, 0, NULL, &ret);
    if (ret != CL_SUCCESS) {
        cl_int flag = 0;
        fv_log_error("Failed to allocate OpenCL input image buffer of size %d x %d: %s",
                     w, h, clErrorString(ret));
        return 1;
    }

    context->output = clCreateImage2D(context->context, CL_MEM_WRITE_ONLY,
            &fmt, w, h, 0, NULL, &ret);

    clSetKernelArg(context->kernel, 0, sizeof(cl_mem), &context->input);
    clSetKernelArg(context->kernel, 1, sizeof(cl_mem), &context->output);

    if (ret != CL_SUCCESS) {
        fv_log_error("Failed to allocate OpenCL output image buffer of size %d x %d: %s",
                     w, h, clErrorString(ret));
        return 1;
    }

    fv_log_info("Yay! OpenCL buffers (%d, %d) ready!", w, h);
    context->status = 2;
    return 0;
}

static int process_plane_float_opencl(
        PrivateContext *pc, FvVectorData *kernel,
        const uint8_t *in, uint8_t *out, uint8_t *buf,
        uint32_t width, uint32_t height) {
    CLContext *context = &pc->opencl_context;
    //fv_log_info("OpenCL stub");
    if (!context->status) init_opencl(pc);
    if (context->status < 2) init_opencl_buffers(pc, width, height);
    return 1;
}
#endif

static int process_plane_u16(PrivateContext *pc, FvVectorData *kernel,
                             const uint8_t *in, uint8_t *out,
                             uint8_t *buf,
                             uint32_t width, uint32_t height) {
    return 1;
}

#if HAVE_OPENMP
#    define STRINGIFY(x) #x
#    define OMP_DYNAMIC_FOR(n, var) \
            _Pragma(STRINGIFY(omp parallel for schedule(dynamic,10) private(var)))
#endif

static int process_plane_u16_simd(PrivateContext *pc, FvVectorData *kernel,
                                  const uint8_t *in, uint8_t *out,
                                  uint8_t *buf,
                                  uint32_t width, uint32_t height) {
    uint16_t *buf1 = (uint16_t *) (buf + pc->padding_left + 
                               pc->stride * pc->padding_top);
    uint16_t *buf2 = (uint16_t *) &buf1[pc->frame.format.total_bytes / 2];
    uint16_t *src;

    uint16_t *k = kernel->data.u16;
    size_t ksize = kernel->format.num_elements;
    size_t i;
    size_t u16_padding_left = pc->padding_left / 2;
    size_t u16_padding_right = pc->padding_right / 2;
    size_t u16_stride = pc->stride / 2;
    size_t frame_stride = width * pc->frame.format.element_bytes;

    /* Convert to u16 */
    OMP_DYNAMIC_FOR(threads, i)
    for (i = 0; i < height; i++) {
        fv_u8_to_u16_sse2(&buf1[i * u16_stride], &in[i * width], width);
    }

    /* Pad top and bottom sides by duplicating border pixels */

    /* Top */
    for (i = 1; i < ksize; i++) {
        memcpy(buf1-i*u16_stride, buf1, frame_stride);
    }

    /* Bottom */
    src = buf1 + (((size_t) height - 1) * u16_stride);
    for (i = 1; i < ksize; i++) {
        memcpy(&src[i * u16_stride], src, frame_stride);
    }

    /* Apply gaussian blur vertically */
    OMP_DYNAMIC_FOR(threads, i)
    for (i=0; i < height; i++) {
        size_t j;
        size_t pos = i * u16_stride;
        fv_multiply_u16_row_sse2(&buf2[pos], &buf1[pos], width, &k[0]);
        for (j = 1; j < ksize; j++) {
            fv_multiply_add_u16_row_sse2(&buf2[pos], &buf1[pos - j * u16_stride],
                                         width, &k[8*j]);
            fv_multiply_add_u16_row_sse2(&buf2[pos], &buf1[pos + j * u16_stride],
                                         width, &k[8*j]);
        }
    }

    /* Pad left and right sides by duplicating border pixels */

    /* Left */
    for (i = 0; i < height; i++) {
        size_t pos = i * u16_stride;
        size_t j;
        uint16_t *dst = &buf2[pos - u16_padding_left];
        for (j = 0; j < u16_padding_left; j++) {
            dst[j] = buf2[pos];
        }
    }

    /* Right */
    for (i = 0; i < height; i++) {
        size_t pos = i * u16_stride + width;
        size_t j;
        uint16_t *dst = &buf2[pos];
        for (j = 0; j < u16_padding_right; j++) {
            dst[j] = buf2[pos-1];
        }
    }

    /* Apply gaussian blur horizontally */
    OMP_DYNAMIC_FOR(threads, i)
    for (i=0; i < height; i++) {
        size_t j;
        size_t pos = i * u16_stride;
        fv_multiply_u16_row_sse2(&buf1[pos], &buf2[pos], width, &k[0]);
        for (j = 1; j < ksize; j++) {
            fv_multiply_add_unaligned_u16_row_sse2(&buf1[pos], &buf2[pos - j*2],
                                                       width, &k[8*j]);
            fv_multiply_add_unaligned_u16_row_sse2(&buf1[pos], &buf2[pos + j*2],
                                                       width, &k[8*j]);
        }
    }

/*
    src = (uint16_t *) buf1;
    for (i = 0; i < height; i++) {
        size_t row = i * width;
        for (j = 0; j < width; j++) {
            size_t pos = row + j;
            int value = (int) in[pos] +
                        ((int) lrintf((float) pc->amount * ((float) (in[pos]<<8) - (float) (src[j]))) >> 8);
            out[pos] = (uint8_t) SATURATE_8_BITS_FULL(value);
        }
        src += u16_stride;
    }
*/

    OMP_DYNAMIC_FOR(threads, i)
    for (i = 0; i < height; i++) {
        size_t pos = i * width;
        fv_unsharp_mask_u16_sse2(&out[pos], &in[pos], &buf1[i * u16_stride],
                                 pc->amount_vector.data.s16, width);
    }

    return 1;
}


static void update_opt(PrivateContext *pc) {
#if HAVE_ASM
    int i;
#endif
    /* Reset buffers and kernel */
    pc->frame.format.total_bytes = 0;
    pc->luma_kernel.format.total_bytes = 0;

    /* Select actual optimization based on user-selected parameter and
     * available CPU features */
    switch (pc->user_opt) {
        case OPT_AUTO:
#if ARCH_X86
            if (pc->cpu_features & FV_CPU_FLAG_SSE2)
                pc->opt = OPT_U16_SIMD;
            else if (pc->cpu_features & FV_CPU_FLAG_SSE)
                pc->opt = OPT_FLOAT_SIMD;
            else if (pc->cpu_features & FV_CPU_FLAG_MMX)
                pc->opt = OPT_U16_SIMD;
            else {
                fv_log_info("MMX/SSE/SSE2 support not available. "
                            "Falling back to x87 instruction set.");
                pc->opt = OPT_FLOAT;
            }
            break;
#else
            fv_log_warning("No SIMD implementation available for the host "
                           "CPU. Falling back to generic floating-point.");
#endif
        case OPT_FLOAT:
            pc->opt = OPT_FLOAT;
            break;
        case OPT_FLOAT_OPENCL:
#if HAVE_OPENCL
            pc->opt = OPT_FLOAT_OPENCL;
            break;
#else
            fv_log_warning("Filter has not been compiled with OpenCL "
                           "support. Falling back to CPU implementation.");
#endif
        case OPT_FLOAT_SIMD:
#if ARCH_X86
            if (pc->cpu_features & FV_CPU_FLAG_SSE)
                pc->opt = OPT_FLOAT_SIMD;
            else {
                fv_log_warning("SSE support not available. "
                               "Falling back to x87 instruction set.");
                pc->opt = OPT_FLOAT;
            }
#else
            fv_log_warning("No SIMD implementation available for the host "
                           "CPU. Falling back to generic floating-point.");
            pc->opt = OPT_FLOAT;
#endif
            break;
        case OPT_U16:
            pc->opt = OPT_U16;
            break;
        case OPT_U16_SIMD:
#if ARCH_X86
            if (pc->cpu_features & (FV_CPU_FLAG_MMX | FV_CPU_FLAG_SSE2))
                pc->opt = OPT_U16_SIMD;
            else {
                fv_log_info("MMX/SSE2 support not available. "
                            "Falling back to generic floating-point.");
                pc->opt = OPT_FLOAT;
                /* pc->opt = OPT_U16; */
            }
#else
            fv_log_warning("No SIMD implementation available for the host "
                           "CPU. Falling back to generic floating-point.");
            pc->opt = OPT_FLOAT;
#endif
            break;
        default:
            pc->opt = OPT_FLOAT;
    }


    /* Set data format parameters for intermediate calculations */
    switch (pc->opt) {
        case OPT_U16:
            pc->vector.total_bytes   = 4;
            pc->vector.num_elements  = 2;
            pc->vector.element_bytes = 2;
            pc->vector.element_type  = FV_TYPE_UINT16;
            pc->process_plane        = process_plane_u16;
            fv_log_info("fvsharpen implementation: int16");
            break;
#if HAVE_ASM
        case OPT_U16_SIMD:
            if (pc->cpu_features & FV_CPU_FLAG_SSE2) {
                pc->vector.total_bytes  = 16;
                pc->vector.num_elements = 8;
            } else {
                pc->vector.total_bytes  = 8;
                pc->vector.num_elements = 4;
            }
            pc->vector.element_bytes = 2;
            pc->vector.element_type  = FV_TYPE_UINT16;
            pc->process_plane = process_plane_u16_simd;
            fv_log_info("fvsharpen implementation: int16_simd");

            if (pc->amount_vector.data.u8 != NULL)
                fv_free(pc->amount_vector.data.u8);
            pc->amount_vector.data.u8 = (uint8_t *)
                    fv_malloc(pc->vector.total_bytes);
            pc->amount_vector.data.s16[0] = (int16_t)
                    (lrint(511.0 * pc->amount));
            for (i = 1; i < 8; i++) {
                pc->amount_vector.data.s16[i] =
                        pc->amount_vector.data.s16[0];
            }
            break;
        case OPT_FLOAT_SIMD:
            if (pc->cpu_features & FV_CPU_FLAG_AVX) {
                pc->vector.total_bytes  = 32;
                pc->vector.num_elements = 8;
            } else {
                pc->vector.total_bytes  = 16;
                pc->vector.num_elements = 4;
            }
            pc->vector.element_bytes = 4;
            pc->vector.element_type  = FV_TYPE_FLOAT;
            pc->process_plane        = process_plane_float_simd;
            fv_log_info("fvsharpen implementation: float_simd");
            break;
#endif
#if HAVE_OPENCL
        case OPT_FLOAT_OPENCL:
            pc->vector.total_bytes   = 4;
            pc->vector.num_elements  = 1;
            pc->vector.element_bytes = 4;
            pc->vector.element_type  = FV_TYPE_FLOAT;
            pc->process_plane        = process_plane_float_opencl;
            fv_log_info("fvsharpen implementation: float_opencl");
            break;
#endif
        case OPT_FLOAT:
        default:
            pc->vector.total_bytes   = 4;
            pc->vector.num_elements  = 1;
            pc->vector.element_bytes = 4;
            pc->vector.element_type  = FV_TYPE_FLOAT;
            pc->process_plane        = process_plane_float;
            fv_log_info("fvsharpen implementation: float");
            break;
    }
}


static int filter_init(FvFilterContext *fc) {
#ifdef DEBUG
    char *cpu_features_string;
#endif
    PrivateContext *pc = (PrivateContext *) calloc(1, sizeof(PrivateContext));
    if (pc == NULL) return 0;

    fc->filter = &fvsharpen;
    fc->priv_size = sizeof(PrivateContext);
    fc->priv = pc;

    pc->sigma       = 1.0;
    pc->amount      = 1.0;
    pc->kernel_size = 0;
    pc->components  = 15;

    pc->cpu_features = fv_get_cpu_features();
    pc->user_opt = OPT_AUTO;
    update_opt(pc);


#ifdef HAVE_OPENCL
    pc->opencl_context.use_gpu = 1;
    init_opencl(pc);
#endif

#ifdef DEBUG
    cpu_features_string = fv_list_flags(
            fv_get_cpu_feature_flag_descriptions(),
            fv_printable_cpu_features(pc->cpu_features), ",");
    if (cpu_features_string != NULL)
        fv_log_info("Detected CPU features: %s\n", cpu_features_string);
    free(cpu_features_string);
#endif
    return 1;
}

static void filter_uninit(FvFilterContext *fc) {
    PrivateContext *pc = (PrivateContext *) fc->priv;
    if (pc == NULL) return;
    fv_free(pc->frame.data.u8);
    fv_free(pc->luma_kernel.data.u8);
    fv_free(pc->amount_vector.data.u8);
    free(pc);
    fc->priv = NULL;
}

static int filter_set_input_format(FvFilterContext *fc,
        enum FvFormat format, uint32_t width, uint32_t height) {
    PrivateContext *pc = (PrivateContext *) fc->priv;
    if ((format > FV_FORMAT_0) && (format < FV_FORMAT_LAST)) {
        fc->in_format = format;
    }
    fc->in_width = width;
    fc->in_height = height;    

    if (pc == NULL) return 0;
    return 1;
}

static int filter_set_parameter(FvFilterContext *fc, unsigned int param_id,
        const FvValue *value) {
    int    ret;
    PrivateContext *pc = (PrivateContext *) fc->priv;
    switch (param_id) {
        case PARAM_SIGMA:
            ret = fv_value_get_checked_double(value, &pc->sigma);
            if (ret) {
                pc->luma_kernel.format.total_bytes = 0;
                return 1;
            } else return 0;
        case PARAM_KERNEL_SIZE:
            ret = fv_value_get_checked_uint32(value, &pc->kernel_size);
            if (ret) {
                pc->luma_kernel.format.total_bytes = 0;
                /* Force frame buffer reallocation if padding needs
                 * to be increased. */
                pc->frame.format.num_elements = 0;
                return 1;
            } else return 0;
        case PARAM_AMOUNT:
            ret = fv_value_get_checked_double(value, &pc->amount);
            if (ret) {
                update_opt(pc);
                return 1;
            } return 0;
        case PARAM_COMPONENTS:
            return fv_value_get_checked_uint32(value, &pc->components);
        case PARAM_CPU_FEATURES:
            ret = fv_value_get_checked_flags(value, &pc->cpu_features);
            if (ret) {
                update_opt(pc);
#ifdef DEBUG
                {
                char *cpu_features_string = fv_list_flags(
                        fv_get_cpu_feature_flag_descriptions(),
                        fv_printable_cpu_features(pc->cpu_features), ",");
                fv_log_info("User-selected CPU features: %s\n",
                            cpu_features_string);
                free(cpu_features_string);
                }
#endif
                return 1;
            } else return 0;
        case PARAM_OPTIMIZATION:
            ret = fv_value_get_checked_enum(value, (int32_t *) &pc->user_opt);
            if (ret) {
                update_opt(pc);
                return 1;
            } return 0;
        default:
            return 0;
    }
}

static int filter_get_parameter(FvFilterContext *fc, unsigned int param_id,
        FvValue *value) {
    PrivateContext *pc = (PrivateContext *) fc->priv;
    switch (param_id) {
        case PARAM_SIGMA:
            fv_value_set_double(value, pc->sigma);
            return 1;
        case PARAM_KERNEL_SIZE:
            fv_value_set_uint32(value, pc->kernel_size);
            return 1;
        case PARAM_AMOUNT:
            fv_value_set_double(value, pc->amount);
            return 1;
        case PARAM_COMPONENTS:
            fv_value_set_uint32(value, pc->components);
            return 1;
        case PARAM_CPU_FEATURES:
            fv_value_set_flags(value, pc->cpu_features);
            return 1;
        case PARAM_OPTIMIZATION:
            fv_value_set_enum(value, pc->opt);
            return 1;
        default:
            return 0;
    }
}


static int filter_process(FvFilterContext *fc,
                          const uint8_t *in, uint8_t *out, size_t size) {
    int ret;
    uint32_t luma_size, chroma_size;
    PrivateContext *pc = (PrivateContext *) fc->priv;

    luma_size = fc->in_width * fc->in_height;
    chroma_size = luma_size >> 2;

    if (3 * fc->in_width * fc->in_height / 2 != size) {
        memcpy(out, in, size);
        return 0;
    }
    if (pc->luma_kernel.format.total_bytes == 0) {
        ret = init_kernel(pc);
        if (!ret) {
            memcpy(out, in, size);
            return 0;
        }
    }
    if (pc->frame.format.num_elements != luma_size) {
        ret = init_buffer(pc, fc->in_width, fc->in_height, fc->in_format);
        if (!ret) {
            memcpy(out, in, size);
            return 0;
        }
    }
    if (pc->components & 1) {
        pc->process_plane(pc, &pc->luma_kernel, in, out,
                          pc->frame.data.u8,
                          fc->in_width, fc->in_height);
    } else memcpy(out, in, luma_size);

    if (pc->components & 2) {
        pc->process_plane(pc, &pc->chroma_kernel, &in[luma_size], &out[luma_size],
                          pc->frame.data.u8,
                          fc->in_width >> 1, fc->in_height >> 1);
    } else memcpy(&out[luma_size], &in[luma_size], chroma_size);

    if (pc->components & 4) {
        pc->process_plane(pc, &pc->chroma_kernel, &in[luma_size+chroma_size], &out[luma_size+chroma_size],
                          pc->frame.data.u8,
                          fc->in_width >> 1, fc->in_height >> 1);
    } else memcpy(&out[luma_size+chroma_size], &in[luma_size+chroma_size], chroma_size);

    return 1;
}

FvFilter fvsharpen = {
    .name           = "fvsharpen",
    .longname       = "Gaussian Blur and Unsharp Mask (USM)",
    .classification = "Filter/Effect/Video",
    .description    = "Apply gaussian blur or unsharp mask to video",
    .author         = "Niko Mikkilä <nm@phnet.fi>",

    .formats        = (enum FvFormat[]) {FV_FORMAT_I420, FV_FORMAT_YV12, FV_FORMAT_0},
    .parameters     = (const FvParam[]) {
          { PARAM_SIGMA,
            "sigma", "Sigma",
            "Filter radius as the sigma parameter (or standard deviation) "
            "of a normalized gaussian distribution.",
            FV_PARAM_RANGE_DOUBLE(1.0, 0.0, 1000.0) },
          { PARAM_KERNEL_SIZE,
            "kernelsize", "Convolution kernel size",
            "Size of the 1-dimensional gaussian blur convolution kernel. "
            "Even sizes are increased by one to get a kernel that can be "
            "applied symmetrically. Default value is 0: "
            "automatic setting based on sigma, floor(6*sigma)+1",
            FV_PARAM_RANGE_UINT32(0, 0, 8192) },
          { PARAM_AMOUNT,
            "amount", "Amount",
            "Amount of blurring/sharpening to apply on the video. "
            "Positive values sharpen, negative values blur.",
            FV_PARAM_RANGE_DOUBLE(1.0, -1000.0, 1000.0) },
          { PARAM_COMPONENTS,
            "components", "Color Components",
            "Color components to process. For YCbCrA colorspace, "
            "1=Y, 2=Cb, 4=Cr and 8=Alpha. These values can be added "
            "together. For example, Y and Cr are processed when using "
            "value 5 and all channels are processed when using 15.",
            FV_PARAM_RANGE_UINT32(15, 0, 15) },
#ifdef DEBUG
          { PARAM_CPU_FEATURES,
            "cpufeatures", "CPU feature flags",
            "Manual override of detected CPU features. Intended to be used "
            "for debugging and filter development.",
            FV_PARAM_FLAGS(fv_get_cpu_feature_flag_descriptions, 0) },
          { PARAM_OPTIMIZATION,
            "opt", "Filter optimization",
            "Manual selection of an optimized filter implementation.",
            FV_PARAM_ENUM(get_optimizations, OPT_AUTO) },
#endif
          FV_PARAM_END
    },
    .init             = filter_init,
    .uninit           = filter_uninit,
    .set_input_format = filter_set_input_format,
    .set_parameter    = filter_set_parameter,
    .get_parameter    = filter_get_parameter,
    .process          = filter_process
};


/**
 * @file      fvtsmooth.c
 * @brief     Temporal smoothing (mean, median) video filter.
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */


#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <stdint.h>
#include <string.h>

/* Turn off specific GCC warnings triggered by GStreamer and GLib headers */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpadded"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#include <gst/gst.h>
#include <gst/video/video.h>
#pragma GCC diagnostic pop

#include "fvutil.h"


gboolean fvtsmooth_init(GstPlugin *plugin);


GST_DEBUG_CATEGORY_STATIC(gst_tsmooth_debug);
#define GST_CAT_DEFAULT gst_tsmooth_debug


#define GST_TYPE_TSMOOTH \
  (gst_tsmooth_get_type())
#define GST_TSMOOTH(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),GST_TYPE_TSMOOTH,GstTSmooth))
#define GST_TSMOOTH_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),GST_TYPE_TSMOOTH,GstTSmoothClass))
#define GST_IS_TSMOOTH(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),GST_TYPE_TSMOOTH))
#define GST_IS_TSMOOTH_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),GST_TYPE_TSMOOTH))

#define TSMOOTH_MEAN    0
#define TSMOOTH_MEDIAN  1

struct ComponentInfo {
    int offset;
    int width;
    int height;
    int row_stride;
    int pixel_stride;
};

typedef struct GstTSmooth {
    GstElement     element;
    GstPad        *sinkpad, *srcpad;

    int            nframes;
    int            type;

    /* GstVideoInfo vinfo; */
    GstVideoFormat format;
    int            width;
    int            height;
    int            ncomponents;

    struct ComponentInfo cinfo[4];

    GstBuffer    **cache;
    int            cache_next;
    int            cache_size;

    uint32_t      *tmp;
} GstTSmooth;

typedef struct GstTSmoothClass {
    GstElementClass parent_class;
} GstTSmoothClass;

GType gst_tsmooth_get_type(void);


enum {
  LAST_SIGNAL
};

enum {
  PROP_0,
  PROP_FRAMES,
  PROP_TYPE
};

static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS("video/x-raw-yuv"));

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS("video/x-raw-yuv"));

GST_BOILERPLATE(GstTSmooth, gst_tsmooth, GstElement, GST_TYPE_ELEMENT)

static void gst_tsmooth_set_property(GObject *object, guint prop_id,
        const GValue *value, GParamSpec *pspec);
static void gst_tsmooth_get_property(GObject *object, guint prop_id,
        GValue *value, GParamSpec *pspec);

static gboolean gst_tsmooth_set_caps(GstPad *pad, GstCaps *caps);

static GstFlowReturn gst_tsmooth_chain_mean(GstPad *pad, GstBuffer *buf);

static GstFlowReturn gst_tsmooth_chain_median(GstPad *pad, GstBuffer *buf);


static void cache_free(GstTSmooth *filter) {
    int i;
    for (i = 0; i < filter->cache_size; i++) {
        gst_buffer_unref(filter->cache[i]);
    }
    free(filter->cache);
}

static void cache_init(GstTSmooth *filter) {
    if (filter->cache != NULL) {
        cache_free(filter);
    }
    filter->cache = (GstBuffer **) malloc((size_t) filter->nframes * sizeof(GstBuffer *));
    filter->cache_next = 0;
    filter->cache_size = 0;
}

static GstBuffer *cache_add_buffer(GstTSmooth *filter, GstBuffer *buf) {
    GstBuffer *ret = NULL;
    if (filter->cache_size < filter->nframes) {
        filter->cache_size++;
    } else {
        if (filter->cache_next >= filter->cache_size)
            filter->cache_next = 0;
        ret = filter->cache[filter->cache_next];
        gst_buffer_unref(ret);
    }
    gst_buffer_ref(buf);
    filter->cache[filter->cache_next] = buf;
    filter->cache_next++;
    return ret;
}

#define cache_prev_buffer(f) (((f)->cache_next == 0) ? NULL : (f)->cache[(f)->cache_next-1])

static void gst_tsmooth_base_init(gpointer gclass) {
    GstElementClass *element_class = GST_ELEMENT_CLASS(gclass);

    gst_element_class_set_details_simple(element_class,
            "FvTSmooth",
            "Filter/Effect/Video",
            "Forevid Temporal Smoothing filter for video",
            "Niko Mikkilä <nm@phnet.fi>");

    gst_element_class_add_pad_template(element_class,
            gst_static_pad_template_get(&src_factory));
    gst_element_class_add_pad_template(element_class,
            gst_static_pad_template_get(&sink_factory));
}


static void gst_tsmooth_class_init(GstTSmoothClass *gclass) {
    GObjectClass    *gobject_class    = (GObjectClass    *) gclass;
    /* GstElementClass *gstelement_class = (GstElementClass *) gclass; */

    gobject_class->set_property = gst_tsmooth_set_property;
    gobject_class->get_property = gst_tsmooth_get_property;

    g_object_class_install_property(gobject_class, PROP_FRAMES,
            g_param_spec_int("frames", "Frames",
                    "Temporal width of the filter in frames. "
                    "Note! Longer ranges may require tweaks to video "
                    "output sink (for example: xvimagesink max-lateness=-1 "
                    "sync=0)",
                    1, 1000, 9, (GParamFlags) G_PARAM_READWRITE));
    g_object_class_install_property(gobject_class, PROP_TYPE,
            g_param_spec_int("type", "Type",
                    "Filter type: 0=mean, 1=median",
                    0, 1, 0, (GParamFlags) G_PARAM_READWRITE));
}

static void gst_tsmooth_init(GstTSmooth *filter, GstTSmoothClass *gclass) {
    /* unused parameter */
    (void) gclass;

    filter->sinkpad = gst_pad_new_from_static_template(&sink_factory, "sink");
    gst_pad_set_setcaps_function(filter->sinkpad,
            GST_DEBUG_FUNCPTR(gst_tsmooth_set_caps));
    gst_pad_set_getcaps_function(filter->sinkpad,
            GST_DEBUG_FUNCPTR(gst_pad_proxy_getcaps));
    gst_pad_set_chain_function(filter->sinkpad,
            GST_DEBUG_FUNCPTR(gst_tsmooth_chain_mean));

    filter->srcpad = gst_pad_new_from_static_template(&src_factory, "src");
    gst_pad_set_getcaps_function(filter->srcpad,
            GST_DEBUG_FUNCPTR(gst_pad_proxy_getcaps));

    gst_element_add_pad(GST_ELEMENT(filter), filter->sinkpad);
    gst_element_add_pad(GST_ELEMENT(filter), filter->srcpad);
    filter->nframes = 10;
    filter->type = TSMOOTH_MEAN;
    filter->cache = NULL;
    filter->cache_size = 0;
    filter->cache_next = 0;
    filter->tmp = NULL;
}

static void gst_tsmooth_set_property(GObject *object, guint prop_id,
                                    const GValue *value, GParamSpec *pspec) {
    GstTSmooth *filter = (GstTSmooth *) object;

    switch (prop_id) {
        case PROP_FRAMES:
            filter->nframes = g_value_get_int(value);
            cache_init(filter);
            break;
        case PROP_TYPE:
            filter->type = g_value_get_int(value);
            if (filter->type == TSMOOTH_MEAN) {
                gst_pad_set_chain_function(filter->sinkpad,
                        GST_DEBUG_FUNCPTR(gst_tsmooth_chain_mean));
            } else {
                size_t tmpsize = (size_t) (4 * filter->width *
                        filter->height) * sizeof(uint32_t);
                memset(filter->tmp, 0, tmpsize);
                gst_pad_set_chain_function(filter->sinkpad,
                        GST_DEBUG_FUNCPTR(gst_tsmooth_chain_median));
            }
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

static void gst_tsmooth_get_property(GObject *object, guint prop_id,
                                    GValue *value, GParamSpec *pspec) {
    GstTSmooth *filter = (GstTSmooth *) object;

    switch (prop_id) {
        case PROP_FRAMES:
            g_value_set_int(value, filter->nframes);
            break;
        case PROP_TYPE:
            g_value_set_int(value, filter->type);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID(object, prop_id, pspec);
            break;
    }
}

/* GstElement vmethod implementations */

static gboolean gst_tsmooth_set_caps(GstPad *pad, GstCaps *caps) {
    GstTSmooth *filter;
    GstPad     *otherpad;
    int i;
    size_t tmpsize;

    filter = (GstTSmooth *) gst_pad_get_parent(pad);
    otherpad = (pad == filter->srcpad) ? filter->sinkpad : filter->srcpad;
    gst_object_unref(filter);

    /* gst_video_info_from_caps(filter->vinfo, caps); */
    gst_video_format_parse_caps(caps, &filter->format,
                                &filter->width, &filter->height);
    if (gst_video_format_is_gray(filter->format)) filter->ncomponents = 1;
    else filter->ncomponents = 3;
    for (i = 0; i < filter->ncomponents; i++) {
        filter->cinfo[i].offset = gst_video_format_get_component_offset(
                filter->format, i, filter->width, filter->height);
        filter->cinfo[i].width = gst_video_format_get_component_width(
                filter->format, i, filter->width);
        filter->cinfo[i].height = gst_video_format_get_component_height(
                filter->format, i, filter->height);
        filter->cinfo[i].row_stride = gst_video_format_get_row_stride(
                filter->format, i, filter->width);
        filter->cinfo[i].pixel_stride = gst_video_format_get_pixel_stride(
                filter->format, i);
    }
    if (gst_video_format_has_alpha(filter->format)) {
        filter->ncomponents++;
        filter->cinfo[i].offset = gst_video_format_get_component_offset(
                filter->format, 3, filter->width, filter->height);
        filter->cinfo[i].width = gst_video_format_get_component_width(
                filter->format, 3, filter->width);
        filter->cinfo[i].height = gst_video_format_get_component_height(
                filter->format, 3, filter->height);
        filter->cinfo[i].row_stride = gst_video_format_get_row_stride(
                filter->format, 3, filter->width);
        filter->cinfo[i].pixel_stride = gst_video_format_get_pixel_stride(
                filter->format, 3);
    }
    cache_init(filter);
    if (filter->tmp != NULL) fv_free(filter->tmp);
    tmpsize = (size_t) (4 * filter->width * filter->height) * sizeof(uint32_t);
    filter->tmp = (uint32_t *) fv_malloc(tmpsize);
    memset(filter->tmp, 0, tmpsize);

    return gst_pad_set_caps(otherpad, caps);
}

static void mean(GstTSmooth *filter, GstBuffer *out) {
    uint8_t  *out_frame = GST_BUFFER_DATA(out);
    uint32_t *tmp = filter->tmp;
    size_t    frame_size = (size_t) (filter->width * filter->height);
    int       cache_size = filter->cache_size;
    int i;

    if (tmp == NULL) return;

    for (i = 0; i < filter->ncomponents; i++) {
        struct ComponentInfo *cinfo = &filter->cinfo[i];
        uint32_t *tmp_row;
        uint8_t  *out_row = out_frame + cinfo->offset;
        int n, y;
        memset(tmp, 0, frame_size * sizeof(uint32_t));
        for (n = 0; n < cache_size; n++) {
            uint8_t  *in_frame = GST_BUFFER_DATA(filter->cache[n]);
            uint8_t  *in_row   = in_frame + cinfo->offset;
            tmp_row = tmp;
            for (y = 0; y < cinfo->height; y++) {
                int x, p;
                for (x = 0, p = 0; x < cinfo->width; x++, p += cinfo->pixel_stride) {
                    tmp_row[x] += (uint32_t) in_row[p];
                }
                in_row += cinfo->row_stride;
                tmp_row += filter->width;
            }
        }
        tmp_row  = tmp;
        for (y = 0; y < cinfo->height; y++) {
            int x, p;
            for (x = 0, p = 0; x < cinfo->width; x++, p += cinfo->pixel_stride) {
                out_row[p] = (uint8_t) (tmp_row[p] / (uint32_t) cache_size);
            }
            out_row += cinfo->row_stride;
            tmp_row += filter->width;
        }
    }
}

struct MedianContext {
    uint8_t value;
    uint8_t edge_low;
    uint8_t edge_high;
};

#define NCOMPONENTS 3

static void median_init(GstTSmooth *filter) {
    struct MedianContext *mc = (struct MedianContext *) filter->tmp;
    uint8_t  *added = GST_BUFFER_DATA(cache_prev_buffer(filter));
    size_t    frame_size = (size_t) (filter->width * filter->height);
    int       cache_size = filter->cache_size;
    uint8_t   half = (uint8_t) (cache_size >> 1);
    int i;

    if (mc == NULL) return;
    if (cache_size == 1) {
        /* First frame in cache. Copy to median context */
        for (i = 0; i < NCOMPONENTS; i++) {
            struct ComponentInfo *cinfo = &filter->cinfo[i];
            struct MedianContext *mc_row = mc + (frame_size * (size_t) i);
            intptr_t              p_row = cinfo->offset;
            int y;
            for (y = 0; y < cinfo->height; y++, p_row += cinfo->row_stride,
                                           mc_row += filter->width) {
                int x;
                intptr_t p = p_row;
                for (x = 0; x < cinfo->width; x++, p += cinfo->pixel_stride) {
                    struct MedianContext *median = mc_row + x;
                    median->value = added[p];
                    median->edge_low = 0;
                    median->edge_high = 0;
                }
            }
        }
        return;
    }

    for (i = 0; i < NCOMPONENTS; i++) {
        struct ComponentInfo *cinfo = &filter->cinfo[i];
        struct MedianContext *mc_row = mc + (frame_size * (size_t) i);
        intptr_t              p_row = cinfo->offset;
        int n, y;
        for (y = 0; y < cinfo->height; y++, p_row += cinfo->row_stride,
                                       mc_row += filter->width) {
            int x;
            intptr_t p = p_row;
            for (x = 0; x < cinfo->width; x++, p += cinfo->pixel_stride) {
                int find_new_median = 0;
                struct MedianContext *median = mc_row + x;
                if (added[p] < median->value) {
                    median->edge_low++;
                    median->edge_high++;
                } else if (added[p] == median->value) median->edge_high++;

                if (median->edge_low > half) {
                    find_new_median = -1;
                } else if (median->edge_high < half) {
                    find_new_median = 1;
                }

                if (find_new_median < 0) {
                    uint8_t oldmedian = median->value;
                    uint8_t candidate = 0;
                    uint8_t num = 0;
                    for (n = 0; n < cache_size; n++) {
                        uint8_t c = (GST_BUFFER_DATA(filter->cache[n]) + p)[0];
                        if (c < oldmedian) {
                            if (c > candidate) {
                               candidate = c;
                               num = 1;
                            } else if (c == candidate) {
                                num++;
                            }
                        }
                    }
                    median->edge_low = (uint8_t) (half + 1 - num);
                    median->edge_high = half;
                    median->value = candidate;
                } else if (find_new_median > 0) {
                    uint8_t oldmedian = median->value;
                    uint8_t candidate = 255;
                    uint8_t num = 0;
                    for (n = 0; n < cache_size; n++) {
                        uint8_t c = (GST_BUFFER_DATA(filter->cache[n]) + p)[0];
                        if (c > oldmedian) {
                            if (c < candidate) {
                               candidate = c;
                               num = 1;
                            } else if (c == candidate) {
                                num++;
                            }
                        }
                    }
                    median->edge_low = half;
                    median->edge_high = (uint8_t) (half - 1 + num);
                    median->value = candidate;
                }
            }
        }
    }
}


static void median_update(GstTSmooth *filter, GstBuffer *out) {
    struct MedianContext *mc = (struct MedianContext *) filter->tmp;
    uint8_t  *removed = GST_BUFFER_DATA(out);
    uint8_t  *added = GST_BUFFER_DATA(cache_prev_buffer(filter));
    size_t    frame_size = (size_t) (filter->width * filter->height);
    int       cache_size = filter->cache_size;
    uint8_t   half = (uint8_t) (cache_size >> 1);
    int i;

    if (mc == NULL) return;

    for (i = 0; i < NCOMPONENTS; i++) {
        struct ComponentInfo *cinfo = &filter->cinfo[i];
        struct MedianContext *mc_row = mc + (frame_size * (size_t) i);
        intptr_t              p_row = cinfo->offset;
        int n, y;
        for (y = 0; y < cinfo->height; y++, p_row += cinfo->row_stride,
                                       mc_row += filter->width) {
            int x;
            intptr_t p = p_row;
            for (x = 0; x < cinfo->width; x++, p += cinfo->pixel_stride) {
                int find_new_median = 0;
                struct MedianContext *median = mc_row + x;
                if (removed[p] < median->value) {
                    if (added[p] == median->value) median->edge_low--;
                    else if (added[p] > median->value) {
                        median->edge_low--;
                        median->edge_high--;
                        if (median->edge_high < half) {
                            find_new_median = 1;
                        }
                    }
                } else if (removed[p] > median->value) {
                    if (added[p] == median->value) median->edge_high++;
                    else if (added[p] < median->value) {
                        median->edge_low++;
                        median->edge_high++;
                        if (median->edge_low > half) {
                            find_new_median = -1;
                        }
                    }
                } else {
                    if (added[p] < median->value) {
                        median->edge_low++;
                        if (median->edge_low > half) {
                            find_new_median = -1;
                        }
                    } else if (added[p] > median->value) {
                        median->edge_high--;
                        if (median->edge_high < half) {
                            find_new_median = 1;
                        }
                    }
                }
                if (find_new_median < 0) {
                    uint8_t oldmedian = median->value;
                    uint8_t candidate = 0;
                    uint8_t num = 0;
                    for (n = 0; n < cache_size; n++) {
                        uint8_t c = (GST_BUFFER_DATA(filter->cache[n]) + p)[0];
                        if (c < oldmedian) {
                            if (c > candidate) {
                               candidate = c;
                               num = 1;
                            } else if (c == candidate) {
                                num++;
                            }
                        }
                    }
                    median->edge_low = (uint8_t) (half + 1 - num);
                    median->edge_high = half;
                    median->value = candidate;
                } else if (find_new_median > 0) {
                    uint8_t oldmedian = median->value;
                    uint8_t candidate = 255;
                    uint8_t num = 0;
                    for (n = 0; n < cache_size; n++) {
                        uint8_t c = (GST_BUFFER_DATA(filter->cache[n]) + p)[0];
                        if (c > oldmedian) {
                            if (c < candidate) {
                               candidate = c;
                               num = 1;
                            } else if (c == candidate) {
                                num++;
                            }
                        }
                    }
                    median->edge_low = half;
                    median->edge_high = (uint8_t) (half - 1 + num);
                    median->value = candidate;
                }
                removed[p] = median->value;
            }
        }
    }
}


static GstFlowReturn gst_tsmooth_chain_mean(GstPad *pad, GstBuffer *buf) {
    GstTSmooth *filter;
    GstBuffer  *outbuf;

    filter = (GstTSmooth *) GST_OBJECT_PARENT(pad);

    buf = gst_buffer_make_writable(buf);
    outbuf = cache_add_buffer(filter, buf);

    if (outbuf == NULL) {
        return GST_FLOW_OK;
    } else {
        mean(filter, outbuf);
        return gst_pad_push(filter->srcpad, outbuf);
    }
}

static GstFlowReturn gst_tsmooth_chain_median(GstPad *pad, GstBuffer *buf) {
    GstTSmooth *filter;
    GstBuffer  *outbuf;

    filter = (GstTSmooth *) GST_OBJECT_PARENT(pad);

    buf = gst_buffer_make_writable(buf);
    outbuf = cache_add_buffer(filter, buf);

    if (outbuf == NULL) {
        median_init(filter);
        return GST_FLOW_OK;
    } else {
        median_update(filter, outbuf);
        return gst_pad_push(filter->srcpad, outbuf);
    }
}



gboolean fvtsmooth_init(GstPlugin *plugin) {
    GST_DEBUG_CATEGORY_INIT(gst_tsmooth_debug, "plugin",
            0, "FvTSmooth");

    return gst_element_register(plugin, "fvtsmooth", GST_RANK_NONE,
            GST_TYPE_TSMOOTH);
}


/**
 * @file      test_fvmultiarray.c
 * @brief     Unit tests for fvmultiarray
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

/*
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
*/
#include <math.h>
#include <string.h>

#include "fvmultiarray.h"
#include "fvvalue.h"
#include "utest.h"

struct TestData {
    const char *input;
    const char *expected_output;
};

static int fvtree_parse_test(const char *input, const char *expected_output,
                             FvValueConstraint *constraint) {
    FvTree tree;
    char *str;
    int ret;

    if (!fvtree_from_string(&tree, input, &fv_parse_float, constraint)) {
        return 1;
    }
    str = fvtree_to_string(&tree);

    ret = strcmp(str, expected_output);
    if (ret) {
        printf("\n            Input:    %s\n"
               "            Output:   %s\n"
               "            Expected: %s\n",
               input, str, expected_output);
        ret = 1;
    }
    fvtree_free(&tree);
    free(str);
    return ret;
}

static int fvmultiarray_parse_test(const char *input,
                                   const char *expected_output,
                                   FvValueConstraint *constraint,
                                   uint32_t data_vector_size,
                                   uint32_t num_dimensions,
                                   uint32_t flags) {
    FvMultiArray ma;
    char *str;
    int ret;

    if (fvmultiarray_from_string(&ma, input, &fv_parse_float, constraint,
                                 data_vector_size, num_dimensions, flags)) {
        return 1;
    }
    str = fvmultiarray_to_string(&ma);

    ret = strcmp(str, expected_output);
    if (ret) {
        printf("\n            Input:    %s\n"
               "            Output:   %s\n"
               "            Expected: %s\n",
               input, str, expected_output);
        ret = 1;
    }
    fvmultiarray_free(&ma);
   free(str);
    return ret;
}


int main (void) {
    FvValueConstraint constraint = {
        .defval.f = 0.0F,
        .min.f = -INFINITY,
        .max.f = INFINITY,
        .list = NULL,
        .type = FV_TYPE_FLOAT
    };
    /*
    FvValueConstraint constraint = {
        .defval.s32 = 0,
        .min.s32 = INT32_MIN,
        .max.s32 = INT32_MAX,
        .list = NULL,
        .type = FV_TYPE_INT32
    };
    */

    int i = 0;
    int failed = 0;

    struct TestData set1[] = {
        {"",
         "(empty)"},
        {"7",
         "[7.000000]"},
        {"-0.007",
         "[-0.007000]"},
        {"inf",
         "[inf]"},
        {"test",
         "(empty)"},
        {"1.001 2.002 3.003",
         "[1.001000, 2.002000, 3.003000]"},
        {"1.001, 2.002, 3.003",
         "[1.001000, 2.002000, 3.003000]"},
        {"  1.001 \t 2.002\n   3.003",
         "[1.001000, 2.002000, 3.003000]"},
        {"1.001,      , 3.003",
         "[1.001000, (empty), 3.003000]"},
        {",,",
         "(empty)"},
        {"1.1;2.1",
         "[[1.100000], [2.100000]]"},
        {"[]",
         "(empty)"},
        {"[,]",
         "(empty)"},
        {"[,]]",
         "(empty)"},
        {"1 2 a b",
         "[1.000000, 2.000000, (empty), (empty)]"},
        {"[a,1.0 test b]",
         "[(empty), 1.000000, (empty), (empty)]"},
        {"X[1.1,] 1.2",
         "[(empty), [1.100000, (empty)], 1.200000]"},
        {"[1.001 2.002 3.003]",
         "[1.001000, 2.002000, 3.003000]"},
        {"[11 12 13][21 22 23]",
         "[[11.000000, 12.000000, 13.000000], [21.000000, 22.000000, 23.000000]]"},
        {"[[1.1 -1.2 1.3](2.1 -2.2 2.3)]",
         "[[1.100000, -1.200000, 1.300000], [2.100000, -2.200000, 2.300000]]"},
        {"1.1 1.2; 2.1 2.2; 3.1 3.2",
         "[[1.100000, 1.200000], [2.100000, 2.200000], [3.100000, 3.200000]]"},
        {"[11; 21 22; 31 32 33]",
         "[[11.000000], [21.000000, 22.000000], [31.000000, 32.000000, 33.000000]]"},
        {"{.1, [1 2], [11 12; 21 22]}",
         "[0.100000, [1.000000, 2.000000], [[11.000000, 12.000000], [21.000000, 22.000000]]]"},
        {"{[(1.1e6)]}",
         "[[[1100000.000000]]]"},
        {"{[(1.1e6])}",
         "[[[1100000.000000]]]"},
        {NULL, NULL}
    };

    fv_set_log_level(FV_LOG_LEVEL_ERROR);

    run_start();
    for (i = 0; set1[i].input != NULL; i++) {
        run_test("Parse FvTree from a string",
                 assert(fvtree_parse_test(set1[i].input,
                                          set1[i].expected_output,
                                          &constraint) == 0));
    }
    run_end();
    failed += num_failed_runs(); 
/*
    run_start();
    for (i = 0; set1[i].input != NULL; i++) {
        run_test("Parse FvMultiArray from a string",
                 assert(fvmultiarray_parse_test(set1[i].input,
                                set1[i].expected_output,
                                &constraint,
                                1,
                                4,
                                1+4) == 0));
    }
    run_end();
    failed += num_failed_runs(); 
*/
    return failed;
}


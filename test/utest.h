/**
 * @file   utest.h
 * @brief  Minimal unit test system for C from
 *         http://www.shike2.com/blog/2010/12/written-unit-test-using-c.html
 * @author Roberto Vargas
 */

#ifndef UTEST_H
#define UTEST_H

#ifdef NDEBUG
#undef NDEBUG
#endif

#include <assert.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>

int utst_actual_test;
int utst_test_ok;
jmp_buf utst_jmp;


static void utst_sigabrt(int dummy) {
    /* Silence warning about unused parameter */
    (void) dummy;
    longjmp(utst_jmp, 1);
}


#define run_start() \
    do { \
        utst_test_ok = 0; \
        utst_actual_test = 0; \
    } while (0)

#define run_test(message, test) \
    do { \
        printf("Subtest %2d: %s ... ", utst_actual_test, message); \
        signal(SIGABRT, utst_sigabrt); \
        ++utst_actual_test; \
        if (!setjmp(utst_jmp)) { \
            test; \
            puts("OK"); \
            ++utst_test_ok; \
        } else { \
            puts("FAILED"); \
        } \
        fflush(stdin); \
    } while (0)

#define num_failed_runs() (utst_actual_test - utst_test_ok)

#define run_end() \
    do { \
        if (utst_actual_test == utst_test_ok) \
            puts("ALL SUBTESTS PASSED"); \
        else \
            printf("FAILED %d SUBTESTS\n", num_failed_runs()); \
    } while (0)


#endif /* UTEST_H */

/**
 * @file      cpu.c
 * @brief     CPU feature detection functions
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include <string.h>

#include "config.h"
#include "fvutil.h"
#include "x86.h"

static const FvFlag fv_cpu_feature_flags[] = {
#if ARCH_X86
    {FV_CPU_FLAG_MMX,     "MMX",                               "mmx"     },
    {FV_CPU_FLAG_MMX2,    "SSE integer functions / AMD MMXext","mmxext"  },
    {FV_CPU_FLAG_SSE,     "SSE",                               "sse"     },
    {FV_CPU_FLAG_SSE2,    "SSE2",                              "sse2"    },
    {FV_CPU_FLAG_SSE2SLOW,"SSE2 supported, but slow",          "sse2slow"},
    {FV_CPU_FLAG_SSE3,    "SSE3 (Prescott)",                   "sse3"    },
    {FV_CPU_FLAG_SSSE3,   "Supplemental SSE3 (Conroe)",        "ssse3"   },
    {FV_CPU_FLAG_ATOM, "some SSSE3 instructions are slow on Atom", "atom"},
    {FV_CPU_FLAG_SSE41,   "SSE4.1 (Penryn)",                   "sse41"   },
    {FV_CPU_FLAG_SSE42,   "SSE4.2 (Nehalem)",                  "sse42"   },
    {FV_CPU_FLAG_AVX,     "AVX (Sandy Bridge)",                "avx"     },
    {FV_CPU_FLAG_XOP,     "XOP (Bulldozer)",                   "xop"     },
    {FV_CPU_FLAG_FMA4,    "FMA4 (Bulldozer)",                  "fma4"    },
    {FV_CPU_FLAG_3DNOW,   "3DNow! (AMD-only, K6-2)",           "3dnow"   },
    {FV_CPU_FLAG_3DNOWEXT,"Extended 3DNow! (AMD-only, Athlon)","3dnowext"},
    {FV_CPU_FLAG_CMOV,    "Conditional move, i686",            "cmov"    },
#endif
    {0, NULL, NULL}
};

int fv_get_cpu_info(FvCPUInfo *cpuinfo) {
    memset(cpuinfo, 0, sizeof(FvCPUInfo));
#if HAVE_ASM
#if ARCH_X86
    return fv_get_cpu_info_x86(cpuinfo);
#endif /* ARCH_X86 */
#endif /* HAVE_ASM */
    return 1;
}

uint32_t fv_get_cpu_features(void) {
    uint32_t flags = 0;
#if HAVE_ASM
#if ARCH_X86
    flags = fv_get_cpu_features_x86();
#endif /* ARCH_X86 */
#endif /* HAVE_ASM */
    return flags;
}

uint32_t fv_printable_cpu_features(uint32_t flags) {
#if ARCH_X86
    uint32_t f;

    /* Turn off SSE2 flag if SSE2SLOW is switched on */
    f = FV_CPU_FLAG_SSE2 | FV_CPU_FLAG_SSE2SLOW;
    if ((flags & f) == f) flags &= ~FV_CPU_FLAG_SSE2;

    /* Turn off SSE4.1 flag if SSE4.2 is switched on */
    f = FV_CPU_FLAG_SSE41 | FV_CPU_FLAG_SSE42;
    if ((flags & f) == f) flags &= ~FV_CPU_FLAG_SSE41;
#endif
    return flags;
}

char *fv_get_cpu_feature_string(uint32_t features, const char *delim) {
    uint32_t f = fv_printable_cpu_features(features);
    return fv_list_flags(fv_cpu_feature_flags, f, delim);
}

const FvFlag *fv_get_cpu_feature_flag_descriptions(void) {
    return fv_cpu_feature_flags;
}



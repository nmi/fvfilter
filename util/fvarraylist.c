/**
 * @file      fvarraylist.c
 * @brief     Type-agnostic resizable array implementation
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */


#include <stdio.h>
#include <stdint.h>

#include <fvutil.h>

#include "fvarraylist.h"


int fv_array_list_init(FvArrayList *al, uint32_t element_bytes,
                       int initial_capacity, uint32_t flags) {
    al->flags = flags;
    al->size = 0;
    al->element_bytes = element_bytes;
    al->data.u8 = fv_malloc(initial_capacity * element_size);
    if (al->data == NULL) {
        al->capacity = 0;
        return 1;
    }
    al->capacity = initial_capacity;
    return 0;
}


void fv_array_list_free(FvArrayList *al) {
    fv_free(al->data.u8);
    al->data = NULL;
    al->size = 0;
    al->capacity = 0;
}


int fv_array_list_append(FvArrayList *al, void *element) {
    if (al->size >= al->capacity) {
        void *p = fv_malloc((al->capacity << 1) * al->element_bytes);
        if (p == NULL) {
            fv_log_error("Failed to allocate memory. This may happen "
                         "when the process hits a memory limit.");
            return 1; 
        }
        memcpy(p, al->data.p, al->size * al->element_bytes),
        fv_free(al->data.p);
        al->data.p = p;
        al->capacity <<= 1;
    }
    memcpy(&al->data.u8[al->size * (size_t) al->element_bytes], element,
            al->element_bytes);
    al->size++;
    return 0;
}


int fv_array_list_insert(FvArrayList *al, size_t i, void *element) {
    size_t newsize = 1 + ((i > al->size) ? i : al->size);
    if (newsize > al->capacity) {
        void *p = fv_malloc((newsize << 1) * al->element_bytes);
        if (p == NULL) {
            fv_log_error("Failed to allocate memory. This may happen "
                         "when the process hits a memory limit.");
            return 1; 
        }
        memcpy(p, al->data.p, al->size * al->element_bytes),
        fv_free(al->data.p);
        al->data.p = p;
        al->capacity = newsize << 1;
    }
    if (i > al->size) {
        /* Insert zero elements between the new element and array end */
        memset(al->data.u8[al->size * al->element_bytes], 0,
               (i - al->size) * al->element_bytes);
    } else if (i < al->size) {
        uint8_t *p = &al->data.u8[i * (size_t) al->element_bytes];
        memmove(p, p + al->element_bytes, (al->size-i) * al->element_bytes);
    }
    al->size = newsize;
    memcpy(&al->data.u8[al->size * (size_t) al->element_bytes], element,
            al->element_bytes);
    return 0;
}


void fv_array_list_remove(FvArrayList *al, size_t i) {
    if (i >= al->size) return;
    if (i == al->size - 1) {
        al->size--; 
    } else {
        uint8_t *p = &al->data.u8[i * (size_t) al->element_bytes];
        memmove(p, p + al->element_bytes,
                (al->size - 1 - i) * al->element_bytes);
        al->size--;
    }
    if (al->flags | FV_ARRAY_LIST_DOWNSIZE) {
        fv_array_list_downsize(al);
    }
}


void fv_array_list_remove_range(FvArrayList *al, size_t i1, size_t i2) {
    if (i1 > i2) {
        size_t tmp = i1;
        i1 = i2;
        i2 = tmp;
    }
    if (i1 >= al->size) return;
    if (i2 >= al->size - 1) {
        al->size = i1; 
    } else {
        uint8_t *p = &al->data.u8[i1 * (size_t) al->element_bytes];
        memmove(p, p + (i2 - i1 + 1) * al->element_bytes,
                (al->size - i2) * al->element_bytes);
        al->size -= (i2 - i1 + 1);
    }
    if (al->flags | FV_ARRAY_LIST_DOWNSIZE) {
        fv_array_list_downsize(al);
    }
}


int fv_array_list_downsize(FvArrayList *al) {
    if (al->capacity > (al->size << 2)) {
        void *p = fv_malloc((al->capacity >> 1) * al->element_bytes);
        if (p == NULL) {
            fv_log_error("Failed to allocate memory. This may happen "
                         "when the process hits a memory limit.");
            return 1; 
        }
        memcpy(p, al->data.p, al->size * al->element_bytes),
        fv_free(al->data.p);
        al->data.p = p;
        al->capacity >>= 1;
    }
    return 0;
}


#define fv_array_list_get(al, index) \
        ((void *) ((index >= al->size) \
                       ? NULL \
                       : al->data.c[index * al->element_bytes]))


#define fv_array_list_get_unchecked(al, index) \
        ((void *) al->data.c[index * al->element_bytes])



/**
 * @file      fvarraylist.h
 * @brief     Type-agnostic resizable array implementation
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef FVARRAYLIST_H
#define FVARRAYLIST_H

#include <stdio.h>
#include <stdint.h>

#include <fvutil.h>

#ifdef __cplusplus
extern "C" {
#endif


/**
 * FvArrayList flag for downsizing the array automatically when removing
 * elements
 */ 
#define FV_ARRAY_LIST_DOWNSIZE 1


/**
 * Type-agnostic resizable array.
 */ 
typedef struct FvArrayList {
    union FvArrayPtr data;
    size_t           capacity;
    size_t           size;
    uint32_t         element_size;
    uint32_t         flags;
} FvArrayList;


/**
 * Returns a void pointer to the requested element of an FvArrayList.
 * This version checks that the index falls within the array.
 *
 * You may also use fv_array_list_get_unchecked for faster access
 * (especially when false sharing between parallel threads is an issue).
 * With built-in types, you may also access the array directly
 * through FvArraylist.data.* pointers.
 *
 * @param al    pointer to an FvArrayList instance
 * @param index element index
 *
 * @return void pointer to the element or NULL if index is out of range.
 *         Cast the pointer to the proper type.
 */
#define fv_array_list_get(al, index) \
        ((void *) ((index >= al->size) \
                       ? NULL \
                       : al->data.c[index * al->element_bytes]))


/**
 * Returns a void pointer to a requested element of an FvArrayList.
 * This version does not check whether the index is valid.
 *
 * @param al    pointer to an FvArrayList instance
 * @param index element index
 *
 * @return void pointer to the element. Cast the pointer to the proper type.
 */
#define fv_array_list_get_unchecked(al, index) \
        ((void *) al->data.c[index * al->element_bytes])


/**
 * Initializes an FvArrayList.
 *
 * @param al               FvArrayList to initialize
 * @param element_bytes    size of an individual element in bytes
 * @param initial_capacity amount of space to allocate initially, specified
 *                         as number of elements
 * @param flags            optional flags such as FV_ARRAY_LIST_DOWNSIZE
 *
 * @return 0 on success, 1 if allocation fails
 */
int fv_array_list_init(FvArrayList *al, uint32_t element_bytes,
                       int initial_capacity, uint32_t flags);

/**
 * Frees an FvArrayList.
 *
 * @param al FvArrayList instance to free
 */
void fv_array_list_free(FvArrayList *al);


/**
 * Adds an element at the end of an FvArrayList.
 *
 * @param al      FvArrayList instance
 * @param element pointer to an element of size al->element_size.
 *                Element data will be copied to the array.
 *
 * @return 0 on success, 1 if resizing the array fails
 */
int fv_array_list_append(FvArrayList *al, void *element);


/**
 * Inserts an element at a specific position in an FvArrayList.
 * If the position is greater than array size, zero elements are
 * added between the inserted element and the current array end.
 *
 * @param al      FvArrayList instance
 * @param i       position into which the new element is inserted
 * @param element pointer to an element of size al->element_size.
 *                Element data will be copied to the array.
 * 
 * @return 0 on success, 1 if resizing the array fails.
 */
int fv_array_list_insert(FvArrayList *al, size_t i, void *element);


/**
 * Removes an element from an FvArrayList.
 *
 * @param al FvArrayList instance
 * @param i  element index
 */
void fv_array_list_remove(FvArrayList *al, size_t i);


/**
 * Removes a range of elements from an FvArrayList.
 *
 * @param al FvArrayList instance
 * @param i1 index of the first element to remove
 * @param i2 index of the last element to remove. If i2 is less than i1,
 *           i1 and i2 are swapped before proceeding.
 */
void fv_array_list_remove_range(FvArrayList *al, size_t i1, size_t i2);


/**
 * Halves the allocated memory area of an FvArrayList if it covers
 * the number of elements stored in the data structure over four times.
 *
 * @return 0 on success, 1 if resizing the array fails
 */
int fv_array_list_downsize(FvArrayList *al);


#ifdef __cplusplus
}
#endif

#endif /* FVARRAYLIST_H */


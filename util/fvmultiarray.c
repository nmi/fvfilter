/**
 * @file      fvmultiarray.c
 * @brief     Type-agnostic multi-dimensional array implementation
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//#include "fvutil.h"

#include "fvmultiarray.h"
#include "fvvalue.h"


/* Turn off warnings about struct padding. */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpadded"

typedef struct TreeNode {
    const char       *strpos;
    struct TreeNode  *nextsibling;
    struct TreeNode  *firstchild;
    size_t            nchildren;
    union FvValueData value;
    int               used;
    char              bracket;
} TreeNode;

#pragma GCC diagnostic pop


static int fvmultiarray_dimensions_from_fvtree(FvMultiArray *ma,
        FvTree *fvt, size_t level) {
    size_t i = 0;
    int ret = 0;
    if (level >= ma->num_dimensions) return 1;
    for (i = 0; fvt; i++, fvt = fvt->nextsibling) {
        if (fvt->firstchild) {
            ret |= fvmultiarray_dimensions_from_fvtree(ma, fvt->firstchild,
                                                       level + 1);
        }
    }
    if (i & 1) {
        if (ma->flags & FV_MULTI_ARRAY_EVEN_DIMENSIONS) i++;
    } else {
        if (ma->flags & FV_MULTI_ARRAY_ODD_DIMENSIONS) i++;
    }
    if (ma->dimensions[level] < i) ma->dimensions[level] = i;
    return ret;
}


static int fvmultiarray_fill_from_fvtree(FvMultiArray *ma, FvTree *fvt,
        size_t level, size_t pos, size_t size) {
    size_t i = 0;
    int ret = 0;
    if (level >= ma->num_dimensions) return 1;
    if (ma->flags & FV_MULTI_ARRAY_ALIGN_CENTER) {
        pos += ma->stride[level] * ((ma->dimensions[level] - size) >> 1);
    }
    for (i = 0; fvt; i++, pos += ma->stride[level], fvt = fvt->nextsibling) {
        if (fvt->firstchild) {
            ret |= fvmultiarray_fill_from_fvtree(ma, fvt->firstchild,
                                                 level + 1, pos,
                                                 fvt->nchildren);
        } else {
            union FvValuePtr ptr;
            size_t p = pos;
            if (ma->flags & FV_MULTI_ARRAY_ALIGN_CENTER) {
                size_t l;
                for (l = level + 1; l < ma->num_dimensions; l++) {
                    p += ma->stride[l] * (ma->dimensions[l] >> 1);
                }
            }
            ptr.u8 = ma->array.data.u8 + p * ma->array.format.vector_bytes;
            fv_vector_broadcast_value(ptr, &fvt->value,
                                      ma->array.format.num_elements);
        }
    }
    return ret;
}

int fvmultiarray_from_string(FvMultiArray *ma, const char *str,
        int (*parse_value)(const char *ptr, char **endptr,
                           FvValue *out, FvValueConstraint *cr),
        FvValueConstraint *value_constraint,
        uint32_t data_vector_size,
        uint32_t num_dimensions, uint32_t flags) {
    FvTree  fvtree;
    size_t  height, stride;

    /* Init */
    memset(ma, 0, sizeof(FvMultiArray));
    ma->array.format.element_type  = value_constraint->type;
    ma->array.format.element_bytes = fv_type_sizeof(value_constraint->type);
    ma->array.format.num_elements  = data_vector_size;
    ma->array.format.vector_bytes  = data_vector_size *
                                     ma->array.format.element_bytes;
    ma->defval.type  = value_constraint->type;
    ma->defval.value.u64 = value_constraint->defval.u64;
    ma->flags  = flags;

    /* Parse input string to a tree structure */
    height = fvtree_from_string(&fvtree, str, parse_value, value_constraint);

    /* Check that requested number of dimensions is not exceeded */
    if (!height) {
        fv_log_error("Error parsing input array.");
        return 1;
    } else if (num_dimensions && (height > num_dimensions)) {
        fv_log_error("Parsed array has too many dimensions (%d, when "
                     "requested maximum is %d)",
                     height, num_dimensions);
        fvtree_free(&fvtree);
        return 1;
    }
    if (num_dimensions) ma->num_dimensions = num_dimensions;
    else ma->num_dimensions = (uint32_t) height;

    printf("\nFvMultiArray height: %d\n", ma->num_dimensions);

    /* Allocate memory for array dimensions and strides */
    ma->dimensions = (size_t *) malloc(ma->num_dimensions * sizeof(size_t));
    ma->stride = (size_t *) malloc(ma->num_dimensions * sizeof(size_t));
    if ((ma->dimensions == NULL) || (ma->stride == NULL)) {
        fv_log_error("Failed to allocate memory for FvMultiArray");
        goto exit_error;
    }

    /* Calculate dimensions and strides from the tree */
    for (height = 0; height < ma->num_dimensions; height++) {
        ma->dimensions[height] = 1;
    }
    if (fvmultiarray_dimensions_from_fvtree(ma, &fvtree, 0)) {
        fv_log_error("Tree gained height after last check.");
        goto exit_error;
    }

    for (stride = 1, height = ma->num_dimensions; height > 0;) {
        height--;
        ma->stride[height] = stride;
        stride *= ma->dimensions[height];
    }

    for (height = 0; height < ma->num_dimensions; height++) {
        printf("FvMultiArray d: %d, size: %d, stride: %d\n",
               height, ma->dimensions[height], ma->stride[height]);
    }

    /* Allocate memory for data */
    ma->array.format.num_vectors = stride;
    ma->array.format.total_bytes = stride * ma->array.format.vector_bytes;
    ma->array.data.p = fv_malloc(ma->array.format.total_bytes);
    if (ma->array.data.p == NULL) {
        fv_log_error("Failed to allocate memory for FvMultiArray");
        goto exit_error;
    }

    /* Fill with the default value */
    fv_vector_broadcast_value(ma->array.data, &ma->defval,
                              ma->array.format.num_vectors *
                              (size_t) ma->array.format.num_elements);

    /* Copy data from the tree to the array */
    fvmultiarray_fill_from_fvtree(ma, fvtree.firstchild, 1, 0,
                                  fvtree.nchildren);

    fvtree_free(&fvtree);
    return 0;

exit_error:
    free(ma->dimensions);
    free(ma->stride);
    memset(ma, 0, sizeof(FvMultiArray));
    fvtree_free(&fvtree);
    return 1;
}



void fvmultiarray_free(FvMultiArray *ma) {
    free(ma->dimensions);
    free(ma->stride);
    fv_free(ma->array.data.p);
    memset(ma, 0, sizeof(FvMultiArray));
}


static size_t fvtree_measure_string_recursive(FvTree *n,
        size_t max_value_len, char *tmp) {
    if (n->nchildren == 0) {
        if (n->value.type == FV_TYPE_NONE) {
            strncpy(tmp, "(empty)", max_value_len);
            return ((max_value_len < 7) ? max_value_len : 7);
        } else {
            return fv_value_to_string(&n->value, tmp, max_value_len);
        }
    } else {
        size_t len = 0;
        FvTree *child = n->firstchild;
        len += 2;  /* brackets */
        len += fvtree_measure_string_recursive(child, max_value_len, tmp);
        while ((child = child->nextsibling) != NULL) {
            len += 2;  /* comma and space between elements */
            len += fvtree_measure_string_recursive(child, max_value_len, tmp);
        }
        return len;
    }
}

static char *fvtree_to_string_recursive(FvTree *n, char *str,
        size_t max_value_len) {
    if (n->nchildren == 0) {
        if (n->value.type == FV_TYPE_NONE) {
            size_t len = ((max_value_len < 7) ? max_value_len : 7);
            strncpy(str, "(empty)", len);
            return str + len;
        } else {
            return str + fv_value_to_string(&n->value, str, max_value_len);
        }
    } else {
        FvTree *child = n->firstchild;
        *str = '[';
        str++;
        str = fvtree_to_string_recursive(child, str, max_value_len);
        while ((child = child->nextsibling) != NULL) {
            str[0] = ',';
            str[1] = ' ';
            str += 2;
            str = fvtree_to_string_recursive(child, str, max_value_len);
        }
        *str = ']';
        str++;
    }
    return str;
}


char *fvtree_to_string(FvTree *n) {
    char tmp[256], *str, *end;
    size_t len;

    if (n == NULL) {
        return NULL;
    }
    len = fvtree_measure_string_recursive(n, 256, tmp);
    str = (char *) malloc(len + 1);
    if (str == NULL) {
        fv_log_error("Failed to allocate memory for a tree listing");
        return NULL;
    }
    end = fvtree_to_string_recursive(n, str, 256);
    if (end != str + len) {
        fv_log_error("String length mismatch: expected %lld, got %lld",
                     len, (size_t) (end - str));
    }
    str[len] = '\0';
    return str;
}

static size_t fvmultiarray_measure_string_recursive(FvMultiArray *ma,
        size_t apos, size_t alevel,
        size_t max_value_len, char *tmp) {
    size_t i;
    size_t len = 2;   /* brackets */
    /*int semicolon;
    if ((alevel > 0) && (ma->num_dimensions - alevel == 2)) semicolon = 1;
    else {
        semicolon = 0;
    }
    */
    if (alevel >= ma->num_dimensions) return 0;
    else if (ma->num_dimensions - alevel == 1) {
        for (i = 0; i < ma->dimensions[alevel]; i++, apos++) {
            FvValue val;
            fv_vector_get_value(&val, &ma->array, apos, 0);
            len += fv_value_to_string(&val, tmp, max_value_len) + 1;
            //printf("val1: %f\n", tmp);
        }
        len -= 1;
    } else {
        for (i = 0; i < ma->dimensions[alevel]; i++) {
            len += fvmultiarray_measure_string_recursive(ma, apos,
                        alevel + 1, max_value_len, tmp);
            apos += ma->stride[alevel];
        }
    }
    return len;
}

static char *fvmultiarray_to_string_recursive(FvMultiArray *ma,
        size_t apos, size_t alevel, char *str, size_t max_value_len) {
    size_t i;
    if (alevel >= ma->num_dimensions) return str;
    *str = '[';
    str++;
    if (ma->num_dimensions - alevel == 1) {
        for (i = 0; i < ma->dimensions[alevel]; i++, apos++) {
            FvValue val;
            size_t len;
            fv_vector_get_value(&val, &ma->array, apos, 0);
            len = fv_value_to_string(&val, str, max_value_len);
            //printf("val2: %f\n", str);
            str += len;
            *str = ' ';
            str++;
        }
        str--;
    } else {
        for (i = 0; i < ma->dimensions[alevel]; i++) {
            str = fvmultiarray_to_string_recursive(ma, apos,
                        alevel + 1, str, max_value_len);
            apos += ma->stride[alevel];
        }
    }
    *str = ']';
    str++;
    return str;
}


char *fvmultiarray_to_string(FvMultiArray *ma) {
    char tmp[256], *str, *end;
    size_t len;

    if (ma == NULL) {
        return NULL;
    }
    len = fvmultiarray_measure_string_recursive(ma, 0, 1, 256, tmp);
    str = (char *) malloc(len + 1);
    if (str == NULL) {
        fv_log_error("Failed to allocate memory for an array listing");
        return NULL;
    }
    end = fvmultiarray_to_string_recursive(ma, 0, 1, str, 256);
    if (end != str + len) {
        fv_log_error("String length mismatch: expected %lld, got %lld",
                     len, (size_t) (end - str));
    }
    *end = '\0';
    return str;
}



static TreeNode *tree_trim(TreeNode *n, size_t *num_nodes) {
    TreeNode *prev = NULL;
    TreeNode *child = n->firstchild;
    size_t nchildren = 0;

    n->firstchild = NULL;
    while (child != NULL) {
        TreeNode *next = child->nextsibling;
        child = tree_trim(child, num_nodes);
        if (child != NULL) {
            nchildren++;
            if (prev == NULL) {
                n->firstchild = child;
            } else {
                prev->nextsibling = child;
            }
            prev = child;
            if (child->nextsibling != next) {
                child = child->nextsibling;
                while (child != NULL) {
                    prev = child;
                    child = child->nextsibling;
                }
            }
        }
        child = next;
    }
    n->nchildren = nchildren;
    *num_nodes += nchildren;

    /* Remove unused nodes. */
    if (!n->used && n->firstchild) return n->firstchild;
    else return n;
}


static FvTree *tree_to_fvtree_recursive(TreeNode *n, FvTree *fn,
                                        enum FvType type, size_t depth,
                                        size_t *max_depth) {
    FvTree *next_fn = fn->firstchild;
    if (depth > *max_depth) *max_depth = depth;
    while (n) {
        fn->nchildren = n->nchildren;
        if (n->nchildren > 0) {
            fn->value.value.u64 = 0;
            fn->value.type = FV_TYPE_NONE;
            fn->firstchild = next_fn;
            next_fn->firstchild = next_fn + 1; 
            next_fn = tree_to_fvtree_recursive(n->firstchild, next_fn, type,
                                               depth + 1, max_depth);
        } else {
            if (!n->used) fn->value.type = FV_TYPE_NONE;
            else fn->value.type = type;
            fn->value.value.u64 = n->value.u64;
            fn->firstchild = NULL;
        }
        n = n->nextsibling;
        if (n) fn->nextsibling = next_fn;
        else   fn->nextsibling = NULL;
        fn = next_fn;
        next_fn = fn + 1;
    }
    return fn;
}

static size_t tree_to_fvtree(TreeNode *n, FvTree *fvtree, enum FvType type,
                              size_t num_nodes) {
    size_t max_depth = 0;
    FvTree *allocated_tree = (FvTree *) calloc(num_nodes, sizeof(FvTree));
    if (allocated_tree == NULL) {
        fv_log_error("Failed to allocate memory for an FvTree");
        return 0;
    }
    fvtree->firstchild = allocated_tree;
    tree_to_fvtree_recursive(n, fvtree, type, 1, &max_depth);
    if (fvtree->firstchild == NULL) free(allocated_tree);
    return max_depth;
} 


#define ADD_EMPTY_LEAF \
                if (state >= 2) { \
                    parent = prevsibling[level]; \
                    n = nextfree; \
                    nextfree++; \
                    if (parent->firstchild == NULL) \
                        parent->firstchild = n; \
                    else prevsibling[level+1]->nextsibling = n; \
                    n->strpos = p; \
                    parent->nchildren++; \
                    prevsibling[level+1] = n; \
                }

size_t fvtree_from_string(FvTree *fvtree, const char *str,
        int (*parse_value)(const char *ptr, char **endptr,
                           FvValue *out, FvValueConstraint *cr),
        FvValueConstraint *value_constraint) {
    TreeNode   *root, *n, *parent, *nextfree;
    TreeNode  **prevsibling;
    const char *p = str;
    char       *endp;
    size_t      nnodes = 2;
    FvValue     v;
    size_t      d = 1, level = 1;
    int         state = 3;
    char        matching_bracket;

    v.type = value_constraint->type;

    /* Estimate number of nodes and the maximum depth of the tree */
    while (*p != '\0') {
        switch (*p) {
            case '(':
            case '[':
            case '{':
                state = 3;
                nnodes += 2;
                level += 1;
                if (d < level) d = level;
                break;
            case ')':
            case ']':
            case '}':
                if (state >= 2) nnodes += 1;
                state = 1;
                level -= 1;
                if (level < 1) level = 1;
                break;
            case ';':
                if (state >= 2) nnodes += 1;
                state = 3;
                nnodes += 1;
                break;
            case ',':
                if (state >= 2) nnodes += 1;
                state = 2;
                break;
            case ' ':
            case '\t':
            case '\n':
                if (state == 0) state = 1;
                break;
            default:
                if (state > 0) {
                    nnodes += 1;
                    state = 0;
                }
                break;
        }
        p++;
    }
    if (state >= 2) nnodes += 1;

    root = (TreeNode *) calloc(nnodes, sizeof(TreeNode));
    if (root == NULL) return 0;
    prevsibling = (TreeNode **) calloc(2 * d + 1, sizeof(void *));
    if (prevsibling == NULL) {
        free(root);
        return 0;
    }

    n = root + 1; 
    root->firstchild = n;
    root->nchildren = 1;
    root->strpos = str;
    n->strpos = str;
    nextfree = n + 1;

    prevsibling[0] = root;
    prevsibling[1] = n;
    level = 1;

    state = 3;
    p = str;
    while (*p != '\0') {
        switch (*p) {
            case '(':
            case '[':
            case '{':
                state = 3;
                parent = prevsibling[level];
                level++;
                n = nextfree;
                nextfree++;
                if (prevsibling[level] != NULL)
                    prevsibling[level]->nextsibling = n;
                else {
                    parent->firstchild = n;
                }
                prevsibling[level] = n;
                parent->nchildren++;
                n->bracket = *p;
                n->strpos = p;

                parent = n;
                level++;
                n = nextfree;
                nextfree++;
                parent->firstchild = n;
                parent->nchildren = 1;
                prevsibling[level] = n;
                n->strpos = p;

                break;
            case ')':
                matching_bracket = '(';
                goto closebracket;
            case ']':
                matching_bracket = '[';
                goto closebracket;
            case '}':
                matching_bracket = '{';
closebracket:
                ADD_EMPTY_LEAF
                state = 1;
                n = prevsibling[level];
                prevsibling[level+1] = NULL;
                prevsibling[level] = NULL;
                level--;
                if (level > 0 && n->used) prevsibling[level-1]->used = 1;
                n = prevsibling[level];
                if (level < 1) {
                    char *tmpstring;
                    tmpstring = strndup(str, (size_t) (p - str + 1));
                    fv_log_warning("Encountered a closing bracket without "
                                   "corresponding opening bracket:\n"
                                   "%s <--", tmpstring);
                    free(tmpstring);
                    level = 1;
                } else {
                    level--;
                    if (n->bracket != matching_bracket) {
                        char *tmpstring;
                        tmpstring = strndup(n->strpos,
                                            (size_t) (p - n->strpos + 1));
                        fv_log_warning("Brackets do not match, "
                                       "'%c' vs. '%c': --> %s <--",
                                       n->bracket, *p, tmpstring);
                        free(tmpstring);
                    }
                }
                break;
            case ';':
                ADD_EMPTY_LEAF
                state = 3;
                prevsibling[level+1] = NULL;
                n = prevsibling[level];
                parent = prevsibling[level-1];
                if (n->used) parent->used = 1;
                parent->nchildren++;
                n->nextsibling = nextfree;

                n = nextfree;
                nextfree++;
                n->strpos = p;
                prevsibling[level] = n;
                break;
            case ',':
                ADD_EMPTY_LEAF
                state = 2;
                break;
            case ' ':
            case '\t':
            case '\n':
                break;
            default:
                parse_value(p, &endp, &v, value_constraint);
                parent = prevsibling[level];
                n = nextfree;
                nextfree++;
                if (parent->firstchild == NULL) parent->firstchild = n;
                else prevsibling[level+1]->nextsibling = n;
                parent->nchildren++;
                prevsibling[level+1] = n;
                n->strpos = p;
                if (endp != p) {
                    n->value.u64 = v.value.u64;
                    n->used = 1;
                    parent->used = 1;
                };
                /* Skip over any unidentified characters and throw
                   an error about them. */
                while (*p != '\0') {
                    switch (*p) {
                        case '(':
                        case '[':
                        case '{':
                        case ')':
                        case ']':
                        case '}':
                        case ';':
                        case ',':
                        case ' ':
                        case '\t':
                        case '\n':
                            goto skip_complete;
                        default:
                            break;
                    }
                    p++;
                }
skip_complete:
                if (p > endp) {
                    char *tmpstring = strndup(endp, (size_t) (p - endp));
                    fv_log_warning("Failed to parse value \"%s\". "
                                   "Expected an entry of type %s.",
                                   tmpstring, fv_type_names[v.type]);
                    free(tmpstring);
                }
                state = 1;
                p--;
                break;
        }
        p++;
    }
    ADD_EMPTY_LEAF

    free(prevsibling);

    nnodes = 0;
    n = tree_trim(root, &nnodes);

    /* Remove previously added extra level from the root of the tree if
       it's not really needed. */
    if ((n->nchildren == 1) &&
        (n->firstchild->nchildren >= 1)) {
        n = n->firstchild;
        nnodes--;
    }

    d = tree_to_fvtree(n, fvtree, v.type, nnodes);
    free(root);

    return d;
}

void fvtree_free(FvTree *fvtree) {
    free(fvtree->firstchild);
    memset(fvtree, 0, sizeof(FvTree));
}





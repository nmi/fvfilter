/**
 * @file      fvmultiarray.h
 * @brief     Type-agnostic tree and multi-dimensional array structure
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef FVMULTIARRAY_H
#define FVMULTIARRAY_H

#include <stdio.h>
#include <stdint.h>

#include "fvutil.h"

#ifdef __cplusplus
extern "C" {
#endif

#define FV_MULTI_ARRAY_ODD_DIMENSIONS  1
#define FV_MULTI_ARRAY_EVEN_DIMENSIONS 1 << 1
#define FV_MULTI_ARRAY_ALIGN_CENTER    1 << 2

/**
 * Type-agnostic multi-dimensional array.
 */
typedef struct FvMultiArray {
    FvVectorData array;
    size_t      *dimensions;
    size_t      *stride;
    size_t       num_dimensions;
    FvValue      defval;
    uint32_t     flags;
} FvMultiArray;


/**
 * Tree of FvValues.
 */
typedef struct FvTree {
    struct FvTree *nextsibling;
    struct FvTree *firstchild;
    size_t         nchildren;
    FvValue        value;
} FvTree;


int fvmultiarray_from_string(FvMultiArray *ma, const char *str,
        int (*parse_value)(const char *ptr, char **endptr,
                           FvValue *out, FvValueConstraint *cr),
        FvValueConstraint *value_constraint,
        uint32_t data_vector_size,
        uint32_t num_dimensions, uint32_t flags);

char *fvmultiarray_to_string(FvMultiArray *ma);
 
void fvmultiarray_free(FvMultiArray *ma);

size_t fvtree_from_string(FvTree *tree, const char *str,
        int (*parse_value)(const char *ptr, char **endptr,
                           FvValue *out, FvValueConstraint *cr),
        FvValueConstraint *value_constraint);

char *fvtree_to_string(FvTree *tree);

void fvtree_free(FvTree *tree);


#ifdef __cplusplus
}
#endif

#endif /* FVMULTIARRAY_H */


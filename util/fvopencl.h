/**
 * @file      fvopencl.h
 * @brief     OpenCL utility function declarations and macros
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef FVOPENCL_H
#define FVOPENCL_H

#include "config.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Return a string for an OpenCL error value.
 * From https://github.com/enjalot/adventures_in_opencl/blob/master/part1/util.cpp
 */
const char* clErrorString(cl_int error);

#ifdef __cplusplus
}
#endif

#endif /* FVOPENCL_H */


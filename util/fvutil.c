/**
 * @file      fvutil.c
 * @brief     Utility functions
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include "config.h"

#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "fvutil.h"


#ifdef __GNUC__
  #define GCC_VERSION (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + \
                       __GNUC_PATCHLEVEL__)
  #if GCC_VERSION >= 40400
    #define attribute_printf(m,n) __attribute__((format(gnu_printf, (m), (n))))
  #else
    #define attribute_printf(m,n) __attribute__((format(printf, (m), (n))))
  #endif
#else
    #define attribute_printf(m,n)
#endif



#define FV_DEFAULT_LOG_TARGET stderr

static const char * const log_level_strings[] = {
    "None", "Error", "Warning", "Info", "Debug"
};

static FILE *log_target = NULL;
static enum fv_log_level log_level = FV_LOG_LEVEL_INFO;


enum fv_log_level fv_get_log_level(void) {
    return log_level;
}

void fv_set_log_level(enum fv_log_level newlevel) {
    log_level = newlevel;
}

void fv_set_log_target(FILE *stream) {
    log_target = stream;
}

FILE *fv_get_log_target(void) {
    if (log_target == NULL) return FV_DEFAULT_LOG_TARGET;
    else return log_target;
}



void attribute_printf(5, 6) fv_log(enum fv_log_level msg_level,
                                   const char *file, const char *function,
                                   int line, const char *format, ...) {
    va_list ap;
    FILE *out = log_target ? log_target : FV_DEFAULT_LOG_TARGET;

    if (msg_level < FV_LOG_LEVEL_ERROR)
        msg_level = FV_LOG_LEVEL_ERROR;
    else if (msg_level > FV_LOG_LEVEL_DEBUG)
        msg_level = FV_LOG_LEVEL_DEBUG; 
    if (msg_level > log_level) return;
    if (out == NULL) return;

    if ((log_level == FV_LOG_LEVEL_DEBUG) && (file != NULL) &&
                                             (function != NULL)) {
        fprintf(out, "[%s in function %s of file %s, line %d]:\n    ",
                log_level_strings[msg_level], function, file, line);
    } else {
        fprintf(out, "[%s in function %s]:\n    ",
                log_level_strings[msg_level], function);
    }
    va_start(ap, format);
    vfprintf(out, format, ap);
    fputs("\n", out);
}

void fv_log_parse_msg(enum fv_log_level msg_level, const char *msg,
                      const char *str,
                      const char *error_start, size_t error_len) {
    FILE *out = log_target ? log_target : FV_DEFAULT_LOG_TARGET;
    int pos = 0;

    if (msg_level < FV_LOG_LEVEL_ERROR)
        msg_level = FV_LOG_LEVEL_ERROR;
    else if (msg_level > FV_LOG_LEVEL_DEBUG)
        msg_level = FV_LOG_LEVEL_DEBUG; 
    if (msg_level > log_level) return;
 
/* Use of %n is safe here since we control the format string.
 * Disable warnings about it */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat-security"
    fprintf(out, "[%s] %s \"%n%s\"",
            log_level_strings[msg_level], msg, &pos, str);
#pragma GCC diagnostic pop
    if (error_start != NULL) {
        int i;
        fprintf(out, ", unknown token \"%.*s\"\n", (int) error_len,
                error_start);
        pos += (int) (error_start - str);
        for (i = 0; i < pos; i++) fputc(' ', out);
        fputs("^\n", out);
    } else fputs("\n", out);
}


/**
 * @internal Returns the closest power of two that is larger than or equal
 * to the given value. Minimum alignment is 8 and maximum alignment is one
 * mebibyte (1024 * 1024 bytes).
 *
 * @param alignment an unsigned integer value
 *
 * @return 8 if alignment is less than 8. Alignment if it is a power of two. *         Next power of two otherwise.
 */
static size_t fv_get_power_of_two_alignment(size_t alignment) {
    if (alignment < 8) alignment = 8;
    else if (alignment > 1024 * 1024) alignment = 1024 * 1024;
    else if ((alignment & (alignment - 1)) != 0) {
        /* Not a power of two. Find next one. */
        size_t a = 8;
        while (a < alignment) a = a << 1;
        alignment = a;
    }
    return alignment;
}

void *fv_malloc_aligned(size_t size, size_t alignment) {
    void *p = NULL;
#if HAVE_POSIX_MEMALIGN
    int ret;
#endif

    alignment = fv_get_power_of_two_alignment(alignment);
    
    /* posix_memalign() is safe, try it first. memalign() is broken
     * on some platforms, so we'll use a custom implementation instead. */
#if HAVE_POSIX_MEMALIGN
    ret = posix_memalign(&p, alignment, size);
    if (ret != 0) return NULL;
#else
    /* Allocate a slightly larger area than asked for, find the first
     * aligned position within the area and store the pointer to
     * the original allocation right before that position. Return
     * the aligned position. fv_free() in turn reads the saved original
     * pointer and calls the system free() function. */
    unsigned char *unaligned_p;
    size_t   overhead = alignment + sizeof(void **) - 1;

    assert(size);
    if (!size || (size > (INT_MAX - overhead)))
        return NULL;

    unaligned_p = (unsigned char *) malloc(size + overhead);
    if (!unaligned_p) return unaligned_p;
    p = unaligned_p + overhead;
    p -= (intptr_t) p & (alignment - 1);
    ((void **) p)[-1] = unaligned_p;
#endif

    return p;
}


void fv_free(void *p) {
#if HAVE_POSIX_MEMALIGN
    free(p);
#else
    free(((void**) p)[-1]);
#endif
}


char *fv_str_underscore_to_camelcase(const char *str) {
    const char *p   = str;
    char       *out;
    size_t      len = 1;
    int         uppercase;

    if (str == NULL) return NULL;

    while (*p) {
        if (*p != '_') ++len;
        ++p;
    }
    out = (char *) malloc(len * sizeof(char));
    if (out == NULL) return NULL;

    p = str;   
    len = 0;
    uppercase = 1;
    while (*p) {
        if (*p == '_') uppercase = 1;
        else {
            if (uppercase) {
                out[len] = (char) toupper(*p); 
                uppercase = 0;
            } else out[len] = *p;
            ++len;
        }
        ++p;
    }
    out[len] = '\0';
    return out;
}



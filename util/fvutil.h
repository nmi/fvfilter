/**
 * @file      fvutil.h
 * @brief     Utility function declarations and macros
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef FVUTIL_H
#define FVUTIL_H

#include "config.h"
#include <stdio.h>
#include <stdint.h>

#include "fvvalue.h"

#ifdef __cplusplus
extern "C" {
#endif


/** 
 * Log levels
 */
enum fv_log_level {
    FV_LOG_LEVEL_NONE    = 0,
    FV_LOG_LEVEL_ERROR   = 1,
    FV_LOG_LEVEL_WARNING = 2,
    FV_LOG_LEVEL_INFO    = 3,
    FV_LOG_LEVEL_DEBUG   = 4
};


/**
 * Logs an error message.
 *
 * @param ...       variable arguments for fprintf()
 */
#define fv_log_error(...) \
    fv_log(FV_LOG_LEVEL_ERROR,   __FILE__, __func__, __LINE__, __VA_ARGS__)

/**
 * Logs a warning message.
 *
 * @param ...       variable arguments for fprintf()
 */
#define fv_log_warning(...) \
    fv_log(FV_LOG_LEVEL_WARNING, __FILE__, __func__, __LINE__, __VA_ARGS__)

/**
 * Logs an informative message.
 *
 * @param ...       variable arguments for fprintf()
 */
#define fv_log_info(...) \
    fv_log(FV_LOG_LEVEL_INFO,      NULL,     NULL,     0,      __VA_ARGS__)

/**
 * Logs a debug message.
 *
 * @param ...       variable arguments for fprintf()
 */
#define fv_log_debug(...) \
    fv_log(FV_LOG_LEVEL_DEBUG,      NULL,     NULL,     0,      __VA_ARGS__)

/**
 * Logs a parse error message.
 *
 * @param ...       variable arguments for fprintf()
 */
#define fv_log_parse_error(msg, str, error_start, error_len) \
    fv_log_parse_msg(FV_LOG_LEVEL_ERROR, (msg), (str), (error_start), \
                     (error_len))

/**
 * Logs a parse warning message.
 *
 * @param ...       variable arguments for fprintf()
 */
#define fv_log_parse_warning(msg, str, error_start, error_len) \
    fv_log_parse_msg(FV_LOG_LEVEL_WARNING, (msg), (str), (error_start), \
                     (error_len))


/**
 * Gets current logging level.
 *
 * @return current logging level
 */
enum fv_log_level fv_get_log_level(void);

/**
 * Sets logging level. To only log error messages, use
 * FV_LOG_LEVEL_ERROR. For errors and warnings, use FV_LOG_LEVEL_WARNING.
 * For all messages, use FV_LOG_LEVEL_INFO or FV_LOG_LEVEL_DEBUG, which
 * also prints source code file names, function names and line numbers.
 *
 * @param newlevel  new logging level
 */
void fv_set_log_level(enum fv_log_level newlevel);

/**
 * Gets logging output stream.
 *
 * @return stream used for logging
 */
FILE *fv_get_log_target(void);


/**
 * Sets target output stream for logging functions.
 *
 * @param newtarget stream to use for logging
 */
void fv_set_log_target(FILE *target);

/**
 * Writes a message to the log. This function should be called through
 * the fv_log_error(), fv_log_warning() and fv_log_info() preprocessor
 * macros.
 *
 * @param msg_level message logging level: FV_LOG_LEVEL_ERROR,
 *                  FV_LOG_LEVEL_WARNING or FV_LOG_LEVEL_INFO
 * @param file      source file name
 * @param function  source function name
 * @param line      source line number
 * @param format    output formatting string to pass to fprintf()
 * @param ...       variable arguments for fprintf()
 */
void fv_log(enum fv_log_level msg_level, const char *file,
            const char *function, int line, const char *format, ...);
 
/**
 * Writes a parse error or warning message to the log.
 *
 * @param msg_level   message logging level: FV_LOG_LEVEL_ERROR,
 *                    FV_LOG_LEVEL_WARNING or FV_LOG_LEVEL_INFO
 * @param msg         error message, such as "Failed to parse flag in"
 * @param str         string that was being parsed
 * @param error_start position of the invalid token in str. Can be NULL
 *                    if exact location of the error is unknown
 * @param error_len   length of the invalid token
 */
void fv_log_parse_msg(enum fv_log_level msg_level, const char *msg,
                      const char *str,
                      const char *error_start, size_t error_len);

/** Minimum memory alignment for fv_malloc. AVX needs 32-byte alignment. */
#define FV_MALLOC_ALIGNMENT 32

/**
 * Allocates memory aligned to FV_MIN_ALIGNMENT.
 * Always use fv_free() to free memory allocated by this function!
 */
#define fv_malloc(s) fv_malloc_aligned(s, FV_MALLOC_ALIGNMENT)

/**
 * Allocates memory aligned to FV_MALLOC_ALIGNMENT bytes.
 * Always use fv_free() to free memory allocated by this function!
 *
 * This is inspired by av_malloc() in libavutil/mem.c and
 * x264_malloc() in x264/common/common.c.
 *
 * @param size      number of bytes to allocate
 * @param alignment minimum alignment of the start address in bytes.
 *                  If the value is not a power of two, closest higher
 *                  power of two is used instead.
 * @return pointer to allocated memory or NULL if allocation fails
 */
void *fv_malloc_aligned(size_t size, size_t alignment);

/**
 * Frees memory allocated by fv_malloc().
 *
 * Do not use this function to free memory allocated by other
 * malloc implementations.
 *
 * @param p pointer to an area allocated by fv_malloc()
 */
void fv_free(void *p);


/**
 * Lists a set of flags as a string. Memory for the string is allocated
 * dynamically, so it must be freed by the caller.
 *
 * @param flags   FvFlag array that ends with a {NULL, 0} entry.
 * @param bitmask mask for selecting flags to print
 * @param delim   a delimiter string for the list
 * @return Allocated string that contains a list of flags. The string must
 *         be freed by the caller. NULL is returned if allocation fails.
 */
char *fv_list_flags(const FvFlag *flags, uint32_t bitmask,
                    const char *delim);


/**
 * Parses flag tokens from a string and returns matching flags as
 * a bitfield.
 *
 * Input string should be formatted in FFmpeg/Libav style:
 * - each token may have an optional prefix "+" or "-" to signal addition or
 *   removal from the set. These characters also work as delimiters.
 * - allowed delimiting characters are ",+-" and white space
 * - special token "all" adds all flags to the set, unless prefixed with
 *   "-", in which case it removes all flags
 * - special token "none" also removes all flags from the set, unless
 *   prefixed with "-", in which case it does nothing
 * - special tokens "list" and "help" can be used to get a list of
 *   available flags, if supported by the calling program
 *
 * Example 1: "f1-f2,+f3" sets flags f1 and f3
 * Example 2: "all -f2" sets all flags except f2
 *
 * @param         str    delimited string of flags
 * @param         flags  FvFlag array that ends with a NULL entry
 * @param[in,out] out    a 32-bit field with individual bits set according
 *                       to parsed flags. This value is also used as
 *                       a default, so it must be initialized before
 *                       calling this function.
 * @param[out]    errpos position of the first error, if any found
 *
 * @return 0 if no errors were encountered while parsing, -1 if user
 *         requested a list of available flags, otherwise
 *         length of the first invalid token. Position of the token
 *         is returned through errpos.
 */
int fv_parse_flags(const char *str, const FvFlag *flags, uint32_t *out,
                   const char **errpos);


/**
 * CPU vendors.
 */
enum FvCPUVendor {
    FV_CPU_VENDOR_UNKNOWN = 0,

    FV_CPU_VENDOR_AMD,
    FV_CPU_VENDOR_CENTAUR,
    FV_CPU_VENDOR_CYRIX,
    FV_CPU_VENDOR_DMP,
    FV_CPU_VENDOR_IDT,
    FV_CPU_VENDOR_INTEL,
    FV_CPU_VENDOR_NEXGEN,
    FV_CPU_VENDOR_NSC,
    FV_CPU_VENDOR_RDC,
    FV_CPU_VENDOR_RISE,
    FV_CPU_VENDOR_SIS,
    FV_CPU_VENDOR_TRANSMETA,
    FV_CPU_VENDOR_UMC,
    FV_CPU_VENDOR_VIA,

    FV_NUM_CPU_VENDORS
};

/**
 * CPU families based on different microarchitectures.
 */
enum FvCPUFamily {
    FV_CPU_FAMILY_UNKNOWN = 0,

    /* AMD, main IA-32 and AMD64 lineage */
    FV_CPU_FAMILY_AMD_486,
    FV_CPU_FAMILY_AMD_K5,
    FV_CPU_FAMILY_AMD_K6,
    FV_CPU_FAMILY_AMD_K7,
    FV_CPU_FAMILY_AMD_K8,
    FV_CPU_FAMILY_AMD_K10,
    FV_CPU_FAMILY_AMD_BOBCAT,
    FV_CPU_FAMILY_AMD_BULLDOZER,

    /* AMD Geode */
    FV_CPU_FAMILY_AMD_GEODE,

    /* Cyrix */
    FV_CPU_FAMILY_CYRIX_486,
    FV_CPU_FAMILY_CYRIX_586,
    FV_CPU_FAMILY_CYRIX_686,

    /* IDT / Centaur Technology Pentium-compatible CPUs */
    FV_CPU_FAMILY_IDT_WINCHIP,

    /* Intel, main IA-32 and Intel 64 lineage */
    FV_CPU_FAMILY_INTEL_486,
    FV_CPU_FAMILY_INTEL_P5,
    FV_CPU_FAMILY_INTEL_P6,
    FV_CPU_FAMILY_INTEL_PM,
    FV_CPU_FAMILY_INTEL_NETBURST,
    FV_CPU_FAMILY_INTEL_CORE,
    FV_CPU_FAMILY_INTEL_NEHALEM,
    FV_CPU_FAMILY_INTEL_SANDY_BRIDGE,
    FV_CPU_FAMILY_INTEL_HASWELL,
    FV_CPU_FAMILY_INTEL_SKYLAKE,

    /* Intel Atom */
    FV_CPU_FAMILY_INTEL_ATOM,

    /* Intel Larrabee / Knight's Corner / Xeon Phi */
    FV_CPU_FAMILY_INTEL_MIC,

    /* Intel IA-64 */
    FV_CPU_FAMILY_INTEL_ITANIUM,
    FV_CPU_FAMILY_INTEL_ITANIUM_2,

    FV_CPU_FAMILY_NEXGEN_586,
    FV_CPU_FAMILY_NSC_GEODE,
    FV_CPU_FAMILY_RDC_IAD,
    FV_CPU_FAMILY_RISE_MP6,
    FV_CPU_FAMILY_SIS_550,
    FV_CPU_FAMILY_DMP_VORTEX86,
    FV_CPU_FAMILY_TRANSMETA_CRUSOE,
    FV_CPU_FAMILY_TRANSMETA_EFFICEON,
    FV_CPU_FAMILY_UMC_486,
    FV_CPU_FAMILY_VIA_C3,
    FV_CPU_FAMILY_VIA_C7,

    FV_NUM_CPU_FAMILIES
};

/* Turn off warnings about struct padding. */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpadded"

/* CPU model information */
typedef struct FvCPUModel {
    enum FvCPUVendor vendor;
    enum FvCPUFamily family;
    uint32_t         min_cpuid;
    uint32_t         max_cpuid;
    /** Pointer to a model description. */
    const char      *model_description;
    /** Manufacturing process size in nanometers */
    int              feature_size;
    /** Lowest frequencies of CPUs sharing this model/stepping ID in MHz */
    int              min_frequency;
    /** Highest frequencies of CPUs sharing this model/stepping ID in MHz */
    int              max_frequency;
} FvCPUModel;

#pragma GCC diagnostic pop

/* CPU information */
typedef struct FvCPUInfo {
    FvCPUModel  model;
    char        raw_vendor_str[16];
    char        raw_brand_str[256];
    const char *vendor_name; 
    const char *family_name; 
    const char *family_description; 

    /**
     * 24-bit CPU model ID:
     * family (12 bits), model (8 bits), type / stepping (8 bits)
     */
    uint32_t    id;

    /* Size of general purpose registers in bits: 32 or 64 */
    int         bitness;

    /* Number of available processors as reported by OpenMP's
     * omp_get_num_procs() */
    int         num_processors;

    /* CPU feature flags */
    uint32_t    features;

    /* L1 cacheline size */
    /*int         l1_cacheline;*/
} FvCPUInfo;


/**
 * Retrieves information about the processor on which the program is
 * running, and writes it to an FvCPUInfo struct.
 *
 * @param cpuinfo pointer to a writable FvCPUInfo struct.
 *
 * @return 0 on success, 1 on failure.
 */
int fv_get_cpu_info(FvCPUInfo *cpuinfo);

/**
 * Returns feature flags which specify extensions supported by the CPU.
 *
 * @return a bitfield of CPU feature flags (FV_CPU_FLAG_*)
 */
uint32_t fv_get_cpu_features(void);

/**
 * Removes redundant information from a set of CPU feature flags for
 * polished display.
 *
 * @param features a bitfield of CPU flags to tweak
 * @return printable flags
 */
uint32_t fv_printable_cpu_features(uint32_t features);

/**
 * Returns a list of CPU features as a delimited string.
 * The string is allocated dynamically and the caller must free it.
 * This function uses fv_printable_cpu_features() to tweak the
 * feature flags before listing them.
 *
 * @param delim delimiter string for the list. For example ", ".
 * @param features a list of CPU features
 *
 * @return a string of CPU features. Must be freed by the caller.
 */
char *fv_get_cpu_feature_string(uint32_t features, const char *delim);

/**
 * Returns an FvFlag array that contains CPU feature names and their
 * bitfield flags.
 *
 * @return an array of FvFlags, terminated with {NULL, 0}
 */
const FvFlag *fv_get_cpu_feature_flag_descriptions(void);


#if ARCH_X86
/* Flag definitions are borrowed from Libav */
                              /* lower 16 bits - CPU features */
#define FV_CPU_FLAG_MMX          0x0001U
#define FV_CPU_FLAG_MMX2         0x0002U /**< SSE integer functions or AMD MMX ext */
#define FV_CPU_FLAG_3DNOW        0x0004U
#define FV_CPU_FLAG_SSE          0x0008U
#define FV_CPU_FLAG_SSE2         0x0010U
#define FV_CPU_FLAG_SSE2SLOW 0x40000000U /**< SSE2 supported, but slow  */
#define FV_CPU_FLAG_3DNOWEXT     0x0020U
#define FV_CPU_FLAG_SSE3         0x0040U /**< Prescott SSE3             */
#define FV_CPU_FLAG_SSSE3        0x0080U /**< Conroe Supplemental SSE3  */
#define FV_CPU_FLAG_ATOM     0x10000000U /**< some SSSE3 instructions are slow on Atom */
#define FV_CPU_FLAG_SSE41        0x0100U /**< Penryn SSE4.1 functions   */
#define FV_CPU_FLAG_SSE42        0x0200U /**< Nehalem SSE4.2 functions  */
#define FV_CPU_FLAG_AVX          0x4000U /**< AVX functions: requires OS support even if YMM registers aren't used */
#define FV_CPU_FLAG_XOP          0x0400U /**< Bulldozer XOP functions   */
#define FV_CPU_FLAG_FMA4         0x0800U /**< Bulldozer FMA4 functions  */
#define FV_CPU_FLAG_CMOV         0x1000U /**< i686 cmov                 */

#if HAVE_INLINE_ASM
/**
 * @internal Empty MMX state.
 *
 * Always put fv_emms() between a sequence of vectorized instructions
 * and floating point arithmetic or return. 
 */
#define fv_emms() __asm__ __volatile__ ("emms" ::: "memory")
/* __volatile__ and the memory clobber should make fv_emms()
 * a full reordering barrier. Therefore GCC should never
 * move floating point instructions before emms when
 * optimizing for speed. */

#elif HAVE_MMX_INTRINSICS /* HAVE_INLINE_ASM */

/* An intrinsic implemented in most compilers, but only available in GCC
 * when compiling with -mmmx. Therefore we cannot rely on its presence
 * alone. */
#define fv_emms() _mm_empty

#endif /* HAVE_INLINE_ASM */

#endif /* ARCH_X86 */


/**
 * Turns an underscored identifier to capitalized camelcase.
 * First letter and each letter following an underscore gets changed to
 * uppercase. Underscores are removed.
 *
 * @param str string to convert to camelcase.
 *
 * @return a newly-allocated string, which should be freed when no
 *         longer needed
 */
char *fv_str_underscore_to_camelcase(const char *str);

#ifdef __cplusplus
}
#endif

#endif /* FVUTIL_H */


/**
 * @file      fvvalue.c
 * @brief     Function definitions for FvValue type-agnostic data.
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include <ctype.h>
#include <string.h>

#include "fvvalue.h"
#include "fvutil.h"


size_t fv_type_sizes[] = {
    [FV_TYPE_NONE]    = 0,
    [FV_TYPE_CHAR]    = sizeof(char),
    [FV_TYPE_INT8]    = sizeof(int8_t),
    [FV_TYPE_UINT8]   = sizeof(uint8_t),
    [FV_TYPE_INT16]   = sizeof(int16_t),
    [FV_TYPE_UINT16]  = sizeof(uint16_t),
    [FV_TYPE_INT32]   = sizeof(int32_t),
    [FV_TYPE_UINT32]  = sizeof(uint32_t),
    [FV_TYPE_INT64]   = sizeof(int64_t),
    [FV_TYPE_UINT64]  = sizeof(uint64_t),
    [FV_TYPE_ENUM]    = sizeof(int32_t),
    [FV_TYPE_FLAGS]   = sizeof(uint32_t),
    [FV_TYPE_FLOAT]   = sizeof(float),
    [FV_TYPE_DOUBLE]  = sizeof(double),
    [FV_TYPE_STRING]  = sizeof(char *),
    [FV_TYPE_POINTER] = sizeof(void *),
    [FV_TYPE_LAST]    = 0
};

const char *fv_type_names[] = {
    [FV_TYPE_NONE]    = "(none)",
    [FV_TYPE_CHAR]    = "char",
    [FV_TYPE_INT8]    = "int8_t",
    [FV_TYPE_UINT8]   = "uint8_t",
    [FV_TYPE_INT16]   = "int16_t",
    [FV_TYPE_UINT16]  = "uint16_t",
    [FV_TYPE_INT32]   = "int32_t",
    [FV_TYPE_UINT32]  = "uint32_t",
    [FV_TYPE_INT64]   = "int64_t",
    [FV_TYPE_UINT64]  = "uint64_t",
    [FV_TYPE_ENUM]    = "int32_t",
    [FV_TYPE_FLAGS]   = "uint32_t",
    [FV_TYPE_FLOAT]   = "float",
    [FV_TYPE_DOUBLE]  = "double",
    [FV_TYPE_STRING]  = "char *",
    [FV_TYPE_POINTER] = "void *",
    [FV_TYPE_LAST]    = "(none)",
};


size_t fv_value_to_string(FvValue *v, char *str, size_t size) {
    int len; 
    if (size < 2) return 0;
    switch (v->type) {
        case FV_TYPE_CHAR:
            str[0] = v->value.c;
            len = 1;
            break;
        case FV_TYPE_INT8:
            len = snprintf(str, size, "%hhd", v->value.s8);
            break;
        case FV_TYPE_UINT8:
            len = snprintf(str, size, "%hhd", v->value.u8);
            break;
        case FV_TYPE_INT16:
            len = snprintf(str, size, "%hd",  v->value.s16);
            break;
        case FV_TYPE_UINT16:
            len = snprintf(str, size, "%hd",  v->value.u16);
            break;
        case FV_TYPE_INT32:
            len = snprintf(str, size, "%d",   v->value.s32);
            break;
        case FV_TYPE_UINT32:
            len = snprintf(str, size, "%d",   v->value.u32);
            break;
        case FV_TYPE_INT64:
            len = snprintf(str, size, "%lld", v->value.s64);
            break;
        case FV_TYPE_UINT64:
            len = snprintf(str, size, "%lld", v->value.u64);
            break;
        case FV_TYPE_ENUM:
            /* TODO: print human-readable names */
            len = snprintf(str, size, "%d",   v->value.enmr);
            break;
        case FV_TYPE_FLAGS:
            /* TODO: print human-readable names */
            len = snprintf(str, size, "%d",   v->value.flags);
            break;
        case FV_TYPE_FLOAT:
            len = snprintf(str, size, "%f",   (double) v->value.f);
            break;
        case FV_TYPE_DOUBLE:
            len = snprintf(str, size, "%f",   v->value.d);
            break;
        case FV_TYPE_STRING:
            len = snprintf(str, size, "%s",   v->value.cstr);
            break;
        case FV_TYPE_POINTER:
            len = snprintf(str, size, "%p",   v->value.p);
            break;
        default:
            str[0] = '\0';
            len = 0;
            break;
    }
    if (len < 0) return 0;
    else return (size_t) len;
}


#define FV_PARSE_INT(typename, stype, tmptype, strtox) \
int fv_parse_##typename(const char *ptr, char **endptr, \
                          FvValue *out, FvValueConstraint *cr) { \
    FvValue out0; \
    tmptype val; \
    if (out == NULL) out = &out0; \
    out->type = FV_TYPE_##stype; \
    val = strtox(ptr, endptr, 0); \
    if ((endptr != NULL) && (ptr == *endptr)) { \
        out->value.v_int64 = 0; \
        return 1; \
    } \
    out->value.stype = (typename) val; \
    if (val > (tmptype) stype##_MAX) { \
        fv_log_warning("Input value is greater than maximum %s value " \
                       "(%lld > %lld). Saturating to %lld.", \
                       fv_type_name(out->type), val, stype##_MAX, \
                       stype##_MAX); \
        out->value.stype = stype##_MAX; \
    } \
    else if (val < (tmptype) stype##_MIN) { \
        fv_log_warning("Input value is less than minimum %s value" \
                       "(%lld < %lld). Saturating to %lld.", \
                       fv_type_name(out->type), val, stype##_MIN, \
                       stype##_MIN); \
        out->value.stype = stype##_MIN; \
    } \
    if (cr != NULL) { \
        if (cr->type != FV_TYPE_##stype) { \
            fv_log_warning("Incompatible FvValueConstraint type: " \
                           "expected %s, got %s", \
                           fv_type_name(FV_TYPE_##stype), \
                           fv_type_name(cr->type)); \
        } else { \
            const typename *list = (const typename *) cr->list; \
            if (val > (tmptype) cr->max.stype) { \
                fv_log_warning("Input value is greater than constraint " \
                               "max (%lld > %lld). Saturating to %lld.", \
                                val, cr->max.stype, cr->max.stype); \
                out->value.stype = cr->max.stype; \
            } else if (val < (tmptype) cr->min.stype) { \
                fv_log_warning("Input value is less than constraint " \
                               "min (%lld < %lld). Saturating to %lld.", \
                                val, cr->min.stype, cr->min.stype); \
                out->value.stype = cr->min.stype; \
            } \
            if (list != NULL) { \
                /* Iterate through an array of allowed values until */ \
                /* the array is terminated by a repeated entry. */ \
                int found = 0; \
                typename prev = (typename) (list[0] + 1); \
                while (*list != prev) { \
                    if (val == (tmptype) *list) found = 1; \
                    prev = *list; \
                    list++; \
                } \
                if (!found) { \
                    fv_log_error("Invalid input value %lld; not in the " \
                                 "list of allowed values. Default value " \
                                 "(%lld) will be used instead.", \
                                 val, cr->defval.stype); \
                    out->value.stype = cr->defval.stype; \
                    return 1; \
                } \
            } \
        } \
    } \
    return 0; \
}

#define FV_PARSE_UINT(typename, stype, tmptype, strtox) \
int fv_parse_##typename(const char *ptr, char **endptr, \
                          FvValue *out, FvValueConstraint *cr) { \
    FvValue out0; \
    tmptype val; \
    if (out == NULL) out = &out0; \
    out->type = FV_TYPE_##stype; \
    val = strtox(ptr, endptr, 0); \
    if ((endptr != NULL) && (ptr == *endptr)) { \
        out->value.v_int64 = 0; \
        return 1; \
    } \
    out->value.stype = (typename) val; \
    if (val > (tmptype) stype##_MAX) { \
        fv_log_warning("Input value is greater than maximum %s value " \
                       "(%lld > %lld). Saturating to %lld.", \
                       fv_type_name(out->type), val, stype##_MAX, \
                       stype##_MAX); \
        out->value.stype = stype##_MAX; \
    } \
    if (cr != NULL) { \
        if (cr->type != FV_TYPE_##stype) { \
            fv_log_warning("Incompatible FvValueConstraint type: " \
                           "expected %s, got %s", \
                           fv_type_name(FV_TYPE_##stype), \
                           fv_type_name(cr->type)); \
        } else { \
            const typename *list = (const typename *) cr->list; \
            if (val > (tmptype) cr->max.stype) { \
                fv_log_warning("Input value is greater than constraint " \
                               "max (%lld > %lld). Saturating to %lld.", \
                                val, cr->max.stype, cr->max.stype); \
                out->value.stype = cr->max.stype; \
            } else if (val < (tmptype) cr->min.stype) { \
                fv_log_warning("Input value is less than constraint " \
                               "min (%lld < %lld). Saturating to %lld.", \
                                val, cr->min.stype, cr->min.stype); \
                out->value.stype = cr->min.stype; \
            } \
            if (list != NULL) { \
                /* Iterate through an array of allowed values until */ \
                /* the array is terminated by a repeated entry. */ \
                int found = 0; \
                typename prev = (typename) (list[0] + 1); \
                while (*list != prev) { \
                    if (val == (tmptype) *list) found = 1; \
                    prev = *list; \
                    list++; \
                } \
                if (!found) { \
                    fv_log_error("Invalid input value %lld; not in the " \
                                 "list of allowed values. Default value " \
                                 "(%lld) will be used instead.", \
                                 val, cr->defval.stype); \
                    out->value.stype = cr->defval.stype; \
                    return 1; \
                } \
            } \
        } \
    } \
    return 0; \
}

#define FV_PARSE_FLOAT(typename, stype, strtox) \
int fv_parse_##typename(const char *ptr, char **endptr, \
                          FvValue *out, FvValueConstraint *cr) { \
    FvValue out0; \
    typename val; \
    if (out == NULL) out = &out0; \
    out->type = FV_TYPE_##stype; \
    val = strtox(ptr, endptr); \
    if ((endptr != NULL) && (ptr == *endptr)) { \
        out->value.v_int64 = 0; \
        return 1; \
    } \
    out->value.stype = val; \
    if (cr != NULL) { \
        if (cr->type != FV_TYPE_##stype) { \
            fv_log_warning("Incompatible FvValueConstraint type: " \
                           "expected %s, got %s", \
                           fv_type_name(FV_TYPE_##stype), \
                           fv_type_name(cr->type)); \
        } else { \
            if (val > cr->max.stype) { \
                fv_log_warning("Input value is greater than constraint " \
                               "max (%lld > %lld). Saturating to %lld.", \
                                val, cr->max.stype, cr->max.stype); \
                out->value.stype = cr->max.stype; \
            } else if (val < cr->min.stype) { \
                fv_log_warning("Input value is less than constraint " \
                               "min (%lld < %lld). Saturating to %lld.", \
                                val, cr->min.stype, cr->min.stype); \
                out->value.stype = cr->min.stype; \
            } \
        } \
    } \
    return 0; \
}

/**
 * @name FvValue parsers.
 */
/*@{*/
/**
 * Parses an FvValue from a text string.
 *
 * @param  ptr
 *         string to parse. The value must be at the beginning or following
 *         an arbitrary amount of white space. Text may follow the value.
 * @param  endptr
 *         if not NULL, pointer to the first character following the parsed
 *         value is written to the location referenced by endptr.
 * @param  out
 *         if not NULL, pointer to an FvValue where the parsed number will
 *         be written to.
 * @param  cr
 *         if not NULL, pointer to an FvValueConstraint instance that
 *         matches the type of this function.
 * @return 0 if successful, 1 if parsing fails.
 */
FV_PARSE_INT(char,     c,   long int,                strtol)
FV_PARSE_INT(int8_t,   s8,  long int,                strtol)
FV_PARSE_INT(int16_t,  s16, long int,                strtol)
FV_PARSE_INT(int32_t,  s32, long int,                strtol)
FV_PARSE_INT(int64_t,  s64, long long int,           strtoll)

FV_PARSE_UINT(uint8_t,  u8,  unsigned long int,      strtoul)
FV_PARSE_UINT(uint16_t, u16, unsigned long int,      strtoul)
FV_PARSE_UINT(uint32_t, u32, unsigned long int,      strtoul)
FV_PARSE_UINT(uint64_t, u64, unsigned long long int, strtoull)

FV_PARSE_FLOAT(float,  f,   strtof)
FV_PARSE_FLOAT(double, d,   strtod)
/*@}*/


char *fv_list_flags(const FvFlag *flags, uint32_t bitmask,
                    const char *delim) {
    char *str, *p;
    size_t capacity = 1;
    size_t delim_len = strlen(delim);
    int i = 0;

    /* Calculate string size */
    for (i = 0; flags[i].name; i++) {
        if ((flags[i].value & bitmask) == flags[i].value) {
            capacity += strlen(flags[i].name) + delim_len;
        }
    }
    str = (char *) calloc((uint32_t) capacity, sizeof(char));
    if (str == NULL) return NULL;

    p = str;
    for (i = 0; flags[i].name; i++) {
        if ((flags[i].value & bitmask) == flags[i].value) { 
            size_t len = strlen(flags[i].name);
            if (len > capacity) {
                fv_log_error("String capacity exceeded while listing flag \"%s\"",
                             flags[i].name);
            } else {
                memcpy(p, flags[i].name, len);
                p += len;
                memcpy(p, delim, delim_len);
                p += delim_len;
            }
            capacity = capacity - len - delim_len;
        }
    }
    if (p != str) *(p-delim_len) = '\0';
    return str; 
}


/**
 * @internal Compares a token word to a string case-insensitively.
 *
 * @param token        a token string, not necessarily terminated with
 *                     a null byte.
 * @param str          an ordinary string terminated with a null byte 
 * @param token_length length of the token string, excluding the optional
 *                     null byte.
 *
 * @returns -1, 0 or 1 if token is found, respectively, to be less than,
 *          equal or greater than str in lexicographic order. ASCII
 *          character case is ignored in the comparison. If token matches
 *          the string but the string is longer, -1 is returned.
 */
static int tokencasecmp(const char *token, const char *str,
                        size_t token_length) {
    size_t i;
    for (i = 0; i < token_length; i++) {
        if (str[i] == '\0') return -1;
        else if (tolower(token[i]) < tolower(str[i])) return -1;
        else if (tolower(token[i]) > tolower(str[i])) return 1;
    }
    if (str[i] != '\0') return -1;
    else return 0;
}


int fv_parse_flags(const char *str, const FvFlag *flags, uint32_t *out,
                      const char **errpos) {
    const char *p = str;
    const char *tstart = NULL;
    size_t      tlen = 0;
    int         err = 0;
    uint32_t    ret = *out;
    int         add = 1, addnext = 1, run = 1, delim;

    if (str == NULL) return 0;

    while (run) {
        switch (*p) {
            case '\0':
                delim = 1;
                run = 0;
                break;
            case '+':
            case ',':
            case ' ':
            case '\t':
            case '\n':
                delim = 1;
                addnext = 1;
                break;
            case '-':
                delim = 1;
                addnext = 0;
                break;
            default:
                delim = 0;
                if (tlen == 0) tstart = p;
                tlen++;
        }
        if (delim && (tlen > 0)) {
            int token_found = 0;
            if (!tokencasecmp(tstart, "all", tlen)) {
                if (add) {
                    int i;
                    for (i = 0; flags[i].name; i++) {
                        ret  |= flags[i].value;
                    }
                } else {
                    ret = 0;
                }
                token_found = 1;
            } else if (!tokencasecmp(tstart, "none", tlen)) {
                if (add) ret = 0;
                token_found = 1;
            } else if (!tokencasecmp(tstart, "help", tlen)) {
                err = -1;
                token_found = 1;
            } else if (!tokencasecmp(tstart, "list", tlen)) {
                err = -1;
                token_found = 1;
            } else {
                int i;
                for (i = 0; flags[i].name; i++) {
                    if (!tokencasecmp(tstart, flags[i].name, tlen)) {
                        if (add) ret |=  flags[i].value;
                        else     ret &= ~flags[i].value;
                        token_found = 1;
                    }
                }
            }
            if (!token_found && !err) {
                err = (int) tlen;
                if (errpos != NULL) *errpos = tstart;
            }

            tlen = 0;
        }
        add = addnext;
        p++;
    }
    *out = ret;
    return err;
}


void fv_vector_get_value(FvValue *out, FvVectorData *vd,
                         size_t n, size_t element) {
    n = n * vd->format.num_elements + element;
    out->type = vd->format.element_type;
    switch (vd->format.element_bytes) {
        case 1:
            out->value.u8 = vd->data.u8[n];
            break;
        case 2:
            out->value.u16 = vd->data.u16[n];
            break;
        case 4:
            out->value.u32 = vd->data.u32[n];
            break;
        case 8:
            out->value.u64 = vd->data.u64[n];
            break;
        default:
            break;
    }
}


void fv_vector_broadcast_value(union FvValuePtr vector, FvValue *val,
                                      size_t num_elements) {
    switch (val->type) {
        case FV_TYPE_CHAR:
        case FV_TYPE_INT8:
        case FV_TYPE_UINT8:
            memset(vector.u8, val->value.u8, num_elements);
            break;
        case FV_TYPE_INT16:
        case FV_TYPE_UINT16: {
            size_t i;
            uint32_t tmp32;
            if (num_elements & 1)
                vector.u16[num_elements - 1] = val->value.u16;
            tmp32 = (((uint32_t) val->value.u16) << 16) +
                     ((uint32_t) val->value.u16);
            num_elements >>= 1;
            for (i = 0; i < num_elements; i++)
                vector.u32[i] = tmp32;
            break;
        }
        case FV_TYPE_INT32:
        case FV_TYPE_UINT32:
        case FV_TYPE_ENUM:
        case FV_TYPE_FLAGS:
        case FV_TYPE_FLOAT: {
            size_t i;
            uint32_t tmp32 = val->value.u32;
            for (i = 0; i < num_elements; i++)
                vector.u32[i] = tmp32;
            break;
        }
        case FV_TYPE_INT64:
        case FV_TYPE_UINT64:
        case FV_TYPE_DOUBLE: {
            size_t i;
            uint64_t tmp64 = val->value.u64;
            for (i = 0; i < num_elements; i++)
               vector.u64[i] = tmp64; 
            break;
        }
        case FV_TYPE_STRING:
        case FV_TYPE_POINTER: {
            size_t i;
            char *tmpp = val->value.str;
            for (i = 0; i < num_elements; i++)
               vector.str[i] = tmpp; 
            break;
        }
        default:
            break;
    }
}

int fvvectordata_duplicate_elements(FvVectorData *vd,
                                    size_t factor) {
    size_t i;
    size_t size = vd->format.num_vectors * vd->format.num_elements;
    union FvValuePtr olddata;
    union FvValuePtr newdata;
    newdata.p = fv_malloc(factor * size);
    if (newdata.p == NULL) {
        return 1;
    }
    olddata.p = vd->data.p;
    switch (vd->format.element_type) {
        case FV_TYPE_CHAR:
        case FV_TYPE_INT8:
        case FV_TYPE_UINT8:
            if (factor % 4) {
                for (i = 0; i < size; i++) {
                    size_t j;
                    size_t pos = i * factor;
                    for (j = 0; j < factor; j++) {
                        newdata.u8[pos + j] = olddata.u8[i]; 
                    }
                }
            } else {
                /* Values are duplicated by a factor divisible by 4.
                 * Parallelize the operation by using 32-bit integers. */
                for (i = 0; i < size; i++) {
                    size_t j;
                    size_t factor32 = factor >> 2;
                    size_t      pos = i * factor32;
                    uint32_t  value = (uint32_t) olddata.u8[i] +
                                     ((uint32_t) olddata.u8[i] << 8);
                    value = value + (value << 16);
                    for (j = 0; j < factor32; j++) {
                        newdata.u32[pos + j] = value; 
                    }
                }
            }
            break;
        case FV_TYPE_INT16:
        case FV_TYPE_UINT16:
            if (factor % 2) {
                for (i = 0; i < size; i++) {
                    size_t j;
                    size_t pos = i * factor;
                    for (j = 0; j < factor; j++) {
                        newdata.u16[pos + j] = olddata.u16[i]; 
                    }
                }
            } else {
                for (i = 0; i < size; i++) {
                    size_t j;
                    size_t factor32 = factor >> 1;
                    size_t      pos = i * factor32;
                    uint32_t  value = (uint32_t) olddata.u16[i] +
                                     ((uint32_t) olddata.u16[i] << 16);
                    for (j = 0; j < factor32; j++) {
                        newdata.u32[pos + j] = value; 
                    }
                }
            }
            break;
        case FV_TYPE_INT32:
        case FV_TYPE_UINT32:
        case FV_TYPE_ENUM:
        case FV_TYPE_FLAGS:
        case FV_TYPE_FLOAT: {
            for (i = 0; i < size; i++) {
                size_t j;
                size_t pos = i * factor;
                for (j = 0; j < factor; j++) {
                    newdata.u32[pos + j] = olddata.u32[i]; 
                }
            }
            break;
        }
        case FV_TYPE_INT64:
        case FV_TYPE_UINT64:
        case FV_TYPE_DOUBLE: {
            for (i = 0; i < size; i++) {
                size_t j;
                size_t pos = i * factor;
                for (j = 0; j < factor; j++) {
                    newdata.u64[pos + j] = olddata.u64[i]; 
                }
            }
            break;
        }
        case FV_TYPE_STRING:
        case FV_TYPE_POINTER: {
            for (i = 0; i < size; i++) {
                size_t j;
                size_t pos = i * factor;
                for (j = 0; j < factor; j++) {
                    newdata.str[pos + j] = olddata.str[i]; 
                }
            }
            break;
        }
        default:
            break;
    }
    vd->data = newdata;
    vd->format.num_elements = vd->format.num_elements * factor;
    vd->format.vector_bytes = vd->format.num_elements *
                              vd->format.element_bytes;
    vd->format.total_bytes  = vd->format.num_vectors *
                              vd->format.vector_bytes;
    fv_free(olddata.p);
    return 0;
}



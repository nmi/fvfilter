/**
 * @file      fvvalue.h
 * @brief     Structs and function declarations for FvValue type-agnostic
 *            data storage.
 * @author    Niko Mikkilä <nm@phnet.fi>
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#ifndef FVVALUE_H
#define FVVALUE_H

// #include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif


/**
 * Basic types used in FvValue.
 */
enum FvType {
    FV_TYPE_NONE,
    FV_TYPE_CHAR,
    FV_TYPE_INT8,
    FV_TYPE_UINT8,
    FV_TYPE_INT16,
    FV_TYPE_UINT16,
    FV_TYPE_INT32,
    FV_TYPE_UINT32,
    FV_TYPE_INT64,
    FV_TYPE_UINT64,
    FV_TYPE_ENUM,
    FV_TYPE_FLAGS,
    FV_TYPE_FLOAT,
    FV_TYPE_DOUBLE,
    FV_TYPE_STRING,
    FV_TYPE_POINTER,
    FV_TYPE_LAST
};


#define FV_TYPE_c     FV_TYPE_CHAR
#define FV_TYPE_s8    FV_TYPE_INT8
#define FV_TYPE_u8    FV_TYPE_UINT8
#define FV_TYPE_s16   FV_TYPE_INT16
#define FV_TYPE_u16   FV_TYPE_UINT16
#define FV_TYPE_s32   FV_TYPE_INT32
#define FV_TYPE_u32   FV_TYPE_UINT32
#define FV_TYPE_s64   FV_TYPE_INT64
#define FV_TYPE_u64   FV_TYPE_UINT64
#define FV_TYPE_enmr  FV_TYPE_ENUM
#define FV_TYPE_flags FV_TYPE_FLAGS
#define FV_TYPE_f     FV_TYPE_FLOAT
#define FV_TYPE_d     FV_TYPE_DOUBLE
#define FV_TYPE_str   FV_TYPE_STRING
#define FV_TYPE_cstr  FV_TYPE_STRING
#define FV_TYPE_p     FV_TYPE_POINTER

#define C_TYPE_c      char
#define C_TYPE_s8     int8_t
#define C_TYPE_u8     uint8_t
#define C_TYPE_s16    int16_t
#define C_TYPE_u16    uint16_t
#define C_TYPE_s32    int32_t
#define C_TYPE_u32    uint32_t
#define C_TYPE_s64    int64_t
#define C_TYPE_u64    uint64_t
#define C_TYPE_enmr   int32_t
#define C_TYPE_flags  uint32_t 
#define C_TYPE_f      float
#define C_TYPE_d      double
#define C_TYPE_str    char *
#define C_TYPE_cstr   const char *
#define C_TYPE_p      void *

#define c_MIN   INT8_MIN
#define s8_MIN  INT8_MIN
#define u8_MIN  0
#define s16_MIN INT16_MIN
#define u16_MIN 0
#define s32_MIN INT32_MIN
#define u32_MIN 0
#define s64_MIN INT64_MIN
#define u64_MIN 0

#define c_MAX   INT8_MAX
#define s8_MAX  INT8_MAX
#define u8_MAX  UINT8_MAX
#define s16_MAX INT16_MAX
#define u16_MAX UINT16_MAX
#define s32_MAX INT32_MAX
#define u32_MAX UINT32_MAX
#define s64_MAX INT64_MAX
#define u64_MAX UINT64_MAX

/* Turn off warnings about struct padding. */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpadded"

/**
 * Container for dynamically typed variables.
 */
typedef struct FvValue {
    /** Value */
    union {
        char        v_char;
        int8_t      v_int8;
        uint8_t     v_uint8;
        int16_t     v_int16;
        uint16_t    v_uint16;
        int32_t     v_int32;
        uint32_t    v_uint32;
        int64_t     v_int64;
        uint64_t    v_uint64;
        int32_t     v_enum;
        uint32_t    v_flags;
        float       v_float;
        double      v_double;
        const char *v_string;
        void       *v_pointer;

        char        c;
        int8_t      s8;
        uint8_t     u8;
        int16_t     s16;
        uint16_t    u16;
        int32_t     s32;
        uint32_t    u32;
        int64_t     s64;
        uint64_t    u64;
        int32_t     enmr;
        uint32_t    flags;
        float       f;
        double      d;
        char       *str;
        const char *cstr;
        void       *p;
    } value;
    /** Type id */
    enum FvType  type;
} FvValue;  

#pragma GCC diagnostic pop

/**
 * Multi-typed data for FvValue and related functions.
 */
union FvValueData {
    char        c;
    int8_t      s8;
    uint8_t     u8;
    int16_t     s16;
    uint16_t    u16;
    int32_t     s32;
    uint32_t    u32;
    int64_t     s64;
    uint64_t    u64;
    float       f;
    double      d;
    void       *p;
    char       *str;
    const char *cstr;
};


/**
 * Multi-typed pointer to be used with type-agnostic arrays.
 */
union FvValuePtr {
    char        *c;
    int8_t      *s8;
    uint8_t     *u8;
    int16_t     *s16;
    uint16_t    *u16;
    int32_t     *s32;
    uint32_t    *u32;
    int64_t     *s64;
    uint64_t    *u64;
    float       *f;
    double      *d;
    void        *p;
    void       **pp;
    char       **str;
    const char **cstr;
};


/* Turn off warnings about struct padding. */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpadded"

/**
 * Struct for describing value constraints.
 */
typedef struct FvValueConstraint {
    union FvValueData  defval;
    union FvValueData  min; 
    union FvValueData  max;
    const void        *list;
    enum FvType        type;
} FvValueConstraint;

#pragma GCC diagnostic pop


/**
 * Struct for describing SIMD-vectorized data.
 */
typedef struct FvVectorParams {
    size_t      total_bytes;
    size_t      num_vectors;
    uint32_t    vector_bytes;
    uint32_t    num_elements;
    uint32_t    element_bytes;
    enum FvType element_type; 
} FvVectorParams;

/**
 * Struct for data buffers that can be accessed as byte, short or float
 * arrays. Useful for managing buffers that are accessed by various SIMD
 * functions.
 */
typedef struct FvVectorData {
    union FvValuePtr data;
    FvVectorParams   format;
} FvVectorData;


/* Turn off warnings about struct padding on 64-bit systems.
 * FvEnum and FvFlag must match Glib2's structs, so field order
 * cannot be changed. */
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpadded"

/**
 * Struct for describing enumerations. Matches GLib2's GEnumValue.
 */
typedef struct FvEnum {
    int32_t     value;
    const char *longname;
    const char *name;
} FvEnum;

/**
 * Struct for describing option flags. Matches GLib2's GFlagsValue.
 */
typedef struct FvFlag {
    uint32_t    value;
    const char *longname;
    const char *name;
} FvFlag;

#pragma GCC diagnostic pop


/**
 * Sizes of built-in types covered by FvType. Defined in fvvalue.c
 */
extern size_t fv_type_sizes[];

/**
 * Names of built-in types covered by FvType. Defined in fvvalue.c
 */
extern const char *fv_type_names[];


/**
 * Returns the size of a type described by a given FvType.
 *
 * @param type an FvType enumeration
 *
 * @return size of the type
 */ 
#define fv_type_sizeof(type) fv_type_sizes[type]

/**
 * Returns the name of an FvType.
 *
 * @param type an FvType enumeration
 *
 * @return name of the type
 */ 
#define fv_type_name(type) fv_type_names[type]


/**
 * @name FvValue getter macros
 */
/*@{*/
/**
 * Gets the value of an FvValue.
 *
 * @param      fvv FvValue to read.
 * @return value
 */ 
#define fv_value_get_char(fvv)    (fvv)->value.v_char
#define fv_value_get_uint8(fvv)   (fvv)->value.v_uint8
#define fv_value_get_int32(fvv)   (fvv)->value.v_int32
#define fv_value_get_uint32(fvv)  (fvv)->value.v_uint32
#define fv_value_get_int64(fvv)   (fvv)->value.v_int64
#define fv_value_get_uint64(fvv)  (fvv)->value.v_uint64
#define fv_value_get_enum(fvv)    (fvv)->value.v_enum
#define fv_value_get_flags(fvv)   (fvv)->value.v_flags
#define fv_value_get_float(fvv)   (fvv)->value.v_float
#define fv_value_get_double(fvv)  (fvv)->value.v_double
#define fv_value_get_string(fvv)  (fvv)->value.v_string
#define fv_value_get_pointer(fvv) (fvv)->value.v_pointer
#define fv_value_get_array(fvv)   (fvv)->value.v_array
/*@}*/


/**
 * Macro to generate type-checking getter functions for FvValues.
 *
 * @param uctype upper-case type name as in FV_TYPE_x definitions
 * @param lctype lower-case type names as in FvValue struct union names
 * @param ctype  C type name 
 */
#define DEFINE_FV_VALUE_GET_CHECKED(uctype, lctype, ctype)                \
    static inline int fv_value_get_checked_##lctype(const FvValue *fvv,   \
                                            ctype *out) {                 \
        if (fvv->type == FV_TYPE_##uctype) {                              \
            *out = fvv->value.v_##lctype;                                 \
            return 1;                                                     \
        } else {                                                          \
            return 0;                                                     \
        }                                                                 \
    }

/**
 * @name FvValue type-checking getter functions
 */
/*@{*/
/**
 * Gets the value of an FvValue.
 *
 * @param      fvv FvValue to read from.
 * @param[out] out pointer to a location where the retrieved value
 *                 is written to.
 * @return 1 on success, 0 if the FvValue type does not match
 *         the function
 */ 
DEFINE_FV_VALUE_GET_CHECKED(   CHAR,    char,         char)
DEFINE_FV_VALUE_GET_CHECKED(  UINT8,   uint8,      uint8_t)
DEFINE_FV_VALUE_GET_CHECKED(  INT32,   int32,      int32_t)
DEFINE_FV_VALUE_GET_CHECKED( UINT32,  uint32,     uint32_t)
DEFINE_FV_VALUE_GET_CHECKED(  INT64,   int64,      int64_t)
DEFINE_FV_VALUE_GET_CHECKED( UINT64,  uint64,     uint64_t)
DEFINE_FV_VALUE_GET_CHECKED(   ENUM,    enum,      int32_t)
DEFINE_FV_VALUE_GET_CHECKED(  FLAGS,   flags,     uint32_t)
DEFINE_FV_VALUE_GET_CHECKED(  FLOAT,   float,        float)
DEFINE_FV_VALUE_GET_CHECKED( DOUBLE,  double,       double)
DEFINE_FV_VALUE_GET_CHECKED( STRING,  string, const char *)
DEFINE_FV_VALUE_GET_CHECKED(POINTER, pointer,       void *)

/*@}*/


/**
 * Macro to generate setter functions for FvValues.
 *
 * @param uctype upper-case type name as in FV_TYPE_x definitions
 * @param lctype lower-case type names as in FvValue struct union names
 * @param ctype  C type name 
 */
#define DEFINE_FV_VALUE_SET(uctype, lctype, ctype)                        \
    static inline void fv_value_set_##lctype(FvValue *fvv, ctype in) {    \
        fvv->type = FV_TYPE_##uctype;                                     \
        fvv->value.v_##lctype = in;                                       \
    }

/**
 * @name FvValue setter functions.
 */
/*@{*/
/**
 * Assigns a value to an FvValue.
 *
 * @param fvv FvValue to assign.
 * @param in  value.
 */ 
DEFINE_FV_VALUE_SET(   CHAR,    char,         char)
DEFINE_FV_VALUE_SET(  UINT8,   uint8,      uint8_t)
DEFINE_FV_VALUE_SET(  INT32,   int32,      int32_t)
DEFINE_FV_VALUE_SET( UINT32,  uint32,     uint32_t)
DEFINE_FV_VALUE_SET(  INT64,   int64,      int64_t)
DEFINE_FV_VALUE_SET( UINT64,  uint64,     uint64_t)
DEFINE_FV_VALUE_SET(  FLOAT,   float,        float)
DEFINE_FV_VALUE_SET( DOUBLE,  double,       double)
DEFINE_FV_VALUE_SET( STRING,  string, const char *)
DEFINE_FV_VALUE_SET(POINTER, pointer,       void *)

static inline void fv_value_set_enum(FvValue *fvv, int32_t in) {
    fvv->type = FV_TYPE_ENUM;
    fvv->value.v_int32 = in;
}

static inline void fv_value_set_flags(FvValue *fvv, uint32_t in) {
    fvv->type = FV_TYPE_FLAGS;
    fvv->value.v_uint32 = in;
}

/*@}*/

/**
 * Converts an FvValue to a string.
 *
 * @param v    FvValue to print
 * @param str  pointer to an allocated memory area of at least size bytes
 * @param size maximum number of characters to print, including the
 *             terminating null byte
 *
 * @return number of characters written, excluding the terminating null byte
 */
size_t fv_value_to_string(FvValue *v, char *str, size_t size);


#define FV_PARSE_PROTOTYPE(typename) \
int fv_parse_##typename(const char *ptr, char **endptr, \
        FvValue *out, FvValueConstraint *cr)

FV_PARSE_PROTOTYPE(char);
FV_PARSE_PROTOTYPE(int8_t);
FV_PARSE_PROTOTYPE(uint8_t);
FV_PARSE_PROTOTYPE(int16_t);
FV_PARSE_PROTOTYPE(uint16_t);
FV_PARSE_PROTOTYPE(int32_t);
FV_PARSE_PROTOTYPE(uint32_t);
FV_PARSE_PROTOTYPE(int64_t);
FV_PARSE_PROTOTYPE(uint64_t);
FV_PARSE_PROTOTYPE(float);
FV_PARSE_PROTOTYPE(double);



/**
 * Returns a single value from an FvVectorData buffer.
 *
 * @param out     pointer to an FvValue where the value will be stored
 * @param vd      an FvVectorData instance
 * @param n       vector index
 * @param element value index within the vector
 */
void fv_vector_get_value(FvValue *out, FvVectorData *vd,
                         size_t n, size_t element);

/**
 * Stores a value to a given number of consecutive vector elements.
 *
 * @param vector       pointer to a vector or data buffer
 * @param val          value to store
 * @param num_elements number of elements to fill with the value
 */
void fv_vector_broadcast_value(union FvValuePtr vector, FvValue *val,
                                      size_t num_elements);
 
/**
 * Duplicates each element of an FvVectorData buffer given number of
 * times. A new data buffer is allocated for the duplicated vectors and
 * switched in place of the old data.
 *
 * @param vd     an FvVectorData instance
 * @param factor number of times to duplicate each element
 *
 * @return 0 on success, 1 if memory allocation fails.
 */
int fvvectordata_duplicate_elements(FvVectorData *vd,
                                    size_t factor);


/**
 * Macro to help with type-agnostic processing of vectorized data.
 */
#define VECTORDATA_PROCESS(vectordata, operation) do { \
    size_t num_vectors  = (vectordata)->format->num_vectors; \
    size_t num_elements = (vectordata)->format->num_elements; \
    size_t vdsize       = (vectordata)->format->num_vectors * \
                          (vectordata)->format->num_elements; \
    size_t position = 0; \
    switch ((vectordata)->format.element_type) { \
        case FV_TYPE_CHAR: \
        case FV_TYPE_INT8: \
            for (position = 0; position < vdsize; position += num_elements) { \
                char *value = (vectordata)->data.s8 + position; \
                operation \
            } \
            break; \
        case FV_TYPE_UINT8: \
            for (position = 0; position < vdsize; position += num_elements) { \
                uint8_t *value = (vectordata)->data.u8 + position; \
                operation \
            } \
            break; \
        case FV_TYPE_INT16: \
            for (position = 0; position < vdsize; position += num_elements) { \
                int16_t *value = (vectordata)->data.s16 + position; \
                operation \
            } \
            break; \
        case FV_TYPE_UINT16: \
            for (position = 0; position < vdsize; position += num_elements) { \
                uint16_t *value = (vectordata)->data.u16 + position; \
                operation \
            } \
            break; \
        case FV_TYPE_INT32: \
            for (position = 0; position < vdsize; position += num_elements) { \
                int32_t *value = (vectordata)->data.s32 + position; \
                operation \
            } \
            break; \
        case FV_TYPE_UINT32: \
            for (position = 0; position < vdsize; position += num_elements) { \
                uint32_t *value = (vectordata)->data.u32 + position; \
                operation \
            } \
            break; \
        case FV_TYPE_ENUM: \
            for (position = 0; position < vdsize; position += num_elements) { \
                int32_t *value = (vectordata)->data.s32 + position; \
                operation \
            } \
            break; \
        case FV_TYPE_FLAGS: \
            for (position = 0; position < vdsize; position += num_elements) { \
                uint32_t *value = (vectordata)->data.u32 + position; \
                operation \
            } \
            break; \
        case FV_TYPE_FLOAT: \
            for (position = 0; position < vdsize; position += num_elements) { \
                float *value = (vectordata)->data.f + position; \
                operation \
            } \
            break; \
        case FV_TYPE_INT64: \
            for (position = 0; position < vdsize; position += num_elements) { \
                int64_t *value = (vectordata)->data.s64 + position; \
                operation \
            } \
            break; \
        case FV_TYPE_UINT64: \
            for (position = 0; position < vdsize; position += num_elements) { \
                uint64_t *value = (vectordata)->data.u64 + position; \
                operation \
            } \
            break; \
        case FV_TYPE_DOUBLE: \
            for (position = 0; position < vdsize; position += num_elements) { \
                double *value = (vectordata)->data.d + position; \
                operation \
            } \
            break; \
        case FV_TYPE_STRING: \
            for (position = 0; position < vdsize; position += num_elements) { \
                char **value = (vectordata)->data.str + position; \
                operation \
            } \
            break; \
        case FV_TYPE_POINTER: \
            for (position = 0; position < vdsize; position += num_elements) { \
                void **value = (vectordata)->data.p + position; \
                operation \
            } \
            break; \
        default: \
            break; \
    } \
} while (0);


#ifdef __cplusplus
}
#endif

#endif /* FVUTIL_H */


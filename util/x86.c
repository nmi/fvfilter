/**
 * @file      x86.c
 * @brief     x86 CPU feature detection functions for internal use
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 */

#include "config.h"

#if ARCH_X86

/*#include <stdlib.h>*/
#include <string.h>

#if HAVE_OPENMP
#include <omp.h>
#endif

#include "fvutil.h"
#include "x86.h"


struct FvCPUVendorString {
    const char      *str;
    enum FvCPUVendor vendor;
};

struct FvCPUFamilyInfo {
    const char *name;
    const char *description;
};

static struct FvCPUVendorString cpu_vendor_strings[] = {
    {"AMDisbetter!", FV_CPU_VENDOR_AMD},
    {"AuthenticAMD", FV_CPU_VENDOR_AMD},
    {"CentaurHauls", FV_CPU_VENDOR_CENTAUR},
    {"CyrixInstead", FV_CPU_VENDOR_CYRIX},
    {"GenuineIntel", FV_CPU_VENDOR_INTEL},
    {"GenuineTMx86", FV_CPU_VENDOR_TRANSMETA},
    {"Genuine  RDC", FV_CPU_VENDOR_RDC},
    {"Geode by NSC", FV_CPU_VENDOR_NSC},
    {"NexGenDriven", FV_CPU_VENDOR_NEXGEN},
    {"RiseRiseRise", FV_CPU_VENDOR_RISE},
    {"SiS SiS SiS ", FV_CPU_VENDOR_SIS},
    {"TransmetaCPU", FV_CPU_VENDOR_TRANSMETA},
    {"UMC UMC UMC ", FV_CPU_VENDOR_UMC},
    {"Vortex86 SoC", FV_CPU_VENDOR_DMP},
    {NULL, FV_CPU_VENDOR_UNKNOWN}
};

static const char *cpu_vendors[] = {
    [FV_CPU_VENDOR_UNKNOWN]   = "(unknown)",
    [FV_CPU_VENDOR_AMD]       = "AMD",
    [FV_CPU_VENDOR_CENTAUR]   = "Centaur Technology",
    [FV_CPU_VENDOR_CYRIX]     = "Cyrix",
    [FV_CPU_VENDOR_DMP]       = "DMP",
    [FV_CPU_VENDOR_IDT]       = "IDT",
    [FV_CPU_VENDOR_INTEL]     = "Intel",
    [FV_CPU_VENDOR_NEXGEN]    = "NexGen",
    [FV_CPU_VENDOR_NSC]       = "National Semiconductor",
    [FV_CPU_VENDOR_RDC]       = "RDC",
    [FV_CPU_VENDOR_RISE]      = "Rise",
    [FV_CPU_VENDOR_SIS]       = "SiS",
    [FV_CPU_VENDOR_TRANSMETA] = "Transmeta",
    [FV_CPU_VENDOR_UMC]       = "UMC",
    [FV_CPU_VENDOR_VIA]       = "VIA"
};


static struct FvCPUFamilyInfo cpu_families[] = {
    [FV_CPU_FAMILY_UNKNOWN]
        = {"",
           "unknown"},
    [FV_CPU_FAMILY_AMD_486]
        = {"Am486/5x86",
           "486-compatible"},
    [FV_CPU_FAMILY_AMD_K5]
        = {"K5",
           "P5-compatible"},
    [FV_CPU_FAMILY_AMD_K6]
        = {"K6",
           "P5-compatible with MMX support"},
    [FV_CPU_FAMILY_AMD_K7]
        = {"K7",
           "P6-compatible with SSE support"},
    [FV_CPU_FAMILY_AMD_K8]
        = {"K8",
           "64-bit, SSE2"},
    [FV_CPU_FAMILY_AMD_K10]
        = {"K10 / 10h",
           "64-bit, SSE4a"},
    [FV_CPU_FAMILY_AMD_BOBCAT]
        = {"Bobcat",
           "64-bit, SSE4a, low-power"},
    [FV_CPU_FAMILY_AMD_BULLDOZER]
        = {"Bulldozer",
           "64-bit, SSE4.2, AVX"},

    [FV_CPU_FAMILY_AMD_GEODE]
        = {"Geode",
           "P5-compatible SoC with MMX support"},

    [FV_CPU_FAMILY_CYRIX_486]
        = {"5x86",
           "486-compatible"},
    [FV_CPU_FAMILY_CYRIX_586]
        = {"6x86 M1 / MediaGX",
           "P5-compatible"},
    [FV_CPU_FAMILY_CYRIX_686]
        = {"6x86 MII",
           "P6-compatible with MMX support"},

    [FV_CPU_FAMILY_IDT_WINCHIP]
        = {"WinChip",
           "P5-compatible with MMX support"},

    [FV_CPU_FAMILY_INTEL_486]
        = {"80486",
           "i486, Intel486"},
    [FV_CPU_FAMILY_INTEL_P5]
        = {"P5",
           "Pentium / Pentium MMX"},
    [FV_CPU_FAMILY_INTEL_P6]
        = {"P6",
           "Pentium Pro/II/III"},
    [FV_CPU_FAMILY_INTEL_PM]
        = {"Pentium M",
           "P6-based mobile CPUs"},
    [FV_CPU_FAMILY_INTEL_NETBURST]
        = {"Netburst",
           "Pentium 4 / Pentium D"},
    [FV_CPU_FAMILY_INTEL_CORE]
        = {"Core",
           "Conroe (65 nm), Penryn (45 nm)"},
    [FV_CPU_FAMILY_INTEL_NEHALEM]
        = {"Nehalem",
           "Nehalem (45 nm), Westmere (32 nm)"},
    [FV_CPU_FAMILY_INTEL_SANDY_BRIDGE]
        = {"Sandy Bridge",
           "Sandy Bridge (32 nm), Ivy Bridge (22 nm)"},
    [FV_CPU_FAMILY_INTEL_HASWELL]
        = {"Haswell",
           "Haswell (22 nm), Broadwell (14 nm)"},
    [FV_CPU_FAMILY_INTEL_SKYLAKE]
        = {"Skylake",
           "Skylake (14 nm), Skymont (10 nm)"},

    [FV_CPU_FAMILY_INTEL_ATOM]
        = {"Atom / Bonnell",
           "low-power mobile CPUs"},

    [FV_CPU_FAMILY_INTEL_MIC]
        = {"MIC",
           "Many Integrated Core; Larrabee / Knights's Corner / Xeon Phi"},

    [FV_CPU_FAMILY_INTEL_ITANIUM]
        = {"Itanium",
           "First generation IA-64 CPUs"},
    [FV_CPU_FAMILY_INTEL_ITANIUM_2]
        = {"Itanium 2",
           "Second generation IA-64 CPUs"},

    [FV_CPU_FAMILY_NEXGEN_586]
        = {"Nx586",
           "P5-compatible"},

    [FV_CPU_FAMILY_NSC_GEODE]
        = {"Geode",
           "P5-compatible SoC with MMX support"},

    [FV_CPU_FAMILY_RDC_IAD]
        = {"IAD 100",
           "P5-compatible SoC with MMX support"},

    [FV_CPU_FAMILY_RISE_MP6]
        = {"mP6",
           "P5-compatible with MMX support"},

    [FV_CPU_FAMILY_SIS_550]
        = {"55x / Vortex86",
           "P5-compatible SoC with MMX support"},

    [FV_CPU_FAMILY_DMP_VORTEX86]
        = {"Vortex86",
           "P5-compatible SoC with MMX support"},

    [FV_CPU_FAMILY_TRANSMETA_CRUSOE]
        = {"Crusoe",
           "low-power 128-bit VLIW CPUs with software x86 emulation"},
    [FV_CPU_FAMILY_TRANSMETA_EFFICEON]
        = {"Efficeon",
           "low-power 256-bit VLIW CPUs with software x86 emulation"},

    [FV_CPU_FAMILY_UMC_486]
        = {"U5xx",
           "486-compatible"},

    [FV_CPU_FAMILY_VIA_C3]
        = {"C3",
           "low-power P6-compatible CPUs"},
    [FV_CPU_FAMILY_VIA_C7]
        = {"C7 / Nano",
           "low-power P6-compatible CPUs"}

};


static const FvCPUModel cpu_models[] = {
    /* Intel 80486 */
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_486, 0x040000, 0x0400FF,
     "i486DX (P4)",
     1000, 20, 33},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_486, 0x040100, 0x0401FF,
     "i486DX (P4)",
     1000, 33, 50},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_486, 0x040200, 0x0402FF,
     "i486SX (P23)",
     800, 16, 33},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_486, 0x040300, 0x04030F,
     "i486DX2 (P24)",
     800, 40, 66},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_486, 0x040310, 0x0403FF,
     "i486DX2 Overdrive (P23T/P24T)",
     800, 40, 66},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_486, 0x040400, 0x0404FF,
     "i486SL",
     800, 20, 33},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_486, 0x040500, 0x0405FF,
     "i486SX2 (P23T)",
     800, 40, 66},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_486, 0x040700, 0x0407FF,
     "i486DX2-WB (P24D)",
     800, 50, 66},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_486, 0x040800, 0x04080F,
     "i486DX4 (P24C)",
     600, 75, 100},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_486, 0x040810, 0x0408FF,
     "i486DX4 Overdrive",
     600, 75, 100},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_486, 0x040900, 0x0409FF,
     "i486DX4-WB (P24CT)",
     600, 75, 100},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_486, 0x040000, 0x04FFFF,
     "i486 (unknown model)",
     1000, 20, 100},

    /* Intel Pentium */
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P5, 0x050000, 0x0500FF,
     "Pentium (P5 A-step)",
     800, 60, 66},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P5, 0x050100, 0x05010F,
     "Pentium (P5)",
     800, 60, 66},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P5, 0x050110, 0x0501FF,
     "Pentium OverDrive for P5 (P5T)",
     800, 60, 66},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P5, 0x050200, 0x05020F,
     "Pentium (P54C/P54CS/P54CQS/P54CT/P54LM)",
     600, 75, 200},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P5, 0x050210, 0x0502FF,
     "Pentium OverDrive for P54C",
     600, 75, 200},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P5, 0x050310, 0x0503FF,
     "Pentium Overdrive for i486 (P24T)",
     600, 63, 83},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P5, 0x050400, 0x05040F,
     "Pentium MMX (P55C)",
     350, 120, 233},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P5, 0x050410, 0x0504FF,
     "Pentium Overdrive MMX for P54C",
     600, 120, 166},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P5, 0x050510, 0x0505FF,
     "Pentium Overdrive for i486DX4",
     600, 120, 166},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P5, 0x050600, 0x0506FF,
     "Pentium Overdrive MMX (P55CTB)",
     350, 150, 200},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P5, 0x050700, 0x0507FF,
     "Pentium (P54C/P54LM)",
     350, 75, 300},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P5, 0x050800, 0x0508FF,
     "Pentium MMX (P55C/Tillamook)",
     250, 166, 266},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P5, 0x050000, 0x05FFFF,
     "Pentium (unknown model)",
     800, 60, 300},

    /* Intel Pentium Pro
     * Steppings from "Intel Pentium Pro Processor Specification Update",
     * released on January 1999 */
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060000, 0x0600FF,
     "Pentium Pro (A, 256 kB L2)",
     600, 120, 133},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060100, 0x060101,
     "Pentium Pro (B0, 256 kB L2)",
     500, 150, 150},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060102, 0x060102,
     "Pentium Pro (C0, 256 kB L2)",
     500, 150, 150},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060106, 0x060106,
     "Pentium Pro (sA0, 256 kB L2)",
     350, 180, 200},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060107, 0x060107,
     "Pentium Pro (sA1, 256/512 kB L2)",
     350, 166, 200},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060109, 0x060109,
     "Pentium Pro (sB1, 256/512/1024 kB L2)",
     350, 166, 200},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060100, 0x0601FF,
     "Pentium Pro (unknown model)",
     350, 166, 200},

    /* Intel Pentium II
     * Steppings from "Intel Pentium II Processor Specification Update",
     * released on July 2002 */
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060312, 0x060312,
     "Pentium II Overdrive for Pentium Pro (Deschutes TB0)",
     250, 300, 333},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060303, 0x060303,
     "Pentium II (Klamath C0)",
     350, 233, 300},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060304, 0x060304,
     "Pentium II (Klamath C1)",
     350, 233, 300},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060500, 0x060500,
     "Pentium II, Mobile Pentium II, Xeon, Celeron (Deschutes A0)",
     250, 266, 333},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060410, 0x06041F,
     "Pentium II Overdrive for Pentium Pro",
     250, 300, 333},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060501, 0x060501,
     "Pentium II / Mobile Pentium II / Xeon / Celeron (Deschutes A1)",
     250, 300, 400},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060502, 0x060502,
     "Pentium II / Mobile Pentium II / Xeon / Celeron (Deschutes B0)",
     250, 266, 450},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060503, 0x060503,
     "Pentium II / Xeon / Celeron (Deschutes B1)",
     250, 350, 400},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060500, 0x0605FF,
     "Pentium II / Mobile Pentium II / Xeon / Celeron (Deschutes, unknown model)",
     350, 233, 450},
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060600, 0x0606FF,
     "Mobile Pentium II (Dixon) / Celeron (Mendocino)",
     250, 266, 466},

    /* Intel Core 2 */
    {FV_CPU_VENDOR_INTEL, FV_CPU_FAMILY_INTEL_P6, 0x060F0B, 0x060F0B,
     "Core 2 (G0/G2)",
     65, 1200, 3000},

    /* AMD 80486 */
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_486, 0x040100, 0x0401FF,
     "Am486 DX",
     800, 25, 40},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_486, 0x040300, 0x0403FF,
     "Am486 DX2",
     500, 50, 80},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_486, 0x040700, 0x0407FF,
     "Enhanced Am486 DX2 WB",
     350, 66, 80},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_486, 0x040800, 0x0408FF,
     "Am486 DX4",
     500, 75, 120},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_486, 0x040900, 0x0409FF,
     "Enhanced Am486 DX4 WB",
     350, 75, 120},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_486, 0x040A00, 0x040AFF,
     "Elan SC400/SC410",
     350, 33, 100},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_486, 0x040E00, 0x040EFF,
     "Am5x86",
     350, 133, 150},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_486, 0x040F00, 0x040FFF,
     "Am5x86 WB",
     350, 133, 150},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_486, 0x040000, 0x04FFFF,
     "Am486/Am5x86 (unknown model)",
     500, 50, 150},

    /* AMD 5k86/K5 */
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_K5, 0x050000, 0x050000,
     "5k86, K5 PR75/90 (SSA5)",
     500, 75, 90}, 
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_K5, 0x050001, 0x0500FF,
     "5k86, K5 PR90/100 (SSA5)",
     500, 90, 100},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_K5, 0x050100, 0x0501FF,
     "K5 PR120/133/150 (5k86)",
     350, 90, 113},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_K5, 0x050200, 0x0502FF,
     "K5 PR166 (5k86)",
     350, 117, 117},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_K5, 0x050300, 0x0503FF,
     "K5 PR200 (5k86)",
     350, 133, 133},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_NSC_GEODE, 0x050500, 0x0505FF,
     "Geode GX",
     150, 266, 400},

    /* AMD K6 */
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_K6, 0x050600, 0x0506FF,
     "K6",
     350, 166, 233},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_K6, 0x050700, 0x0507FF,
     "K6 (\"Little Foot\")",
     250, 200, 300},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_K6, 0x050800, 0x05080B,
     "K6-2 (\"Chomper\")",
     250, 233, 350},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_K6, 0x05080C, 0x0508FF,
     "K6-2 (\"Chomper Extendend\" / CXT)",
     250, 266, 550},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_K6, 0x050900, 0x0509FF,
     "K6-III, K6-III-P (\"Sharptooth\", K6-3D+)",
     250, 350, 450},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_GEODE, 0x050A00, 0x050AFF,
     "Geode LX 600 to 900",
     130, 366, 600},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_K6, 0x050D00, 0x050DFF,
     "K6-2+, K6-III+",
     180, 450, 550},
    {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_K6, 0x050000, 0x05FFFF,
     "K5/K6 (unknown model)",
     500, 75, 600},

    /* Geode NX cannot be distinguished from Athlon XP by model ID */
    /* {FV_CPU_VENDOR_AMD, FV_CPU_FAMILY_AMD_GEODE, 0x060800, 0x0608FF,
     *  "Geode NX 1250 to 1750",
     *  130, 667, 1400},*/

    /* IDT / Centaur Technology */
    {FV_CPU_VENDOR_IDT, FV_CPU_FAMILY_IDT_WINCHIP, 0x050400, 0x0504FF,
     "WinChip (C6)",
     350, 180, 240},
    {FV_CPU_VENDOR_IDT, FV_CPU_FAMILY_IDT_WINCHIP, 0x050800, 0x050806,
     "WinChip 2 (W2)",
     350, 200, 233},
    {FV_CPU_VENDOR_IDT, FV_CPU_FAMILY_IDT_WINCHIP, 0x050807, 0x050809,
     "WinChip 2A (W2A)",
     350, 200, 250},
    {FV_CPU_VENDOR_IDT, FV_CPU_FAMILY_IDT_WINCHIP, 0x05080A, 0x0508FF,
     "WinChip 2B-200/233 (W2B)",
     250, 200, 200},
    {FV_CPU_VENDOR_IDT, FV_CPU_FAMILY_IDT_WINCHIP, 0x050900, 0x0509FF,
     "WinChip 3 (W3)",
     250, 233, 333},
    {FV_CPU_VENDOR_IDT, FV_CPU_FAMILY_IDT_WINCHIP, 0x050000, 0x05FFFF,
     "WinChip (unknown model)",
     350, 180, 333},

    /* VIA / Centaur Technology C3 */
    {FV_CPU_VENDOR_VIA, FV_CPU_FAMILY_VIA_C3, 0x060600, 0x0606FF,
     "Cyrix III, C3, 1GigaPro (WinChip 5A / Samuel)",
     180, 466, 733},
    {FV_CPU_VENDOR_VIA, FV_CPU_FAMILY_VIA_C3, 0x060700, 0x060707,
     "Cyrix III, C3, 1GigaPro, Eden ESP 4000/5000/6000 (WinChip 5B / Samuel 2)",
     150, 600, 800},
    {FV_CPU_VENDOR_VIA, FV_CPU_FAMILY_VIA_C3, 0x060708, 0x0607FF,
     "C3 (WinChip 5C / Ezra)",
     130, 733, 933},
    {FV_CPU_VENDOR_VIA, FV_CPU_FAMILY_VIA_C3, 0x060800, 0x0608FF,
     "C3 (WinChip 5N / Ezra-T)",
     130, 800, 1000},
    {FV_CPU_VENDOR_VIA, FV_CPU_FAMILY_VIA_C3, 0x060900, 0x060907,
     "C3, Eden ESP 7000/8000/10000 (WinChip 5XL / Nehemiah)",
     130, 800, 1400},
    {FV_CPU_VENDOR_VIA, FV_CPU_FAMILY_VIA_C3, 0x060908, 0x0609FF,
     "C3, C3-M, Eden-N (WinChip 5P / Antaur/ Nehemiah+)",
     130, 1000, 1400},
    {FV_CPU_VENDOR_VIA, FV_CPU_FAMILY_VIA_C3, 0x060000, 0x0609FF,
     "C3 (unknown model)",
     180, 400, 1400},

    /* VIA / Centaur Technology C7 */
    {FV_CPU_VENDOR_VIA, FV_CPU_FAMILY_VIA_C7, 0x060A00, 0x060AFF,
     "C7, C7-D, C7-M, Eden (WinChip 5J / Esther)",
     90, 400, 2000},
    {FV_CPU_VENDOR_VIA, FV_CPU_FAMILY_VIA_C7, 0x060D00, 0x060DFF,
     "C7-M ULV (WinChip 5J / Esther)",
     90, 500, 1600},
    {FV_CPU_VENDOR_VIA, FV_CPU_FAMILY_VIA_C7, 0x060F00, 0x060F07,
     "Nano, Nano-M 1000/2000 series (CNA / Isaiah)",
     65, 1000, 1800},
    {FV_CPU_VENDOR_VIA, FV_CPU_FAMILY_VIA_C7, 0x060F08, 0x060F09,
     "Nano 3000 series (CNB / Isaiah)",
     65, 800, 2000},
    {FV_CPU_VENDOR_VIA, FV_CPU_FAMILY_VIA_C7, 0x060F0A, 0x060F0B,
     "Nano X2, Eden X2 (CNC / Isaiah)",
     40, 800, 1600},
    {FV_CPU_VENDOR_VIA, FV_CPU_FAMILY_VIA_C7, 0x060F0C, 0x060FFF,
     "Nano QuadCore (CNQ / Isaiah)",
     40, 1000, 1467},
    {FV_CPU_VENDOR_VIA, FV_CPU_FAMILY_VIA_C7, 0x060A00, 0x06FFFF,
     "C7 / Nano (unknown model)",
     90, 400, 2000},


    /* Cyrix 5x86/MediaGX */
    {FV_CPU_VENDOR_CYRIX, FV_CPU_FAMILY_CYRIX_486, 0x040900, 0x0409FF,
     "5x86",
     650, 100, 133},
    {FV_CPU_VENDOR_CYRIX, FV_CPU_FAMILY_CYRIX_586, 0x040000, 0x04FFFF,
     "5x86 (unknown model)",
     650, 100, 133},
    {FV_CPU_VENDOR_CYRIX, FV_CPU_FAMILY_CYRIX_586, 0x050200, 0x0502FF,
     "6x86 PR90+ to PR200+ (M1)",
     500, 90, 150},
    {FV_CPU_VENDOR_CYRIX, FV_CPU_FAMILY_CYRIX_586, 0x050400, 0x0504FF,
     "MediaGX / MediaGXm / NSC Geode GXm",
     350, 120, 300},
    {FV_CPU_VENDOR_CYRIX, FV_CPU_FAMILY_CYRIX_586, 0x050000, 0x05FFFF,
     "6x86 M1 / MediaGX (unknown model)",
     500, 90, 300},

    /* Cyrix 6x86 */
    {FV_CPU_VENDOR_CYRIX, FV_CPU_FAMILY_CYRIX_686, 0x060000, 0x0600FF,
     "6x86MX (MII, M2)",
     350, 150, 187},
    /* VIA / Cyrix */
    {FV_CPU_VENDOR_CYRIX, FV_CPU_FAMILY_CYRIX_686, 0x060500, 0x0605FF,
     "VIA/Cyrix III 433-600 (MII/Joshua)",
     180, 350, 500},
    {FV_CPU_VENDOR_CYRIX, FV_CPU_FAMILY_CYRIX_586, 0x060000, 0x06FFFF,
     "6x86 MII (unknown model)",
     350, 150, 500},

   /* NexGen Nx586 */
    {FV_CPU_VENDOR_NEXGEN, FV_CPU_FAMILY_NEXGEN_586, 0x050000, 0x0500FF,
     "Nx586 P75 to P133",
     500, 70, 120},
    {FV_CPU_VENDOR_NEXGEN, FV_CPU_FAMILY_NEXGEN_586, 0x050000, 0x05FFFF,
     "Nx586 (unknown model)",
     500, 70, 120},

    /* National Semiconductor Geode */
    {FV_CPU_VENDOR_NSC, FV_CPU_FAMILY_NSC_GEODE, 0x050400, 0x0504FF,
     "Geode GX1/GXLV/GXm",
     180, 200, 333},
    {FV_CPU_VENDOR_NSC, FV_CPU_FAMILY_NSC_GEODE, 0x050500, 0x0505FF,
     "Geode GX2",
     150, 266, 400},
    {FV_CPU_VENDOR_NSC, FV_CPU_FAMILY_NSC_GEODE, 0x050000, 0x05FFFF,
     "Geode (unknown model)",
     180, 200, 400},

    /* RDC IAD 100 */
    {FV_CPU_VENDOR_RDC, FV_CPU_FAMILY_RDC_IAD, 0x050800, 0x0508FF,
     "IAD 100",
     90, 1000, 1000},
    {FV_CPU_VENDOR_RDC, FV_CPU_FAMILY_RDC_IAD, 0x050000, 0x05FFFF,
     "IAD 100xx (unknown model)",
     90, 1000, 1000},

    /* Rise mP6 */
    {FV_CPU_VENDOR_RISE, FV_CPU_FAMILY_RISE_MP6, 0x050000, 0x0500FF,
     "mP6 iDragon PR166-366 (Kirin)",
     250, 166, 250},
    {FV_CPU_VENDOR_RISE, FV_CPU_FAMILY_RISE_MP6, 0x050200, 0x0502FF,
     "mP6 iDragon PR266-433 (Lynx)",
     180, 200, 350},
    {FV_CPU_VENDOR_RISE, FV_CPU_FAMILY_RISE_MP6, 0x050800, 0x0508FF,
     "mP6 iDragon II 300-400",
     250, 200, 250},
    {FV_CPU_VENDOR_RISE, FV_CPU_FAMILY_RISE_MP6, 0x050900, 0x0509FF,
     "mP6 iDragon II 380-466",
     180, 250, 350},
    {FV_CPU_VENDOR_RISE, FV_CPU_FAMILY_RISE_MP6, 0x050000, 0x05FFFF,
     "mP6 (unknown model)",
     250, 166, 350},

    /* SiS 55x */
    {FV_CPU_VENDOR_SIS, FV_CPU_FAMILY_SIS_550, 0x050000, 0x0500FF,
     "SiS 55x / Vortex86",
     180, 200, 200},
    {FV_CPU_VENDOR_SIS, FV_CPU_FAMILY_SIS_550, 0x050000, 0x05FFFF,
     "Vortex86 (unknown model)",
     180, 200, 200},

    /* DM&P Vortex86 */
    {FV_CPU_VENDOR_DMP, FV_CPU_FAMILY_DMP_VORTEX86, 0x050200, 0x0502FF,
     "Vortex86DX/PDX-600",
     90, 600, 1000},
    {FV_CPU_VENDOR_DMP, FV_CPU_FAMILY_DMP_VORTEX86, 0x050800, 0x0508FF,
     "Vortex86MX/PMX-1000",
     90, 1000, 1000},
    {FV_CPU_VENDOR_DMP, FV_CPU_FAMILY_DMP_VORTEX86, 0x050000, 0x05FFFF,
     "Vortex86 (unknown model)",
     90, 600, 1000},

    /* Transmeta Crusoe */
    {FV_CPU_VENDOR_TRANSMETA, FV_CPU_FAMILY_TRANSMETA_CRUSOE,
     0x050400, 0x050402,
     "Crusoe TM3200",
     180, 333, 400},
    {FV_CPU_VENDOR_TRANSMETA, FV_CPU_FAMILY_TRANSMETA_CRUSOE,
     0x050403, 0x0504FF,
     "Crusoe TM5x00",
     180, 500, 1000},
    {FV_CPU_VENDOR_TRANSMETA, FV_CPU_FAMILY_TRANSMETA_CRUSOE,
     0x050000, 0x05FFFF,
     "Crusoe (unknown model)",
     180, 333, 1000},

    /* Transmeta Efficeon */
    {FV_CPU_VENDOR_TRANSMETA, FV_CPU_FAMILY_TRANSMETA_EFFICEON,
     0x0F0200, 0x0F02FF,
     "Efficeon TM8300/8600/8620",
     130, 900, 1200},
    {FV_CPU_VENDOR_TRANSMETA, FV_CPU_FAMILY_TRANSMETA_EFFICEON,
     0x0F0300, 0x0F03FF,
     "Efficeon TM8500/8800/8820",
     90, 1000, 1700},
    {FV_CPU_VENDOR_TRANSMETA, FV_CPU_FAMILY_TRANSMETA_EFFICEON,
     0x0F0000, 0x0FFFFF,
     "Efficeon (unknown model)",
     130, 900, 1700},

    /* UMC 486 */
    {FV_CPU_VENDOR_UMC, FV_CPU_FAMILY_UMC_486, 0x040100, 0x0401FF,
     "U5SD (486DX)",
     600, 25, 40},
    {FV_CPU_VENDOR_UMC, FV_CPU_FAMILY_UMC_486, 0x040200, 0x0402FF,
     "U5S (486SX)",
     600, 25, 40},
    {FV_CPU_VENDOR_UMC, FV_CPU_FAMILY_UMC_486, 0x040300, 0x0403FF,
     "U5 (486DX2)",
     600, 66, 66},
    {FV_CPU_VENDOR_UMC, FV_CPU_FAMILY_UMC_486, 0x040500, 0x0405FF,
     "U5SX (486SX2)",
     600, 33, 40},
    {FV_CPU_VENDOR_UMC, FV_CPU_FAMILY_UMC_486, 0x040000, 0x04FFFF,
     "U5xx 486 (unknown model)",
     600, 25, 66},

    {FV_CPU_VENDOR_UNKNOWN, FV_CPU_FAMILY_UNKNOWN, 0x000000, 0xFFFFFF,
     "(unknown)",
     0, 0, 0}
};


int fv_get_cpu_brand_string_x86(char *dst) {
    struct cpuid_data cpuid;
    uint32_t max_level;

#if ARCH_X86_32
    if (!fv_x86_cpuid_supported()) return 1;
#endif

    /* Check for brand string support */
    max_level = fv_x86_cpuid(0x80000000, 0, &cpuid);
    if (max_level < 0x80000004) return 1;

    fv_x86_cpuid(0x80000002, 0, &cpuid);
    memcpy(&dst[0], &cpuid, 16);

    fv_x86_cpuid(0x80000003, 0, &cpuid);
    memcpy(&dst[16], &cpuid, 16);

    fv_x86_cpuid(0x80000004, 0, &cpuid);
    memcpy(&dst[32], &cpuid, 16);

    return 0;
}


int fv_get_cpu_info_x86(FvCPUInfo *cpuinfo) {
    struct cpuid_data cpuid;
    union { uint32_t i[4]; char str[16]; } data;
    enum FvCPUVendor vendor = FV_CPU_VENDOR_UNKNOWN;
    uint32_t max_level;
    int i;

#if ARCH_X86_32
    if (!fv_x86_cpuid_supported()) return 1;
#endif

    max_level = fv_x86_cpuid(0, 0, &cpuid);

    /* Get vendor string */
    data.i[0] = cpuid.ebx;
    data.i[1] = cpuid.edx;
    data.i[2] = cpuid.ecx;
    data.i[3] = 0;

    /* Find matching FvCPUVendor enum */
    i = 0;
    while (cpu_vendor_strings[i].str != NULL) {
        if (!strncmp(data.str, cpu_vendor_strings[i].str, 12)) {
            vendor = cpu_vendor_strings[i].vendor;
            break;
        }
        i++;
    }
    if (cpu_vendor_strings[i].str == NULL) return 1;

    memcpy(cpuinfo->raw_vendor_str, data.str, 13);
    cpuinfo->raw_vendor_str[12] = '\0';

    if (max_level >= 1) {
        uint32_t family, model, stepping;
        fv_x86_cpuid(1, 0, &cpuid);
        /* Add the 4-bit family and 8-bit extended family fields together */
        family   = ((cpuid.eax >> 8) & 0xf) + ((cpuid.eax >> 20) & 0xff);
        /* Concatenate 4-bit extended model to the 4-bit model field */ 
        model    = ((cpuid.eax >> 4) & 0xf) + ((cpuid.eax >> 12) & 0xf0);
        /* Concatenate 2-bit processor type to the 4-bit stepping field */
        stepping = (cpuid.eax & 0xf) + ((cpuid.eax >> 8) & 0x30);
        cpuinfo->id = (family << 16) + (model << 8) + stepping;
    }

    /* Centaur has been owned by both IDT and VIA and both use the
     * same "CentaurHauls" vendor string. We'll differentiate
     * between the vendors by CPU family number. */
    if (vendor == FV_CPU_VENDOR_CENTAUR) {
        if (cpuinfo->id < 0x060000) vendor = FV_CPU_VENDOR_IDT;
        else vendor = FV_CPU_VENDOR_VIA;
    }

    i = 0;
    while (cpu_models[i].vendor != FV_CPU_VENDOR_UNKNOWN) {
        if ((cpu_models[i].vendor == vendor) &&
            (cpu_models[i].min_cpuid <= cpuinfo->id) &&
            (cpu_models[i].max_cpuid >= cpuinfo->id)) {
            cpuinfo->model = cpu_models[i];
            break;
        }
        i++;
    }
    if (cpu_models[i].vendor == FV_CPU_VENDOR_UNKNOWN) {
        cpuinfo->model = cpu_models[i];
        cpuinfo->model.vendor = vendor;
    }
    cpuinfo->vendor_name = cpu_vendors[vendor];
    cpuinfo->family_name = cpu_families[cpuinfo->model.family].name;
    cpuinfo->family_description = cpu_families[cpuinfo->model.family].description;

    cpuinfo->bitness = 8 * sizeof(void *);
#if HAVE_OPENMP
    cpuinfo->num_processors = omp_get_num_procs();
#else
    cpuinfo->num_processors = 1;
#endif

    /* Fetch additional CPUID information */
    fv_get_cpu_brand_string_x86(cpuinfo->raw_brand_str);
    cpuinfo->features = fv_get_cpu_features_x86();

    return 0;
}


uint32_t fv_get_cpu_features_x86(void) {
    struct cpuid_data cpuid;
    union { uint32_t i[3]; char c[12]; } vendor;
    uint32_t family = 0, model = 0;
    uint32_t flags = 0;
    uint32_t max_level;

#if ARCH_X86_32
    if (!fv_x86_cpuid_supported()) return 0;
#endif

    max_level = fv_x86_cpuid(0, 0, &cpuid);
    vendor.i[0] = cpuid.ebx;
    vendor.i[1] = cpuid.edx;
    vendor.i[2] = cpuid.ecx;

    if (max_level >= 1) {
        fv_x86_cpuid(1, 0, &cpuid);
        family   = ((cpuid.eax >> 8) & 0xf) + ((cpuid.eax >> 20) & 0xff);
        model    = ((cpuid.eax >> 4) & 0xf) + ((cpuid.eax >> 12) & 0xf0);
        if (cpuid.edx  &  (1 << 15)) flags |= FV_CPU_FLAG_CMOV;
        if (cpuid.edx  &  (1 << 23)) flags |= FV_CPU_FLAG_MMX;
        if (cpuid.edx  &  (1 << 25)) flags |= FV_CPU_FLAG_MMX2;
        if (cpuid.edx  &  (1 << 25)) flags |= FV_CPU_FLAG_SSE;
        if (cpuid.edx  &  (1 << 26)) flags |= FV_CPU_FLAG_SSE2;
        if (cpuid.ecx  &   1       ) flags |= FV_CPU_FLAG_SSE3;
        if (cpuid.ecx  &  (1 <<  9)) flags |= FV_CPU_FLAG_SSSE3;
        if (cpuid.ecx  &  (1 << 19)) flags |= FV_CPU_FLAG_SSE41;
        if (cpuid.ecx  &  (1 << 20)) flags |= FV_CPU_FLAG_SSE42;
        /* Check OXSAVE and AVX bits */
        if ((cpuid.ecx & 0x18000000) == 0x18000000) {
            /* Check for OS support */
            if ((fv_x86_xgetbv(0) & 0x6) == 0x6) flags |= FV_CPU_FLAG_AVX;
        }
    }

    /* Check AMD-specific flags */
    max_level = fv_x86_cpuid(0x80000000, 0, &cpuid);
    if (max_level >= 0x80000001) {
        fv_x86_cpuid(0x80000001, 0, &cpuid);
        if (cpuid.edx & (1U << 31)) flags |= FV_CPU_FLAG_3DNOW;
        if (cpuid.edx & (1  << 30)) flags |= FV_CPU_FLAG_3DNOWEXT;
        if (cpuid.edx & (1  << 23)) flags |= FV_CPU_FLAG_MMX;
        if (cpuid.edx & (1  << 22)) flags |= FV_CPU_FLAG_MMX2;

        /* Allow for selectively disabling SSE2 functions on AMD processors
           with SSE2 support but not SSE4a. This includes Athlon64, some
           Opteron, and some Sempron processors. MMX, SSE, or 3DNow! are
           faster than SSE2 often enough to utilize this special-case flag.
           FV_CPU_FLAG_SSE2 and FV_CPU_FLAG_SSE2SLOW are both set in this
           case so that SSE2 is used unless explicitly disabled by checking
           FV_CPU_FLAG_SSE2SLOW. */
        if (!strncmp(vendor.c, "AuthenticAMD", 12) &&
            flags & FV_CPU_FLAG_SSE2 && !(cpuid.ecx & 0x00000040)) {
            flags |= FV_CPU_FLAG_SSE2SLOW;
        }

        /* XOP and FMA4 use the AVX instruction coding scheme, so they
         * cannot be used unless the OS has AVX support. */
        if (flags & FV_CPU_FLAG_AVX) {
            if (cpuid.ecx & (1 << 11)) flags |= FV_CPU_FLAG_XOP;
            if (cpuid.ecx & (1 << 16)) flags |= FV_CPU_FLAG_FMA4;
        }
    }

    if (!strncmp(vendor.c, "GenuineIntel", 12)) {
        if (family == 6 && (model == 9 || model == 13 || model == 14)) {
            /* 6/9 (pentium-m "banias"), 6/13 (pentium-m "dothan"), and
             * 6/14 (core1 "yonah") theoretically support sse2, but it's
             * usually slower than mmx, so let's just pretend they don't.
             * FV_CPU_FLAG_SSE2 is disabled and FV_CPU_FLAG_SSE2SLOW is
             * enabled so that SSE2 is not used unless explicitly enabled
             * by checking FV_CPU_FLAG_SSE2SLOW. */
            if (flags  & FV_CPU_FLAG_SSE2)
                flags ^= FV_CPU_FLAG_SSE2SLOW | FV_CPU_FLAG_SSE2;
        }
        /* The Atom processor has SSSE3 support, which is useful in many
         * cases. However, sometimes the SSSE3 version is slower than
         * the SSE2 equivalent on the Atom, but is generally faster on
         * other processors supporting SSSE3. This flag allows for
         * selectively disabling certain SSSE3 functions on the Atom. */
        if (family == 6 && model == 28) flags |= FV_CPU_FLAG_ATOM;
    }

    return flags;
}

#endif /* ARCH_X86 */


/**
 * @file      x86.h
 * @brief     x86 CPU feature detection API for internal use
 * @copyright GNU Lesser General Public License version 2.1 or
 *            (at your option) any later version
 *
 * This code is borrowed from Libav.
 */

#ifndef FVUTIL_X86_H
#define FVUTIL_X86_H

#include "config.h"

#if ARCH_X86

#include <stdint.h>


/**
 * @internal Struct for storing CPUID output.
 */
struct cpuid_data {
    uint32_t eax;
    uint32_t ebx;
    uint32_t ecx;
    uint32_t edx;
};

/**
 * @internal Retrieves x86 CPU information.
 *
 * @param cpuinfo pointer to an FvCPUInfo struct where the retrieved
 *                CPU information will be stored.
 *
 * @return 0 on success, 1 on failure.
 */
int fv_get_cpu_info_x86(FvCPUInfo *cpuinfo);
 
/**
 * @internal Retrieves CPU brand string and writes it into a buffer.
 *
 * @param dst destination for the string. Must hold at least 48 bytes.
 *
 * @return 0 on success, 1 on failure.
 */
int fv_get_cpu_brand_string_x86(char *dst);
 
/**
 * @internal Returns x86 CPU features.
 *
 * This code is borrowed from libav.
 *
 * @return a set of CPU feature flags
 */
uint32_t fv_get_cpu_features_x86(void);

/**
 * @internal Checks if CPUID instruction is supported on the host CPU.
 *
 * @return 1 if CPUID is supported on the host, 0 otherwise
 */
int fv_x86_cpuid_supported(void);

/**
 * @internal Calls CPUID.
 *
 * @param leaf        a page index number or function selector which is
 *                    given to CPUID through EAX
 * @param subleaf     additional index used on some pages
 * @param[out] output pointer to a 16-byte memory area for storing CPUID
 *                    output from registers EAX, EBX, ECX and EDX,
 *                    respectively. NULL pointer can be given for no
 *                    output (other than the returned EAX value).
 * @return EAX value after calling CPUID
 */
uint32_t fv_x86_cpuid(uint32_t leaf, uint32_t subleaf,
                      struct cpuid_data *output);

/**
 * @internal Calls XGETBV to read the contents of an extended control
 *           register.
 *
 * @param xcr  number of the XCR register to read
 * @return 64-bit register value (concatenated EDX:EAX)
 */
int64_t fv_x86_xgetbv(int xcr);


#endif /* ARCH_X86 */

#endif /* FVUTIL_X86_H */

;
; @file      x86_cpuid.asm
; @brief     x86 CPU identification functions
; @author    Niko Mikkilä <nm@phnet.fi>
; @copyright GNU Lesser General Public License version 2.1 or
;            (at your option) any later version

%include "x86inc.asm"

SECTION .text


%if ARCH_X86_32

; int x86_cpuid_supported(void)
cglobal x86_cpuid_supported, 0, 2
    pushfd                  ; retrieve EFLAGS to EAX
    pop     eax             ; 
    mov     ecx, eax        ; save a copy to ECX for comparison
    xor     eax, (1 << 21)  ; flip the ID bit
    push    eax             ; write modified flags back to EFLAGS
    popfd                   ;
    pushfd                  ; retrieve EFLAGS back
    pop     eax             ;
    xor     eax, ecx        ; check if the set value survived
    shr     eax, 21         ; shift the bit so that we can return 0 or 1
    RET

%endif
     
; int x86_cpuid(int leaf, int subleaf, struct cpuid_data *data)
cglobal x86_cpuid, 3, 5     ; r4 is retained across a CPUID call in all
%if ARCH_X86_64             ; calling conventions. Make it safe to use.
    push      rbx           ; rbx is not saved by a 5-register cglobal
    mov       eax, r0d      ; load eax with the leaf number
    mov       ecx, r1d      ; load ecx with the subleaf number
%endif
    mov        r4, r2       ; save the data pointer
    cpuid
    cmp        r4, 0        ; if data is NULL, skip writing there
    je       exit
    mov   [r4   ], eax      ; write CPUID output to data
    mov   [r4+ 4], ebx
    mov   [r4+ 8], ecx
    mov   [r4+12], edx
exit:
%if ARCH_X86_64
    pop       rbx
%endif
    RET


; int64_t x86_xgetbv(int xcr)
cglobal x86_xgetbv, 1, 3    ; no need to save RBX
    mov    ecx, r0d
    xgetbv
%if ARCH_X86_64
    shl    rdx, 32      ; convert EDX:EAX to one 64-bit RAX value on x86-64
    or     rax, rdx
%endif
    RET
    

